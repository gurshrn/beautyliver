<?php

namespace AmeliaBooking\Application\Controller\User;

use AmeliaBooking\Application\Commands\User\ConfrimPhoneCommand;
use AmeliaBooking\Application\Commands\CommandResult;
use AmeliaBooking\Application\Controller\Controller;
use AmeliaBooking\Domain\Events\DomainEventBus;
use Slim\Http\Request;
use Twilio\Rest\Client;

/**
 * Class ConfrimPhoneController
 *
 * @package AmeliaBooking\Application\Controller\User
 */
class ConfrimPhoneController extends Controller
{
    /**
     * Instantiates the Delete User command to hand it over to the Command Handler
     *
     * @param Request $request
     * @param         $args
     *
     * @return ConfrimPhoneCommand
     * @throws \RuntimeException
     */
    protected function instantiateCommand(Request $request, $args)
    {
        $command = new ConfrimPhoneCommand($args);
        $requestBody = $request->getParsedBody();
        $command->setField('phone', $requestBody['phone']);
        $command->setField('pin', $requestBody['pin']);
        $command->setField('user_id', $requestBody['id']);
        return $command;
    }

    
}
