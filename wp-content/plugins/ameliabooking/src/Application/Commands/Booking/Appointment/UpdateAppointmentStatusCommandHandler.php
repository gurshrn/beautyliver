<?php

namespace AmeliaBooking\Application\Commands\Booking\Appointment;

use AmeliaBooking\Application\Commands\CommandHandler;
use AmeliaBooking\Application\Commands\CommandResult;
use AmeliaBooking\Application\Common\Exceptions\AccessDeniedException;
use AmeliaBooking\Domain\Common\Exceptions\InvalidArgumentException;
use AmeliaBooking\Domain\Entity\Booking\Appointment\Appointment;
use AmeliaBooking\Domain\Entity\Entities;
use AmeliaBooking\Domain\Services\Booking\AppointmentDomainService;
use AmeliaBooking\Domain\ValueObjects\String\BookingStatus;
use AmeliaBooking\Infrastructure\Common\Exceptions\QueryExecutionException;
use AmeliaBooking\Infrastructure\Repository\Bookable\Service\ServiceRepository;
use AmeliaBooking\Infrastructure\Repository\Booking\Appointment\AppointmentRepository;
use AmeliaBooking\Infrastructure\Repository\Booking\Appointment\CustomerBookingRepository;
use AmeliaBooking\Infrastructure\WP\Translations\BackendStrings;
use Twilio\Rest\Client;

/**
 * Class UpdateAppointmentStatusCommandHandler
 *
 * @package AmeliaBooking\Application\Commands\Booking\Appointment
 */
class UpdateAppointmentStatusCommandHandler extends CommandHandler
{
    /**
     * @var array
     */
    public $mandatoryFields = [
        'status'
    ];

    /**
     * @param UpdateAppointmentStatusCommand $command
     *
     * @return CommandResult
     * @throws \Slim\Exception\ContainerValueNotFoundException
     * @throws QueryExecutionException
     * @throws InvalidArgumentException
     * @throws AccessDeniedException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function handle(UpdateAppointmentStatusCommand $command)
    {
		if (!$this->getContainer()->getPermissionsService()->currentUserCanWriteStatus(Entities::APPOINTMENTS)) {
            throw new AccessDeniedException('You are not allowed to update appointment status');
        }

        $result = new CommandResult();

        $this->checkMandatoryFields($command);

        /** @var CustomerBookingRepository $bookingRepository */
        $bookingRepository = $this->container->get('domain.booking.customerBooking.repository');
        /** @var AppointmentRepository $appointmentRepo */
        $appointmentRepo = $this->container->get('domain.booking.appointment.repository');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $this->container->get('domain.bookable.service.repository');
        /** @var AppointmentDomainService $appointmentDS */
        $appointmentDS = $this->container->get('domain.booking.appointment.service');

        $appointmentId = (int)$command->getArg('id');
        $requestedStatus = $command->getField('status');

        /** @var Appointment $appointment */
        $appointment = $appointmentRepo->getById($appointmentId);
		
		$serviceId = $appointment->getServiceId()->getValue();
        $providerId = $appointment->getProviderId()->getValue();

        $service = $serviceRepository->getProviderServiceWithExtras($serviceId, $providerId);

        $bookings = $appointment->getBookings();
        foreach ($bookings->toArray() as $booking) {
            $booking = $bookings->getItem($booking['id']);
            $booking->setStatus(new BookingStatus($requestedStatus));
        }

        $bookingsCount = $appointmentDS->getBookingsStatusesCount($appointment);

        $appointmentStatus = $appointmentDS->getAppointmentStatusWhenApprovingAppointment(
            $service,
            $bookingsCount,
            $requestedStatus
        );

        if ($appointmentStatus === BookingStatus::APPROVED &&
            array_sum($bookingsCount) > $service->getMaxCapacity()->getValue()) {
            $result->setResult(CommandResult::RESULT_SUCCESS);
            $result->setMessage('Appointment status not updated');
            $result->setData([
                Entities::APPOINTMENT => $appointment->toArray(),
                'status'              => $appointment->getStatus()->getValue(),
                'message'             => 'Maximum capacity is ' . $service->getMaxCapacity()->getValue()
            ]);

            return $result;
        }

        $appointment->setStatus(new BookingStatus($appointmentStatus));

        $appointmentRepo->beginTransaction();

        try {
            $bookingRepository->updateStatusByAppointmentId($appointmentId, $requestedStatus);
            $appointmentRepo->updateStatusById($appointmentId, $appointmentStatus);
        } catch (QueryExecutionException $e) {
            $appointmentRepo->rollback();
            throw $e;
        }

        $appointmentRepo->commit();

        $result->setResult(CommandResult::RESULT_SUCCESS);
        $result->setMessage('Successfully updated appointment status');
		
		/*====Send sms by twilio====*/
		
		global $wpdb;
		$appointmentDetail = "SELECT wp_amelia_customer_bookings.id as bookingId,wp_customer_address_detail.name as bellName,wp_customer_address_detail.additional_address_info,wp_amelia_appointments.serviceId,wp_amelia_appointments.customer_address as address,wp_amelia_appointments.bookingStart as date,wp_amelia_users.firstName as customerName, prof.firstName as profFirstName ,prof.phone as proPhone, prof.lastName as profLastName, wp_amelia_users.phone,wp_amelia_services.name FROM wp_amelia_appointments LEFT JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId LEFT JOIN wp_amelia_users ON wp_amelia_customer_bookings.customerId = wp_amelia_users.id LEFT JOIN wp_amelia_users as prof ON wp_amelia_appointments.providerId = prof.id LEFT JOIN wp_amelia_services ON wp_amelia_appointments.serviceId = wp_amelia_services.id LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_appointments.providerId = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id LEFT JOIN wp_customer_address_detail ON wp_amelia_customer_bookings.id = wp_customer_address_detail.bookingId WHERE wp_amelia_appointments.id='".$appointmentId."'";
		$getAppointmentDetail = $wpdb->get_results($appointmentDetail, OBJECT);
		
		$services = "SELECT wp_amelia_services.*,wp_amelia_categories.id as catId,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.id as profId,wp_amelia_locations.latitude,wp_amelia_locations.longitude,wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.pictureFullPath as user_picture FROM wp_amelia_services LEFT JOIN wp_amelia_categories ON wp_amelia_services.categoryId = wp_amelia_categories.id LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId LEFT JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_services.id = '".$getAppointmentDetail[0]->serviceId."'";
		$getServices = $wpdb->get_results($services, OBJECT);
		
		$extraService = "SELECT wp_amelia_extras.name,wp_amelia_extras.price,wp_amelia_extras.duration FROM  wp_amelia_customer_bookings_to_extras LEFT JOIN wp_amelia_extras ON wp_amelia_customer_bookings_to_extras.extraId = wp_amelia_extras.id WHERE wp_amelia_customer_bookings_to_extras.customerBookingId ='".$getAppointmentDetail[0]->bookingId."'";
		$getExtraServiceDetail = $wpdb->get_results($extraService, OBJECT);
		if(count($getExtraServiceDetail) == 0)
		{
			$extraServices = 'None';
		}
		else
		{
			foreach($getExtraServiceDetail as $vals)
			{
				$extraSer[] = $vals->name;
			}
			$extraServices = implode(",",$extraSer);
		}
		/*get total extra service duration and price*/
		$extraServiceDuration = '';
		$extraServicePrice = '';
		if(count($getExtraServiceDetail)>0)
		{
			foreach($getExtraServiceDetail as $val)
			{
				$extraServiceDuration += $val->duration;
				$extraServicePrice += $val->price;
			}
		}
		$totalServiceDuration = $getServices[0]->duration+$extraServiceDuration;
		$totalDuration = convertIntoMin($totalServiceDuration);
		$totalServicePrice = $getServices[0]->price+$extraServicePrice;
		
		$message = '';
		if($getAppointmentDetail[0]->additional_address_info == '')
		{
			$addInfo = 'None';
		}
		else
		{
			$addInfo = $getAppointmentDetail[0]->additional_address_info;
		}
		
		if($requestedStatus == 'approved')
		{
			
			 $message .= 'Dear '.ucfirst($getAppointmentDetail[0]->customerName).', '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' has confirmed your order for '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).' at '.$getAppointmentDetail[0]->address.'. Name on the Bell: '.$getAppointmentDetail[0]->bellName.' Add Info: '.$addInfo.' Service: '.$getServices[0]->name.', Extras: '.$extraServices.', Time: '.$totalDuration.', Price '.$totalServicePrice.'. Professional contact: '.$getAppointmentDetail[0]->proPhone.'. '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' will arrive in time at your selected location.';
		}
		else if($requestedStatus == 'pending')
		{
			$message .= ' Dear '.ucfirst($getAppointmentDetail[0]->customerName).',The '.$getAppointmentDetail[0]->name.' appointment with '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' at '.$getAppointmentDetail[0]->address.' ,scheduled for '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).'is waiting for a confirmation.Thank you for choosing our company,BeautyLivery';
		}
		else if($requestedStatus == 'rejected')
		{
			$message .= 'Dear '.ucfirst($getAppointmentDetail[0]->customerName).', We are sorry to tell you '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' not accepted your service request. We hope you still try to get your service done with us. Why not trying another Professional at the same time and location ? Just follow this link: '.get_site_url();
		}
		else if($requestedStatus == 'canceled')
		{
			$message .= 'Dear '.ucfirst($getAppointmentDetail[0]->customerName).', your order for '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).' at '.$getAppointmentDetail[0]->address.' has been cancelled by '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.'.';
		}
        else if($requestedStatus == 'completed')
        {
            $message .= ' Dear '.$getAppointmentDetail[0]->customerName.',Your '.$getAppointmentDetail[0]->name.' appointment with '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' scheduled on '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).' at'. ' '.$getAppointmentDetail[0]->address.' has been completed.Thank you for choosing our company,BeautyLivery';
        }
		
		$TWILIO_SID = get_field('account_sid','options');
		$TWILIO_TOKEN = get_field('auth_token','options');
		$sender_phone_number = get_field('sender_phone_number','options');
		$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
		$response = $client->messages->create(
			$getAppointmentDetail[0]->phone,
			array(
				'from' => $sender_phone_number,
				'body' => $message
			)
		);
		
		/*====End Send sms by twilio====*/
		
        $result->setData([
            Entities::APPOINTMENT => $appointment->toArray(),
            'status'              => $appointmentStatus,
            'message'             =>
                ($requestedStatus === BookingStatus::APPROVED && $appointmentStatus !== BookingStatus::APPROVED) ?
                    BackendStrings::getAppointmentStrings()['minimum_number_of_persons']
                    . ' ' . $service->getMinCapacity()->getValue() :
                    BackendStrings::getAppointmentStrings()['appointment_status_changed'] . $requestedStatus
        ]);

        return $result;
    }
}
