<?php

namespace AmeliaBooking\Application\Commands\Booking\Appointment;

use AmeliaBooking\Application\Commands\CommandHandler;
use AmeliaBooking\Application\Commands\CommandResult;
use AmeliaBooking\Application\Services\Booking\AppointmentApplicationService;
use AmeliaBooking\Domain\Common\Exceptions\InvalidArgumentException;
use AmeliaBooking\Domain\Entity\Booking\Appointment\Appointment;
use AmeliaBooking\Domain\Entity\Entities;
use AmeliaBooking\Infrastructure\Common\Exceptions\QueryExecutionException;
use AmeliaBooking\Infrastructure\Repository\Bookable\Service\ServiceRepository;
use AmeliaBooking\Infrastructure\Repository\Booking\Appointment\AppointmentRepository;
use AmeliaBooking\Infrastructure\WP\Translations\FrontendStrings;
use Twilio\Rest\Client;

/**
 * Class AddAppointmentCommandHandler
 *
 * @package AmeliaBooking\Application\Commands\Booking\Appointment
 */
class AddAppointmentCommandHandler extends CommandHandler
{
    /**
     * @var array
     */
    public $mandatoryFields = [
        'bookings',
        'bookingStart',
        'notifyParticipants',
        'serviceId',
        'providerId'
    ];

    /**
     * @param AddAppointmentCommand $command
     *
     * @return CommandResult
     * @throws \Slim\Exception\ContainerValueNotFoundException
     * @throws InvalidArgumentException
     * @throws QueryExecutionException
     * @throws \Interop\Container\Exception\ContainerException
     * @throws \Exception
     */
    public function handle(AddAppointmentCommand $command)
    {
        $result = new CommandResult();

        $this->checkMandatoryFields($command);

        /** @var AppointmentRepository $appointmentRepo */
        $appointmentRepo = $this->container->get('domain.booking.appointment.repository');
        /** @var AppointmentApplicationService $appointmentAS */
        $appointmentAS = $this->container->get('application.booking.appointment.service');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $this->container->get('domain.bookable.service.repository');

        $appointmentData = $command->getFields();
        $service = $serviceRepository->getProviderServiceWithExtras(
            $appointmentData['serviceId'],
            $appointmentData['providerId']
        );

        $appointment = $appointmentAS->build($appointmentData, $service);

        if (!$appointment instanceof Appointment) {
            $result->setResult(CommandResult::RESULT_ERROR);
            $result->setMessage('Could not create new appointment');

            return $result;
        }
        

        if (!$appointmentAS->canBeBooked($appointment)) {
            $result->setResult(CommandResult::RESULT_ERROR);
            $result->setMessage(FrontendStrings::getCommonStrings()['time_slot_unavailable']);
            $result->setData([
                'timeSlotUnavailable' => true
            ]);

            return $result;
        }

        $appointmentRepo->beginTransaction();

        try {
            $appointmentAS->add($appointment, $service, $command->getField('payment'));
			
			
        } catch (QueryExecutionException $e) {
            $appointmentRepo->rollback();
            throw $e;
        }

        $result->setResult(CommandResult::RESULT_SUCCESS);
        $result->setMessage('Successfully added new appointment');
        $result->setData(
            [
                Entities::APPOINTMENT => $appointment->toArray()
            ]
        );
		
		$appointmentRepo->commit();
		
		/*====Send sms by twilio====*/
		
			global $wpdb;
			$appoId = $appointment->toArray();
			$appointmentDetail = "SELECT wp_amelia_locations.address,wp_amelia_appointments.bookingStart as date,wp_amelia_users.firstName as customerName, prof.firstName as profFirstName , prof.lastName as profLastName, wp_amelia_users.phone,wp_amelia_services.name FROM wp_amelia_appointments LEFT JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId LEFT JOIN wp_amelia_users ON wp_amelia_customer_bookings.customerId = wp_amelia_users.id LEFT JOIN wp_amelia_users as prof ON wp_amelia_appointments.providerId = prof.id LEFT JOIN wp_amelia_services ON wp_amelia_appointments.serviceId = wp_amelia_services.id LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_appointments.providerId = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_appointments.id='".$appoId['bookings'][0]['appointmentId']."'";
			$getAppointmentDetail = $wpdb->get_results($appointmentDetail, OBJECT);
			
			$message = '';
			if($appoId['bookings'][0]['status'] == 'approved')
			{
				$message .= ' Dear '.$getAppointmentDetail[0]->customerName.',You have successfully scheduled '.$getAppointmentDetail[0]->name.' appointment with '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.'. We are waiting you at '.$getAppointmentDetail[0]->address.' on '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).'.Thank you for choosing our company,BeautyLivery';
			}
			else if($appoId['bookings'][0]['status'] == 'pending')
			{
				$message .= ' Dear '.$getAppointmentDetail[0]->customerName.',The '.$getAppointmentDetail[0]->name.' appointment with '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' at '.$getAppointmentDetail[0]->address.' ,scheduled for '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).'is waiting for a confirmation.Thank you for choosing our company,BeautyLivery';
			}
			else if($appoId['bookings'][0]['status'] == 'rejected')
			{
				$message .= ' Dear '.$getAppointmentDetail[0]->customerName.',Your '.$getAppointmentDetail[0]->name.' appointment with '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' scheduled on '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).' at'. ' '.$getAppointmentDetail[0]->address.' has been rejected.Thank you for choosing our company,BeautyLivery';
			}
			else if($appoId['bookings'][0]['status'] == 'canceled')
			{
				$message .= ' Dear '.$getAppointmentDetail[0]->customerName.',Your '.$getAppointmentDetail[0]->name.' appointment with '.$getAppointmentDetail[0]->profFirstName.' '.$getAppointmentDetail[0]->profLastName.' scheduled on '.date("d M Y h:i A",strtotime($getAppointmentDetail[0]->date)).' at'. ' '.$getAppointmentDetail[0]->address.' has been canceled.Thank you for choosing our company,BeautyLivery';
			}
			
			$TWILIO_SID = get_field('account_sid','options');
			$TWILIO_TOKEN = get_field('auth_token','options');
			$sender_phone_number = get_field('sender_phone_number','options');
			$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
			$response = $client->messages->create(
				$getAppointmentDetail[0]->phone,
				array(
					'from' => $sender_phone_number,
					'body' => $message
				)
			);
			
			/*====End Send sms by twilio====*/

        return $result;
    }
}
