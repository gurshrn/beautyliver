<?php

namespace AmeliaBooking\Application\Commands\User;

use AmeliaBooking\Application\Common\Exceptions\AccessDeniedException;
use AmeliaBooking\Application\Services\User\UserApplicationService;
use AmeliaBooking\Domain\Common\Exceptions\InvalidArgumentException;
use AmeliaBooking\Domain\Entity\Entities;
use AmeliaBooking\Application\Commands\CommandResult;
use AmeliaBooking\Application\Commands\CommandHandler;
use AmeliaBooking\Infrastructure\Common\Exceptions\NotFoundException;
use AmeliaBooking\Infrastructure\Common\Exceptions\QueryExecutionException;
use Twilio\Rest\Client;

/**
 * Class DeleteUserCommandHandler
 *
 * @package AmeliaBooking\Application\Commands\User
 */
class ConfrimPhoneCommandHandler extends CommandHandler
{
    /**
     * @param DeleteUserCommand $command
     *
     * @return CommandResult
     * @throws \Slim\Exception\ContainerValueNotFoundException
     * @throws InvalidArgumentException
     * @throws AccessDeniedException
     * @throws \Interop\Container\Exception\ContainerException
     * @throws QueryExecutionException
     * @throws NotFoundException
     */
    public function handle(ConfrimPhoneCommand $command)
    {
        $fileds=$command->getFields();
        $result = new CommandResult();
        global $wpdb;
        $phone = $fileds['phone'];
        $user_id = $fileds['user_id'];
        try
        {
            $checkPhoneNumber = "SELECT * FROM wp_user_sms_pin WHERE phone_number = ".$phone." and sms_pin=". $fileds['pin'];
            $getCheckPhoneNumber = $wpdb->get_results($checkPhoneNumber, OBJECT);
            if(count($getCheckPhoneNumber) > 0)
            {
                $data = array('phone' => $phone);
                $wpdb->update('wp_amelia_users',  $data, array('id'=>$user_id));
                $result->setResult(CommandResult::RESULT_SUCCESS);
                $result->setMessage('Phone update successfully.');            
            }
            else
            {
                $result->setResult(CommandResult::RESULT_ERROR);
                $result->setMessage('Wrong Pin, Kindly try once again with correct pin.');
            }
        } 
        catch (Exception $e) 
        {
            $result->setResult(CommandResult::RESULT_ERROR);
            $result->setMessage('Oops, There is some issue while update phone.');

        }

        return $result;
    }
}
