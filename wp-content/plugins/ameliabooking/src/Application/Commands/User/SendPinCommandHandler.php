<?php

namespace AmeliaBooking\Application\Commands\User;

use AmeliaBooking\Application\Common\Exceptions\AccessDeniedException;
use AmeliaBooking\Application\Services\User\UserApplicationService;
use AmeliaBooking\Domain\Common\Exceptions\InvalidArgumentException;
use AmeliaBooking\Domain\Entity\Entities;
use AmeliaBooking\Application\Commands\CommandResult;
use AmeliaBooking\Application\Commands\CommandHandler;
use AmeliaBooking\Infrastructure\Common\Exceptions\NotFoundException;
use AmeliaBooking\Infrastructure\Common\Exceptions\QueryExecutionException;
use Twilio\Rest\Client;

/**
 * Class DeleteUserCommandHandler
 *
 * @package AmeliaBooking\Application\Commands\User
 */
class SendPinCommandHandler extends CommandHandler
{
    /**
     * @param DeleteUserCommand $command
     *
     * @return CommandResult
     * @throws \Slim\Exception\ContainerValueNotFoundException
     * @throws InvalidArgumentException
     * @throws AccessDeniedException
     * @throws \Interop\Container\Exception\ContainerException
     * @throws QueryExecutionException
     * @throws NotFoundException
     */
    public function handle(SendPinCommand $command)
    {
        $fileds=$command->getFields();
        $result = new CommandResult();
        global $wpdb;
        $length = 5;
        $randomNumber = substr(str_shuffle("0123456789"), 0, $length);
        $to = $fileds['phone'];
        $user_id = $fileds['user_id'];
        $sender_phone_number = get_field('sender_phone_number','options');
        $message   = 'Your One time otp password '.$randomNumber;
        $TWILIO_SID = get_field('account_sid','options');
        $TWILIO_TOKEN = get_field('auth_token','options');
        try
        {
            $checkPhoneNumberUser = "SELECT * FROM wp_amelia_users WHERE id= '".$user_id."' AND phone = ".$to;
            $getCheckPhoneNumberUser = $wpdb->get_results($checkPhoneNumberUser, OBJECT);
            if(count($getCheckPhoneNumberUser) > 0)
            {
                $result->setResult(CommandResult::RESULT_ERROR);
                $result->setMessage('You have already login with this phone number');
            }
            else
            {
                $checkPhoneNumberUser1 = "SELECT * FROM wp_amelia_users WHERE id != '".$user_id."' AND phone = ".$to;
                $getCheckPhoneNumberUser1 = $wpdb->get_results($checkPhoneNumberUser1, OBJECT);
                if(count($getCheckPhoneNumberUser1) > 0)
                {
                    $result->setResult(CommandResult::RESULT_ERROR);
                    $result->setMessage('Phone number already used by other user');
                }
                else
                {
                    $checkPhoneNumber = "SELECT * FROM wp_user_sms_pin WHERE phone_number = ".$to;
                    $getCheckPhoneNumber = $wpdb->get_results($checkPhoneNumber, OBJECT);
                    if(count($getCheckPhoneNumber) > 0)
                    {
                        $client = new Client($TWILIO_SID, $TWILIO_TOKEN);
                        
                        $response = $client->messages->create(
                            $to ,
                            array(
                                'from' => $sender_phone_number,
                                'body' => $message
                            )
                        );
                        $data = array('sms_pin' => $randomNumber,'created_at'=>date('Y-m-d H:i:s'));
                        $wpdb->update('wp_user_sms_pin',  $data, array('phone_number'=>$to));
                    }
                    else
                    {
                        $client = new Client($TWILIO_SID, $TWILIO_TOKEN);
                        $response = $client->messages->create(
                            $to ,
                            array(
                                'from' => $sender_phone_number,
                                'body' => $message
                            )
                        );
                        $wpdb->insert('wp_user_sms_pin', array(
                            'phone_number' => $to,
                            'sms_pin' => $randomNumber, 
                        ));
                    }
                    $result->setResult(CommandResult::RESULT_SUCCESS);
                    $result->setMessage('Kindly check your phone for pin.');

                }
            }
        } 
        catch (Exception $e) 
        {
            $result->setResult(CommandResult::RESULT_ERROR);
            $result->setMessage('Otp not sent');

        }

        return $result;
    }
}
