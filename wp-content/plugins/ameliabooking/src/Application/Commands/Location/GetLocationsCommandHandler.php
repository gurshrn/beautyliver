<?php

namespace AmeliaBooking\Application\Commands\Location;

use AmeliaBooking\Application\Commands\CommandResult;
use AmeliaBooking\Application\Commands\CommandHandler;
use AmeliaBooking\Application\Common\Exceptions\AccessDeniedException;
use AmeliaBooking\Domain\Collection\AbstractCollection;
use AmeliaBooking\Domain\Common\Exceptions\InvalidArgumentException;
use AmeliaBooking\Domain\Entity\Entities;
use AmeliaBooking\Domain\Services\Settings\SettingsService;
use AmeliaBooking\Infrastructure\Common\Exceptions\QueryExecutionException;
use AmeliaBooking\Infrastructure\Repository\Location\LocationRepository;

/**
 * Class GetLocationsCommandHandler
 *
 * @package AmeliaBooking\Application\Commands\Location
 */
class GetLocationsCommandHandler extends CommandHandler
{

    /**
     * @param GetLocationsCommand $command
     *
     * @return CommandResult
     * @throws \Slim\Exception\ContainerValueNotFoundException
     * @throws QueryExecutionException
     * @throws InvalidArgumentException
     * @throws AccessDeniedException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function handle(GetLocationsCommand $command)
    {
        if (!$this->getContainer()->getPermissionsService()->currentUserCanRead(Entities::LOCATIONS)) {
            throw new AccessDeniedException('You are not allowed to read locations');
        }

        $result = new CommandResult();
		
		 /** @var AbstractUser $user */
        $user = $this->container->get('logged.in.user');

        $userCanReadOthers = $this->container->getPermissionsService()->currentUserCanReadOthers(Entities::CUSTOMERS);
        $userIsCustomer = !$userCanReadOthers && $user->getType() === Entities::CUSTOMER;
        $userIsProvider = !$userCanReadOthers && $user->getType() === Entities::PROVIDER;
        $userId = $user->getId() === null ? 0 : $user->getId()->getValue();

        /** @var SettingsService $settingsService */
        $settingsService = $this->container->get('domain.settings.service');
        $itemsPerPage = $settingsService->getSetting('general', 'itemsPerPage');

        /** @var LocationRepository $locationRepository */
        $locationRepository = $this->getContainer()->get('domain.locations.repository');
		
		$params = $command->getField('params');
        if($userIsProvider){
            $params['providers']= [$userId];
        }
		
		$locations = $locationRepository->getFiltered($params, $itemsPerPage);

        if (!$locations instanceof AbstractCollection) {
            $result->setResult(CommandResult::RESULT_ERROR);
            $result->setMessage('Could not get locations');

            return $result;
        }

        $result->setResult(CommandResult::RESULT_SUCCESS);
        $result->setMessage('Successfully retrieved locations.');
        $result->setData([
            Entities::LOCATIONS => $locations->toArray(),
            'countFiltered'     => (int)$locationRepository->getCount($params),
            'countTotal'        => $userIsProvider?(int)$locationRepository->getCount(['providers'=>[$userId]]):(int)$locationRepository->getCount([])
        ]);

        return $result;
    }
}
