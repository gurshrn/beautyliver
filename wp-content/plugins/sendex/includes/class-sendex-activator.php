<?php

/**
 * Fired during plugin activation
 *
 * @link       http://menusms.com/dongido
 * @since      1.0.0
 *
 * @package    Sendex
 * @subpackage Sendex/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sendex
 * @subpackage Sendex/includes
 * @author     Gurjeevan Kaur <gurjeevan.kaur@imarkinfotech.com>
 */
class Sendex_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
