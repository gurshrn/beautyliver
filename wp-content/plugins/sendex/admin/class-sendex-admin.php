<?php


/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://menusms.com/dongido
 * @since      1.0.0
 *
 * @package    Sendex
 * @subpackage Sendex/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sendex
 * @subpackage Sendex/admin
 * @author     Gurjeevan Kaur <gurjeevan.kaur@imarkinfotech.com>
 */


require_once( plugin_dir_path( __FILE__ ) .'/../twilio/Twilio/autoload.php');

use Twilio\Rest\Client;
class Sendex_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sendex_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sendex_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sendex-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sendex_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sendex_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sendex-admin.js', array( 'jquery' ), $this->version, false );

	}
		/**
	 *  Register the administration menu for this plugin into the WordPress Dashboard
	 * @since    1.0.0
	 */

	public function add_sendex_admin_setting() {

	    /*
	     * Add a settings page for this plugin to the Settings menu.
	     *
	     * Administration Menus: http://codex.wordpress.org/Administration_Menus
	     *
	     */
	    add_options_page( 'SENDEX SMS PAGE', 'SENDEX', 'manage_options', $this->plugin_name, array($this, 'display_sendex_settings_page')
	    );
	}

	/**
	 * Render the settings page for this plugin.( The html file )
	 *
	 * @since    1.0.0
	 */

	public function display_sendex_settings_page() {
	    include_once( 'partials/sendex-admin-display.php' );
	}
	/**
	 * Registers and Defines the necessary fields we need.
	 *
	 */
	public function sendex_admin_settings_save(){

	    register_setting( $this->plugin_name, $this->plugin_name, array($this, 'plugin_options_validate') );

	    add_settings_section('sendex_main', 'Main Settings', array($this, 'sendex_section_text'), 'sendex-settings-page');

	    add_settings_field('api_sid', 'API SID', array($this, 'sendex_setting_sid'), 'sendex-settings-page', 'sendex_main');

	    add_settings_field('api_auth_token', 'API AUTH TOKEN', array($this, 'sendex_setting_token'), 'sendex-settings-page', 'sendex_main');
	}

	/**
	 * Displays the settings sub header
	 *
	 */
	public function sendex_section_text() {
	    echo '<h3>Edit api details</h3>';
	} 

	/**
	 * Renders the sid input field
	 *
	 */
	public function sendex_setting_sid() {

	   $options = get_option($this->plugin_name);
	   echo "<input id='plugin_text_string' name='$this->plugin_name[api_sid]' size='40' type='text' value='{$options['api_sid']}' />";
	}   

	/**
	 * Renders the auth_token input field
	 *
	 */
	public function sendex_setting_token() {
	   $options = get_option($this->plugin_name);
	   echo "<input id='plugin_text_string' name='$this->plugin_name[api_auth_token]' size='40' type='text' value='{$options['api_auth_token']}' />";
	}

	/**
	 * Sanitises all input fields.
	 *
	 */
	public function plugin_options_validate($input) {
	    $newinput['api_sid'] = trim($input['api_sid']);
	    $newinput['api_auth_token'] = trim($input['api_auth_token']);

	    return $newinput;
	}
		/**
	* Register the sms page for the admin area.
	*
	* @since    1.0.0
	*/
	public function register_sendex_sms_page() {
	  // Create our settings page as a submenu page.
	    add_submenu_page(
	            'tools.php',                                         // parent slug
	            __( 'SENDEX SMS PAGE', $this->plugin_name.'-sms' ), // page title
	            __( 'SENDEX', $this->plugin_name.'-sms' ),         // menu title
	            'manage_options',                                 // capability
	            $this->plugin_name.'-sms',                       // menu_slug
	            array( $this, 'display_sendex_sms_page' )       // callable function
	    );
	}

	/**
	* Display the sms page - The page we are going to be sending message from.
	*
	* @since    1.0.0
	*/

	public function display_sendex_sms_page() {
	   include_once( 'partials/sendex-admin-sms.php' );
	}

	public function send_message()
	{
		if(isset($_POST['action']) && $_POST['action'] == 'update_dashboard_profile')
		{
			$this->update_phone_number($_POST);
		}
		if(isset($_POST['action']) && $_POST['action'] == 'cancel_appointment')
		{
			$this->cancel_appoint_by_customer($_POST);
		}
		if(isset($_POST['action']) && $_POST['action'] == 'send_phone_pin')
		{
			$this->send_phone_pin($_POST);
		}
		if(isset($_POST['action']) && $_POST['action'] == 'send_login_phone_pin')
		{
			$this->send_login_phone_pin($_POST);
		}
		if(isset($_POST['action']) && $_POST['action'] == 'update_appointment_status')
		{
			$this->send_order_detail($_POST);
		}
		if(isset($_POST['action']) && $_POST['action'] == 'update_appointment_status')
		{
			$this->send_order_detail($_POST);
		}
	}
	
	public function update_phone_number($param)
	{
		if(isset($param['update_pin']) && $param['update_pin'] == 'yes')
		{
			global $wpdb;
			$length = 5;
			$randomNumber = substr(str_shuffle("0123456789"), 0, $length);
			$to = $param['phone_number'];
			$sender_phone_number = get_field('sender_phone_number','options');
          	$message   = 'Your One time otp password '.$randomNumber;
          	$TWILIO_SID = get_field('account_sid','options');
            $TWILIO_TOKEN = get_field('auth_token','options');
			try
            {
				$checkPhoneNumberUser = "SELECT * FROM wp_amelia_users WHERE id= '".$param['user_id']."' AND phone = ".$param['phone_number'];
				$getCheckPhoneNumberUser = $wpdb->get_results($checkPhoneNumberUser, OBJECT);
				if(count($getCheckPhoneNumberUser) > 0)
				{
					$result['success'] = false;
					$result['msg'] = 'You have already login with this phone number';
				}
				else
				{
					$checkPhoneNumberUser1 = "SELECT * FROM wp_amelia_users WHERE id != '".$param['user_id']."' AND phone = ".$param['phone_number'];
					$getCheckPhoneNumberUser1 = $wpdb->get_results($checkPhoneNumberUser1, OBJECT);
					if(count($getCheckPhoneNumberUser1) > 0)
					{
						$result['success'] = false;
						$result['msg'] = 'Phone number already exist';
					}
					else
					{
						$checkPhoneNumber = "SELECT * FROM wp_user_sms_pin WHERE phone_number = ".$param['phone_number'];
						$getCheckPhoneNumber = $wpdb->get_results($checkPhoneNumber, OBJECT);
						if(count($getCheckPhoneNumber) > 0)
						{
							$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
							
							$response = $client->messages->create(
								$to ,
								array(
									'from' => $sender_phone_number,
									'body' => $message
								)
							);
							$data = array('sms_pin' => $randomNumber,'created_at'=>date('Y-m-d H:i:s'));
							$wpdb->update('wp_user_sms_pin',  $data, array('phone_number'=>$param['phone_number']));
						}
						else
						{
							$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
							$response = $client->messages->create(
								$to ,
								array(
									'from' => $sender_phone_number,
									'body' => $message
								)
							);
							$wpdb->insert('wp_user_sms_pin', array(
								'phone_number' => $param['phone_number'],
								'sms_pin' => $randomNumber, 
							));
						}
						$result['success'] = true;
						$result['msg'] = 'Otp sent';
					}
				}
			} 
            catch (Exception $e) 
            {
            	$result['success'] = false;
                $result['msg'] = 'Otp not sent';

            }
            echo json_encode($result);exit;
		}
	}
	
	public function cancel_appoint_by_customer($param)
	{
		if(isset($param['cancel_appointment_by_customer']) && $param['cancel_appointment_by_customer'] != '')
		{
			global $wpdb;
			$data = array('status'=>$_POST['status']);
			if($wpdb->update('wp_amelia_appointments', $data,array('id' => $_POST['appointmentId'])) === False)
			{
				$result['success'] = false;
				$result['msg'] = 'Appointment not cancelled successfully';
			}
			else
			{
				$wpdb->update('wp_amelia_customer_bookings', $data,array('appointmentId' => $_POST['appointmentId']));
				
				$appointment = "SELECT wp_amelia_appointments.customer_address,wp_amelia_appointments.bookingStart,wp_amelia_services.name,wp_amelia_users.phone,wp_amelia_users.firstName FROM  wp_amelia_appointments LEFT JOIN wp_amelia_services ON wp_amelia_appointments.serviceId = wp_amelia_services.id LEFT JOIN wp_amelia_users ON wp_amelia_appointments.providerId = wp_amelia_users.id WHERE wp_amelia_appointments.id = '".$param['appointmentId']."'";
				$getAppointment = $wpdb->get_results($appointment, OBJECT);
				$phoneNumber = $getAppointment[0]->phone;
				
				
				/*====Send sms to provider====*/
				if($phoneNumber != '')
				{
					$providerMsg = ' Hi '.$getAppointment[0]->firstName.', Your '.$getAppointment[0]->name.', scheduled on '.date("d.m.y",strtotime($getAppointment[0]->bookingStart)).' at '.$getAppointment[0]->customer_address.' has been canceled.Thank you';
					
					$TWILIO_SID = get_field('account_sid','options');
					$TWILIO_TOKEN = get_field('auth_token','options');
					$sender_phone_number = get_field('sender_phone_number','options');
					$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
					$response = $client->messages->create(
						$phoneNumber,
						//'+917355423498',
						array(
							'from' => $sender_phone_number,
							'body' => $providerMsg
						)
					);
				}
				$result['success'] = true;
				$result['url'] = get_site_url().'/dashboard-appointment';
				$result['msg'] = 'Appointment cancelled successfully';
			}
			echo json_encode($result);exit;
		}
	}
	
	public function send_order_detail($param)
	{
		if(isset($param['send_order_notificatiom']) && $param['send_order_notificatiom'] != '')
		{
			
			global $wpdb;
			$appointmentId = $param['appointmentId'];
			
			$wpdb->update('wp_amelia_customer_bookings', $param,array('appointmentId' => $appointmentId));
			$providerPhoneNumber = "SELECT wp_amelia_customer_bookings.id as bookingids,wp_customer_address_detail.name as custName,wp_amelia_services.name,wp_amelia_services.price,wp_amelia_services.duration,wp_amelia_users.firstName,wp_amelia_users.phone,wp_amelia_appointments.* FROM  wp_amelia_appointments LEFT JOIN wp_amelia_users ON wp_amelia_appointments.providerId = wp_amelia_users.id LEFT JOIN wp_amelia_services ON wp_amelia_appointments.serviceId = wp_amelia_services.id LEFt JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId LEFT JOIN wp_customer_address_detail ON wp_amelia_customer_bookings.id = wp_customer_address_detail.bookingId WHERE wp_amelia_appointments.id = '".$appointmentId."'";
			$getProviderPhoneNumber = $wpdb->get_results($providerPhoneNumber, OBJECT);
			
			$serviceExtra = "SELECT wp_amelia_customer_bookings_to_extras.*,wp_amelia_extras.name,wp_amelia_extras.duration,(SELECT SUM(wp_amelia_customer_bookings_to_extras.price) FROM wp_amelia_customer_bookings_to_extras WHERE customerBookingId = '".$getProviderPhoneNumber[0]->bookingids."') as extraTotalPrice from wp_amelia_customer_bookings_to_extras LEFT JOIN wp_amelia_extras ON wp_amelia_customer_bookings_to_extras.extraId = wp_amelia_extras.id WHERE customerBookingId = '".$getProviderPhoneNumber[0]->bookingids."'";
			$getserviceExtra = $wpdb->get_results($serviceExtra, OBJECT);
			
			$totalPrice = number_format($getProviderPhoneNumber[0]->price+$getserviceExtra[0]->extraTotalPrice,2);
			$serviceExtraName = '';
			$extraServiceDuration = '';
			if(count($getserviceExtra)>0)
			{
				foreach($getserviceExtra as $val)
				{
					$serviceExtraName[] = $val->name;
					$extraServiceDuration += $val->duration;
				}
			}
			if(count($getserviceExtra)>0)
			{
				$serviceExtraName = implode(", ",$serviceExtraName);
			}
			else
			{
				$serviceExtraName = 'None';
			}
			
			$totalServiceDuration = $getProviderPhoneNumber[0]->duration+$extraServiceDuration;
			$getTotalDuration = convertIntoMin($totalServiceDuration);
			$customerPhoneNumber  = getLoggedInUserDetail($_SESSION['user_id']);
			
			$providerMsg = ' Dear '.$getProviderPhoneNumber[0]->firstName.',order received from '.$getProviderPhoneNumber[0]->custName.' for '.date("d.m.y",strtotime($getProviderPhoneNumber[0]->bookingStart)).' '.date("h:i A",strtotime($getProviderPhoneNumber[0]->bookingStart)).' - '.date("h:i A",strtotime($getProviderPhoneNumber[0]->bookingEnd)).' at '.$getProviderPhoneNumber[0]->customer_address.'. Name: '.$getProviderPhoneNumber[0]->custName.' Add Info: '.$getProviderPhoneNumber[0]->customer_additional_address.' Service: '.$getProviderPhoneNumber[0]->name.', Extras : '.$serviceExtraName.', Time: '.$getTotalDuration.', Price '.$totalPrice.'.Please go to '.get_site_url().' to confirm the order. User contact: '.$customerPhoneNumber[0]->phone;
			
			/*====Send sms to provider====*/
				
				$TWILIO_SID = get_field('account_sid','options');
				$TWILIO_TOKEN = get_field('auth_token','options');
				$sender_phone_number = get_field('sender_phone_number','options');
				try
				{
					$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
					$response = $client->messages->create(
						$getProviderPhoneNumber[0]->phone,
						array(
							'from' => $sender_phone_number,
							'body' => $providerMsg
						)
					);
					$result['success'] = true;
					$result['msg'] = 'Service ordered successfully';
					$result['url'] = get_site_url().'/thank-you';
				}
				catch (Exception $e) 
				{
					$result['success'] = false;
					$result['msg'] = 'Provider number not added in twilio trail account';

				}
				echo json_encode($result);exit;
		}
		
	}
   	
   	public function send_phone_pin($param)
   	{
		if(isset($param['send_pin_type']) && $param['send_pin_type'] == 'send_phone_pin')
		{
			global $wpdb;
			$length = 5;
			$randomNumber = substr(str_shuffle("0123456789"), 0, $length);
			$to = $param['phone_number'];
			$sender_phone_number = get_field('sender_phone_number','options');
          	$message   = 'Your One time otp password '.$randomNumber;
          	$TWILIO_SID = get_field('account_sid','options');
            $TWILIO_TOKEN = get_field('auth_token','options');
			try
            {
				$checkPhoneNumberUser = "SELECT * FROM wp_amelia_users WHERE phone = ".$param['phone_number'];
				$getCheckPhoneNumberUser = $wpdb->get_results($checkPhoneNumberUser, OBJECT);
				
				if(count($getCheckPhoneNumberUser) > 0)
				{
					$result['success'] = false;
					$result['msg'] = 'Phone number already registered';
				}
				else
				{
					$checkPhoneNumber = "SELECT * FROM wp_user_sms_pin WHERE phone_number = ".$param['phone_number'];
					$getCheckPhoneNumber = $wpdb->get_results($checkPhoneNumber, OBJECT);
					if(count($getCheckPhoneNumber) > 0)
					{
						$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
						
						$response = $client->messages->create(
							$to ,
							array(
								'from' => $sender_phone_number,
								'body' => $message
							)
						);
						$data = array('sms_pin' => $randomNumber,'created_at'=>date('Y-m-d H:i:s'));
						$wpdb->update('wp_user_sms_pin',  $data, array('phone_number'=>$param['phone_number']));
					}
					else
					{
						$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
						$response = $client->messages->create(
							$to ,
							array(
								'from' => $sender_phone_number,
								'body' => $message
							)
						);
						$wpdb->insert('wp_user_sms_pin', array(
							'phone_number' => $param['phone_number'],
							'sms_pin' => $randomNumber, 
						));
					}
					$result['success'] = true;
					$result['msg'] = 'Otp sent';
				}
			} 
            catch (Exception $e) 
            {
            	$result['success'] = false;
                $result['msg'] = 'Otp not sent';

            }
            echo json_encode($result);exit;
        }
	}

   	public function send_login_phone_pin($param)
   	{
		if(isset($param['send_pin_type']) && $param['send_pin_type'] == 'send_login_phone_pin')
		{
			global $wpdb;
			$length = 5;
			$randomNumber = substr(str_shuffle("0123456789"), 0, $length);
			$to = $param['phone_number'];
			$sender_phone_number = get_field('sender_phone_number','options');
          	$message   = 'Your One time otp password '.$randomNumber;
          	$TWILIO_SID = get_field('account_sid','options');
            $TWILIO_TOKEN = get_field('auth_token','options');
			try
            {
				$checkPhoneNumberUser = "SELECT * FROM wp_amelia_users WHERE phone = ".$param['phone_number'];
				$getCheckPhoneNumberUser = $wpdb->get_results($checkPhoneNumberUser, OBJECT);
				
				if(count($getCheckPhoneNumberUser) == 0)
				{
					$result['success'] = false;
					$result['msg'] = 'Phone number not valid';
				}
				else
				{
					$checkPhoneNumber = "SELECT * FROM wp_user_sms_pin WHERE phone_number = ".$param['phone_number'];
					$getCheckPhoneNumber = $wpdb->get_results($checkPhoneNumber, OBJECT);
					if(count($getCheckPhoneNumber) > 0)
					{
						$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
						$response = $client->messages->create(
							$to ,
							array(
								'from' => $sender_phone_number,
								'body' => $message
							)
						);
						$data = array('sms_pin' => $randomNumber,'created_at'=>date('Y-m-d H:i:s'));
						$wpdb->update('wp_user_sms_pin',  $data, array('phone_number'=>$param['phone_number']));
					}
					else
					{
						$client = new Client($TWILIO_SID, $TWILIO_TOKEN);
						$response = $client->messages->create(
							$to ,
							array(
								'from' => $sender_phone_number,
								'body' => $message
							)
						);
						$wpdb->insert('wp_user_sms_pin', array(
							'phone_number' => $param['phone_number'],
							'sms_pin' => $randomNumber, 
						));
					}
					$result['success'] = true;
					$result['msg'] = 'Otp sent';
				}
			} 
            catch (Exception $e) 
            {
            	$result['success'] = false;
                $result['msg'] = 'Otp not sent';

            }
            echo json_encode($result);exit;
        }
   	}
 /*
 * Designs for displaying Notices
 *
 * @since    1.0.0
 * @access   private
 * @var $message - String - The message we are displaying
 * @var $status   - Boolean - its either true or false
 */
public static function admin_notice($message, $status = true) {
    $class =  ($status) ? 'notice notice-success' : 'notice notice-error';
    $message = __( $message, 'sample-text-domain' );

     printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

/**
 * Displays Error Notices
 *
 * @since    1.0.0
 * @access   private
 */
public static function DisplayError($message = "Aww!, there was an error.") {
    add_action( 'admin_notices', function() use($message) {
    self::admin_notice($message, false);
    } );
}

/**
 * Displays Success Notices
 *
 * @since    1.0.0
 * @access   private
 */
public static function DisplaySuccess($message = "Successful!") {
    add_action( 'admin_notices', function() use($message) {
    self::admin_notice($message, true);
    } );
}

}
