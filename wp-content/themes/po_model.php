<?php
class Po_model extends CI_Model
{

	function get_all_data($table_name)
	{
		return $this->db->get_where($table_name)->result_array();
	}

    function get_po($table, $condition)
    {
         $this->db->order_by("id", "dsc");
         $q = $this->db->get_where($table, $condition)->result_array();

        if($q)
        {
            return $q;
        }
    } 	

    function get_categories_sort($table, $condition,$vendorid = NULL )
    {
         // $this->db->order_by("id", "dsc");
         $q = $this->db->order_by('item_code', 'ASC')->get_where($table, $condition)->result_array();
		 //print_r($q);
        if($q)
        {
			if($vendorid['vendorid'] == 2){
				unset($q[0]);
				unset($q[1]);
				unset($q[2]);
				unset($q[3]);
				
			}else if($vendorid['vendorid'] == 1){
				//unset($q[6]);
				unset($q[7]);
				unset($q[8]);
				// if($condition['category_id'] == 2)
				// {
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 2,
				// 					'item_code' => '7734-BTES',
				// 					'item_detail' => 'STRUCTURAL GLASS SUPPORT EARTHSTONE'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 2,
				// 					'item_code' => '7734-BTBL',
				// 					'item_detail' => 'STRUCTURAL GLASS SUPPORT BLACK'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 2,
				// 					'item_code' => '7734-BTMI',
				// 					'item_detail' => 'STRUCTURAL GLASS SUPPORT MILL'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 2,
				// 					'item_code' => '7728-12WH',
				// 					'item_detail' => 'STRUCTURAL GLASS CHAN 151IN WHITE'
				// 				);
				// }
				// elseif($condition['category_id'] == 1)
				// {
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 1,
				// 					'item_code' => '607200WH',
				// 					'item_detail' => '3IN POST CAP WHITE'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 1,
				// 					'item_code' => '607222BL',
				// 					'item_detail' => '3IN SUPPORT POST BLACK 22'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 1,
				// 					'item_code' => '607222ES',
				// 					'item_detail' => '3IN SUPPORT POST EARTHSTONE 22'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 1,
				// 					'item_code' => '607222SD',
				// 					'item_detail' => '3IN SUPPORT POST SAND 22'
				// 				);
				// 	$q[] =  array
				// 				(
				// 					'id' => 'static',
				// 					'category_id' => 1,
				// 					'item_code' => '607222MI',
				// 					'item_detail' => '3IN SUPPORT POST MILL 22'
				// 				);
				// }					
				
			}
        	return $q;
        }
    } 

     public function add_save($table, $data)
    {
        $res = $this->db->insert($table, $data);
        
        if($res)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    

    public function edit_save($data,$id)
    {
        $this->db->where('id',$id);
        $res = $this->db->update('tbl_po',$data);
        if($res)
        {
            return true;
        }
        else
        {
            return false;
        }
    } 
    public function edit_save1($tbl,$data,$condition)
    {
        $this->db->where($condition);
        $res = $this->db->update($tbl,$data);
        if($res)
        {
            return true;
        }
        else
        {
            return false;
        }
    } 

    public function update_table($table_name, $data, $condition)
    {
        $this->db->where($condition);
        $q = $this->db->update($table_name, $data);
        return $q ? true : false;
    }

    public function get_received_items($table_name,$condition)
    {
        $this->db->order_by('rejected_date', 'desc');
        $q = $this->db->get_where($table_name,$condition)->result_array();
        if($q)
        return $q;
    }     

    public function get_received_items_sort($table_name,$condition)
    {
        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($condition);
        $this->db->order_by("date", "desc");
        $this->db->order_by("id", "desc");
        $q = $this->db->get();
        if($q)
        return $q->result_array();
    } 

    public function get_not_received_items($table_name,$condition)
    {
        $this->db->order_by('date', 'desc');
        $q = $this->db->get_where($table_name,$condition)->result_array();
        if($q)
        return $q;
    }    

    public function get_not_received_items_sort($table_name,$condition)
    {

        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($condition);
        $this->db->order_by("date", "desc");
        $this->db->order_by("id", "desc");
        $q = $this->db->get();
        if($q)
        return $q->result_array();
    } 

    public function get_po_name($table_name,$condition)
    {
        $this->db->select('po_number');
        $q = $this->db->get_where($table_name,$condition)->row_array();
        if($q)
        return $q;
    }
    
     public function delete($table_name,$condition)
    {        
         $this->db->where($condition);
         $result = $this->db->delete($table_name); 
         if($result)
         {
            return true;
         }
    }

    
}