<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Beautylivery</title>

        <?php wp_head();?>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/favicon.png" sizes="32x32" type="image/x-icon">

        <?php if(is_page( 'dashboard-appointment' ) || is_page('dashboard-calendar') || is_page('dashboard-profile') || is_page('dashboard-rating')){ ?>
                 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
                <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/css/dashboard.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">

        <?php } else { ?>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
            <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"-->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/css/style.css">

        <?php } ?>
    </head>

    <script>
        var site_url = '<?php echo get_site_url();?>';
    </script>
    <input type="hidden" class="currentLocation" value="<?php echo $_COOKIE['address']; ?>">
    <input type="hidden" class="currentLat" value="<?php echo $_COOKIE['latitude']; ?>">
    <input type="hidden" class="currentLong" value="<?php echo $_COOKIE['longitude']; ?>">
    <?php 
		if(isset($_COOKIE['user_id']))
		{
			$_SESSION['user_id'] = $_COOKIE['user_id'];
		}
        $headerLogo = get_field('header_logo','options');
        $currentUser = wp_get_current_user();
		
		$getLoggedInUserDetail = '';
        if($_SESSION['user_id'] != '')
        {
            $getLoggedInUserDetail = getLoggedInUserDetail($_SESSION['user_id']);
        }
    ?>

    <div class="preloader">
        <div class="loader">
            <img src="<?php echo get_template_directory_uri()?>/assets/images/loader.gif" alt="logo" />
        </div>
    </div>
    <body>
		<?php if(! (is_page( 'dashboard-appointment' )) && ! (is_page('dashboard-calendar')) && ! (is_page('dashboard-rating')) && ! (is_page('dashboard-profile'))){ ?>
			<header class="header <?php if(is_page(8) || is_page(157)){ echo 'transparent-header';}?>">
				<div class="container">
					<div class="navbar-header header-left-sec">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
							<span class="navbar-toggler-icon"></span>
						</button>
						<a class="navbar-brand" href="<?php echo get_site_url();?>">
							<img src="<?php echo $headerLogo['url'];?>" alt="logo" />
						</a>
					</div>

					<div class="header-right-sec">
                    
                    <?php
												
						if ($_SESSION['user_id'] != '' && $getLoggedInUserDetail[0]->type == 'customer' && $currentUser->ID == 0)
						{
							if($getLoggedInUserDetail[0]->pictureFullPath == '')
                            {
                                $profileImg = get_template_directory_uri().'/assets/images/profile.jpg';
                            }
                            else
                            {
                                $profileImg = $getLoggedInUserDetail[0]->pictureFullPath;
                            }
                    ?>
							<div class="porfile_bx dropdown">
								<figure style="background-image:url(<?php echo $profileImg;?>)"></figure>
								<a class="dropdown-toggle" href="<?php echo get_site_url();?>" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $getLoggedInUserDetail[0]->firstName;?></a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="<?php echo get_site_url().'/dashboard';?>">Dashboard</a>
									<a class="dropdown-item" href="<?php echo get_site_url().'/logout';?>">Logout</a>
								</div>
							</div>
							
                    <?php 
						}
                        else if($currentUser->ID != 0) 
                        {
							$currentUserAvatar = get_avatar_url( $currentUser->ID );
                            if($currentUserAvatar == '')
                            {
                                $profileImg = get_template_directory_uri().'/assets/images/profile.jpg';
                            }
                            else
                            {
                                $profileImg = $currentUserAvatar;
                            }
                    ?>
                            <div class="porfile_bx dropdown">
                                <figure style="background-image:url(<?php echo $profileImg; ?>)"></figure>
                                <a class="dropdown-toggle" href="<?php echo admin_url();?>" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $currentUser->display_name;?></a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="<?php echo admin_url().'admin.php?page=wpamelia-appointments#/appointments';?>">Dashboard</a>
                                    <a class="dropdown-item" href="<?php echo wp_logout_url(get_site_url());?>">Logout</a>
                                </div>
                            </div>
							<?php //echo do_shortcode( '[wpml_translate]' ); ?>
                    <?php   
                        }
						else
						{
					?>
							<ul class="login-wrap">
								<li>
									<a href="javascript:void(0)" data-attr="professional" id="sign_up_btn">Register as professional</a>
								</li>
								<li>
									<a href="javascript:void(0)" id="login_modal">Log in</a>
								</li>
							</ul>
							<?php echo do_shortcode( '[wpml_translate]' ); ?>
					<?php } ?>
                </div>
            </div>
        </header>
		
		<?php } ?>

		


    

