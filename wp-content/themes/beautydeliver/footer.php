<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
// $ip = '122.173.233.254';
 // $details = json_decode(file_get_contents("http://ip-api.com/json/"));
 // $countryName = $details->country;
 if(isset($_COOKIE['user_id']))
 {
	$currentUserLocation = getLoggedInUserDetail($_SESSION['user_id']);
 }

?>

<?php if(!(is_page( 'dashboard-appointment' )) && !(is_page('dashboard-calendar')) && !(is_page('dashboard-rating')) && !(is_page('dashboard-profile'))){ ?>



    <section class="signup-wrapper section" style="background-image: url('<?php echo get_template_directory_uri()?>/assets/images/signup-bg.jpg');">
    	<div class="container">
			<?php if($_SESSION['user_id'] == ''){ ?>
				<div class="sec-title">
					<h2>Sign-Up For Beautylivery Today</h2>
				</div>
			
				<a href="javascript:void(0)" class="signup theme-btn" data-attr="customer" id="sign_up_btn">Sign Up</a>
				<a class="reg" href="javascript:void(0)" data-attr="customer" id="sign_up_btn">Register as professional</a>
			<?php } ?>

    	</div>
    </section>

<!-- signup-wrapper -->

<footer class="footer-wrapper">
    <div class="container">
        <div class="footer-left">
            <span><?php the_field('copyright_text','options');?></span>
        </div>
        <div class="footer-right">
            
            <?php $menu = wp_nav_menu( array( 'theme_location' => 'footer') ); ?>
               
        </div>
    </div>
</footer>

<?php } ?>

<!-- footer-wrapper -->

<!-- Modal -->
<div class="popup" id="login_popup">

    <div class="popup-inner">
        <div class="popup-inner-elem">
            <div class="popup-logo">
                <a href="<?php echo get_site_url();?>">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/logo.png" alt="logo" />
                </a>
            </div>
            <div class="title">
                <h3>Login to continue</h3>
            </div>
            <div class="content">
                <form id="amelia_login" method="post">
                    <div class="form-group btn-rel">

                        <input type="tel" id="" name="amelia_phone_number" class="form-control amelia_phone_number" placeholder="PHONE NO.">

                        <button type="button" class="send_pin" data-attr="amelia_login">send pin</button>
                    </div>
                    <div class="form-group">
                        <input type="tel" id="tel1" name="phone_pin" class="form-control onlyvalidNumber" placeholder="PHONE PIN">
                    </div>
                    <div class="new-number">
                        <a href="javascript:void(0)" id="sign_up_btn" data-attr="professional">New phone number?</a>
                    </div>
                    <div class="button-sec">
                        <button type="submit" class="button">Login</button>
                    </div>
                </form>
                <div class="already-account">
                    <span>Don’t have an account? - <a href="javascript:void(0)" id="sign_up_btn" data-attr="professional">Sign Up</a></span>
                </div>
            </div>
            <a href="javascript:void(0)" class="popup-close login_sign_up_close" data-attr="login_close">
                <svg width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                    <g>
                        <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                    </g>
                </svg>
            </a>
        </div>
    </div>
</div>


<div class="popup" id="professional_modal">
    <div class="popup-inner">
        <div class="popup-inner-elem">
            <div class="popup-logo">
                <a href="<?php echo get_site_url();?>">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/logo.png" alt="logo" />
                </a>
            </div>
            <div class="title">
                <h3 id="sign_up_type"></h3>
            </div>
            <div class="content">
                <form id="amelia_sign_up" method="POST" autocomplete="off">
                    <input type="hidden" name="role" class="user_role">
                    <div class="form-group">
                        <input type="text" id="name" name="firstName" class="form-control firstCap alphabets" placeholder="ENTER FIRST NAME" maxlength="25">
                    </div>
                    <div class="form-group">
                        <input type="text" id="name" name="lastName" class="form-control firstCap alphabets" placeholder="ENTER LAST NAME" maxlength="25">
                    </div>
                    <div class="form-group">
                        <input type="email" id="user_email" name="email" class="form-control email" placeholder="ENTER EMAIL">
                    </div>
                    <div class="form-group">
						<input type="hidden" name="address_latitude" class="address_latitude">
						<input type="hidden" name="address_longitude" class="address_longitude">
                        <input type="text" id="address" name="address" class="form-control" placeholder="ADDRESS" autocomplete="off">
                    </div>
                    <div class="form-group btn-rel">
                        <input type="tel" id="" name="phone_number" class="form-control amelia_phone_number" placeholder="PHONE NO.">

                        <button type="button" class="send_pin" data-attr="amelia_sign_up">send pin</button>
                    </div>
                    <div class="form-group">
                        <input type="tel" id="phone_pin" name="phone_pin" class="form-control onlyvalidNumber" placeholder="PHONE PIN">
                    </div>
                    <div class="button-sec">
                        <button type="submit" class="button">Sign up</button>
                    </div>
                </form>
                <div class="already-account">

                    <span id="or_apply_as"></span>
                    <span>Already have an account?<a href="javascript:void(0)" id="login_modal">Login</a></span>

                   
                </div>
            </div>
                <a class="popup-close login_sign_up_close" data-popup-close="Professional" href="javascript:void(0)">
                    <svg width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                        <g>
                            <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                        </g>
                    </svg>
                </a>
        </div>
    </div>
</div>

<!--Location modal-->

    <div class="modal fade" id="location_pop_up" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Set Location</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form_control" id="currentLocation" placeholder="Search location">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<!-- Cancel Appointment popup -->

<div class="modal fade" id="cancelModal" role="dialog">
    <div class="modal-dialog">
        <form id="cancel_apppointment" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Cancel Appointment</h4>
                </div>
                <div class="modal-body">
                   
                        <input type="hidden" class="appointId" name="appointmentId">
                        <input type="hidden" name="status" value="canceled">
                        <input type="hidden" name="cancel_appointment_by_customer" value="cancel_appointment">
						
                    
                        <p>Are you sure you want to cancel this appointment?</p>
                    
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Feedback for service popup -->

<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form id="service_review" method="POST" class="form-horizontal" role="form">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        Review
                    </h4>
                </div>
                <div class="modal-body">
				
					<div class="review_service_detail"></div>
                    
                    <input type="hidden" name="rating" class="rating">
                    <input type="hidden" name="appointmentId" class="appointmentId">
                    <input type="hidden" name="serviceId" class="serviceId">
                        <div class="form-group">
                            <label  class="control-label" for="inputEmail3">Rating</label>
                            <div class='starrr' id='star1'></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"for="inputPassword3" >Comment</label>
                            <textarea class="form-control" rows="5" name="comment" placeholder="Comment"></textarea>
                        </div>
                        <div class="form-group upload-image ">
                            <label class="control-label"for="inputPassword3">Image Upload</label>
                            <div class="relative">
								<input type="text" id="uploadFile" placeholder="Attach Image">
								<input type="file" name="service_images[]" multiple>
							</div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">
                        Submit
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


<?php echo wp_footer();?>

        <script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/popper.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/aos.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/wow.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/slick.js"></script>
		<script src="<?php echo get_template_directory_uri()?>/assets/js/lightbox.js"></script>
		<script src="<?php echo get_template_directory_uri()?>/assets/js/owl.carousel.min.js"></script>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js'></script>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js'></script>
		
		<?php if($countryName == 'CN') { ?>
			<script src="https://maps.google.cn/maps/api/js?key=AIzaSyBTBvhmym9yWEFYTcov-6iMtgfoDVDljBI&libraries=places"></script> 
		<?php } else{ ?>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTBvhmym9yWEFYTcov-6iMtgfoDVDljBI&libraries=places"></script> 
		<?php } ?>
		
		<script src="<?php echo get_template_directory_uri()?>/assets/js/form-validate.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/bootstrap-datetimepicker.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/bootstrap-slider.min.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/starrr.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/custom.js"></script>
        <script src="<?php echo get_template_directory_uri()?>/assets/js/my-custom.js"></script>
    
    </body>
</html>
<script>
var maps;

/*=============get current location with geolocation=============*/
				// if(countryName == 'CN')
			// {
				// jQuery('#location_pop_up').modal('show');
			// }
			// else
			// {


	// $.getJSON("http://ip-api.com/json/", function (data) {
        // var country = data.country_name;//your country
        // console.log(country);
        
    // });
	
	jQuery( document ).ready(function() {
		getCurrentLocation();
		
	});		
	jQuery(document).on('click','.getCurrentLocation',function(){
		
		jQuery.removeCookie("latitude", "", { path: '/' });
		jQuery.removeCookie("longitude", "", { path: '/' });
		jQuery.removeCookie("address", "", { path: '/' });
		jQuery.cookie("current_location", 'yes', { path: '/' });
		if(jQuery.cookie('user_id') != undefined)
		{
			jQuery.cookie("my_current_location", 'yes', { path: '/' });
		}
		getCurrentLocation();
		location.reload();
	});
	
	jQuery(document).on('click','.getMyLocation',function(){
		
		jQuery.removeCookie("latitude", "", { path: '/' });
		jQuery.removeCookie("longitude", "", { path: '/' });
		jQuery.removeCookie("address", "", { path: '/' });
		jQuery.removeCookie("current_location", "", { path: '/' });
		if(jQuery.cookie('user_id') != undefined)
		{
			jQuery.cookie("my_current_location", 'yes', { path: '/' });
		}
		getCurrentLocation();
		location.reload();
	});
	
	function getCurrentLocation()
	{
		if(jQuery.cookie('user_id') == undefined || jQuery.cookie('current_location') != undefined) 
		{ 
			//var countryName = '<?php echo $countryName;?>';
			var x = document.getElementById("demo");
			if (navigator.geolocation) 
			{
				 
				navigator.geolocation.getCurrentPosition(showPosition);
			} 
			else 
			{ 
				x.innerHTML = "Geolocation is not supported by this browser.";
			}
			function showPosition(position) 
			{
				
				var  lat = position.coords.latitude;
				var lng = position.coords.longitude;
				codeLatLng(lat, lng);
			}
			function codeLatLng(lat, lng) 
			{
				
				if(jQuery.cookie('latitude') == undefined && jQuery.cookie('longitude') == undefined)
				{
					jQuery.cookie("latitude", lat);
					jQuery.cookie("longitude", lng);
					jQuery('.currentLat').val(lat);
					jQuery('.currentLong').val(lng);
					jQuery('.searchLat').val(lat);
					jQuery('.searchLong').val(lng);
					getPopularServices(lat,lng);
					getRecentServices(lat,lng);
					getPopularProfessionals(lat,lng);
					getNewProfessionals(lat,lng);
				}
				else
				{
					jQuery('.currentLat').val(jQuery.cookie("latitude"));
					jQuery('.currentLong').val(jQuery.cookie("longitude"));
					jQuery('.searchLat').val(jQuery.cookie("latitude"));
					jQuery('.searchLong').val(jQuery.cookie("longitude"));
					getPopularServices(jQuery.cookie("latitude"),jQuery.cookie("longitude"));
					getRecentServices(jQuery.cookie("latitude"),jQuery.cookie("longitude"));
					getPopularProfessionals(jQuery.cookie("latitude"),jQuery.cookie("longitude"));
					getNewProfessionals(jQuery.cookie("latitude"),jQuery.cookie("longitude"));
					
				}
				getCategories();
				searchCat();
				
				var geocoder= new google.maps.Geocoder();
				var latlng = new google.maps.LatLng(lat, lng);
				geocoder.geocode({'latLng': latlng}, function(results, status) 
				{
					if (status == google.maps.GeocoderStatus.OK) 
					{
						if (results[1]) 
						{
							if(jQuery.cookie('address') == undefined)
							{
								jQuery.cookie("address", results[0].formatted_address, { path: '/' });
								jQuery('.currentLocation').val(results[0].formatted_address);
								jQuery('#location-test').val(results[0].formatted_address);
								jQuery('.search-change-location').val(results[0].formatted_address);
							}
							else
							{ 
								jQuery('.currentLocation').val(jQuery.cookie("address"));
								jQuery('#location-test').val(jQuery.cookie("address"));
								jQuery('.search-change-location').val(jQuery.cookie("address"));
							}
						} 
						else 
						{
							toastr.error('No Location Found');
						}
					} 
					else 
					{
						toastr.warning("Geocoder failed due to: " + status);
					}
				});
			}
		}
		else if(jQuery.cookie('user_id') != undefined && jQuery.cookie('current_location') == undefined)
		{
			jQuery('.currentLat').val('<?php echo $currentUserLocation[0]->latitude;?>');
			jQuery('.currentLong').val('<?php echo $currentUserLocation[0]->longitude;?>');
			jQuery('.currentLocation').val('<?php echo $currentUserLocation[0]->address;?>');
			jQuery('.searchLat').val('<?php echo $currentUserLocation[0]->latitude;?>');
			jQuery('.searchLong').val('<?php echo $currentUserLocation[0]->longitude;?>');
			jQuery('.search-change-location').val('<?php echo $currentUserLocation[0]->address;?>');
			jQuery.cookie("latitude", '<?php echo $currentUserLocation[0]->latitude;?>', { path: '/' });
			jQuery.cookie("longitude", '<?php echo $currentUserLocation[0]->longitude;?>', { path: '/' });
			jQuery.cookie("address", '<?php echo $currentUserLocation[0]->address;?>', { path: '/' });
			jQuery('#location-test').val('<?php echo $currentUserLocation[0]->address;?>');
			getPopularServices('<?php echo $currentUserLocation[0]->latitude;?>','<?php echo $currentUserLocation[0]->longitude;?>');
			getRecentServices('<?php echo $currentUserLocation[0]->latitude;?>','<?php echo $currentUserLocation[0]->longitude;?>');
			getPopularProfessionals('<?php echo $currentUserLocation[0]->latitude;?>','<?php echo $currentUserLocation[0]->longitude;?>');
			getNewProfessionals('<?php echo $currentUserLocation[0]->latitude;?>','<?php echo $currentUserLocation[0]->longitude;?>');
			getCategories();
			searchCat();
		}
	}
	
/*======get services according to location============*/
	
	function getPopularServices(lat,lng)
	{
		jQuery.ajax({
			url: site_url+'/wp-admin/admin-ajax.php',
			type: "POST",
			data: {action:'getLocationServices',lat:lat,lng:lng,type:'popular'},
			dataType: "html",
			success: function(response) {
				jQuery('.getPopularServices').html(response);
				var serviceslider = jQuery('body').find('.service-slider');
				serviceslider.owlCarousel({
					loop:false,
					margin: 30,
					nav:true,
					autoPlay : true,
					dots:false,
					navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
					responsive:{
						0:{
							items:1
						},
						600:{
							items:3,
						},
						1000:{
							items:4,
						}
					}
				});
			}
		});
	}
	
/*======get recent services according to location============*/
	
	function getRecentServices(lat,lng)
	{
		jQuery.ajax({
			url: site_url+'/wp-admin/admin-ajax.php',
			type: "POST",
			data: {action:'getLocationServices',lat:lat,lng:lng,type:'recent'},
			dataType: "html",
			success: function(response) {
				jQuery('.getRecentServices').html(response);
				var serviceslider = jQuery('body').find('.service-slider');
				serviceslider.owlCarousel({
					loop:false,
					margin: 30,
					nav:true,
					autoPlay : true,
					dots:false,
					navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
					responsive:{
						0:{
							items:1
						},
						600:{
							items:3,
						},
						1000:{
							items:4,
						}
					}
				});
			}
		});
	}
	
/*===========get popular professional according to location=========*/
	function getPopularProfessionals(lat,lng)
	{
		jQuery.ajax({
			url: site_url+'/wp-admin/admin-ajax.php',
			type: "POST",
			data: {action:'getLocationProfessionals',lat:lat,lng:lng,type:'popular'},
			dataType: "html",
			success: function(response) {
				jQuery('.getPopularProfessionals').html(response);
				var serviceslider = jQuery('body').find('.service-slider');
				serviceslider.owlCarousel({
					loop:false,
					margin: 30,
					nav:true,
					autoPlay : true,
					dots:false,
					navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
					responsive:{
						0:{
							items:1
						},
						600:{
							items:3,
						},
						1000:{
							items:4,
						}
					}
				});
			}
		});
	}
	
/*===========get new joined professional according to location=========*/
	function getNewProfessionals(lat,lng)
	{
		jQuery.ajax({
			url: site_url+'/wp-admin/admin-ajax.php',
			type: "POST",
			data: {action:'getLocationProfessionals',lat:lat,lng:lng,type:'newjoined'},
			dataType: "html",
			success: function(response) {
				jQuery('.getNewProfessionals').html(response);
				var serviceslider = jQuery('body').find('.service-slider');
				serviceslider.owlCarousel({
					loop:false,
					margin: 30,
					nav:true,
					autoPlay : true,
					dots:false,
					navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
					responsive:{
						0:{
							items:1
						},
						600:{
							items:3,
						},
						1000:{
							items:4,
						}
					}
				});
			}
		});
	}
	

/*============search location when country china=============*/
	var chinaAutocomplete = new google.maps.places.Autocomplete(jQuery("#currentLocation")[0], {});
		google.maps.event.addListener(chinaAutocomplete, 'place_changed', function() {
		var place = chinaAutocomplete.getPlace();
        var Latitude = place.geometry.location.lat();
        var latprecision =  Math.pow(10, 6);
		var new_Latitude = (latprecision * Latitude)/latprecision;
		
		var Longitude = place.geometry.location.lng();
		var lngprecision =  Math.pow(10, 6);
		var new_Longitude = (lngprecision * Longitude)/lngprecision;

        jQuery.cookie("latitude", new_Latitude, { path: '/' });
        jQuery.cookie("longitude", new_Longitude, { path: '/' });
        jQuery.cookie("address", jQuery('#location-test').val(), { path: '/' });
        jQuery('.currentLat').val(new_Latitude);
        jQuery('.currentLong').val(new_Longitude);
        jQuery('.currentLocation').val(jQuery('#location-test').val());
    });
	
/*============sign up address location=============*/
	var signupAutocomplete = new google.maps.places.Autocomplete(jQuery("#address")[0], {});
	google.maps.event.addListener(signupAutocomplete, 'place_changed', function() {
		var place = signupAutocomplete.getPlace();
        var Latitude = place.geometry.location.lat();
        var latprecision =  Math.pow(10, 6);
		var new_Latitude = (latprecision * Latitude)/latprecision;
		
		var Longitude = place.geometry.location.lng();
		var lngprecision =  Math.pow(10, 6);
		var new_Longitude = (lngprecision * Longitude)/lngprecision;

        jQuery('.address_latitude').val(new_Latitude);
        jQuery('.address_longitude').val(new_Longitude);
    });
</script>


	
