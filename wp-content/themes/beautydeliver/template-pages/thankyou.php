<?php 
/*
Template Name: thankyou
*/
if(isset($_GET) && $_GET['orderId'] != '')
{
	$getBookingDetail = $_SESSION['bookingDetails'];
	$getProfessionalDetail = getProvideruserDetail($getBookingDetail['providerId']);
}
else
{
	wp_redirect( get_site_url() );
	exit;
}
get_header();
?>
<div class="main">
    <section class="thankyou-wrapper">
        <div class="container">
            <div class="thankyou-section">
                <div class="icon">
                    <figure>
                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                    </figure>
                </div>
                <div class="title">
                    <h3>Thank you for your order!</h3>
                </div>
                <!-- title -->
                <div class="content">
                    <p><?php echo ucfirst($getProfessionalDetail[0]->firstName).' '.ucfirst($getProfessionalDetail[0]->lastName);?> is informed of your order and we await the confirmation. <br> We will inform you as soon as we have the order confirmation.</p>
                </div>
                <!-- content -->
                <div class="thankyou-btn">
                    <a href="<?php echo get_site_url();?>" class="theme-btn">Thank you</a>
                </div>
                <!-- thankyou-btn -->
            </div>
            <!-- thankyou-section -->
        </div>
        <!-- container -->
    </section>
    <!-- thankyou-wrapper -->
</div>
<!-- main -->

<?php 
get_footer();

?>

