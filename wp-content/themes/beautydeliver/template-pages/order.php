<?php 
    /*
    Template Name: order
    */
	
	get_header();
	$latitude = $_COOKIE['latitude'];
    $longitude = $_COOKIE['longitude'];
	
	$userId = $_SESSION['user_id'];
	$getCustomerId = getLoggedInUserDetail($userId);
	
	$getBookingDetail = $_SESSION['bookingDetails'];
	
	$getProviderDetail = getProvideruserDetail($getBookingDetail['providerId']);
	$providerRange = $getProviderDetail[0]->rangeWilling;
	
	$getServiceDetail = getServiceDetail($getBookingDetail['serviceId']);
	
	$profWeekDays = getProfessionalWeekDays($getServiceDetail[0]->profId);
	$profdayOff = getProfDayOff($getServiceDetail[0]->profId);
	$profdayOffForHighlight = getProfDayOffNew($getServiceDetail[0]->profId);
	
	
	$getExtraServiceDetail = extraServiceDetail($getBookingDetail['extra_service']);
	$customerDetail  = getCustomerDetail($getCustomerId[0]->id);
	
	/*calculate distance*/
	$point1 = array("lat" => $latitude, "long" => $longitude); 
    $point2 = array("lat" => $getServiceDetail[0]->latitude, "long" => $getServiceDetail[0]->longitude); 
    $km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']);
	
	$bookingDateForCalendar = date('Y, m, d',strtotime($getBookingDetail['bookingDate']));
	
	/*get total extra service duration and price*/
	$extraServiceDuration = '';
	$extraServicePrice = '';
	if(count($getExtraServiceDetail)>0)
	{
		foreach($getExtraServiceDetail as $val)
		{
			$extraServiceDuration += $val->duration;
			$extraServicePrice += $val->price;
		}
	}
	$totalServiceDuration = $getServiceDetail[0]->duration+$extraServiceDuration;
	$totalServicePrice = $getServiceDetail[0]->price+$extraServicePrice;
	
	/*get booking time slot*/
	$date = $getBookingDetail['booking_date'];
	$data = array(
		'selectedDate'=>$getBookingDetail['booking_date'],
		'weekDay'=>date('w', strtotime($date)),
		'serviceId'=>$getBookingDetail['serviceId'],
		'duration'=>$totalServiceDuration,
		'profId' => $getBookingDetail['providerId']
	);
	$getTodayTimeSlot = getBookingTimeSlot($data);
	
	/*get service image*/
	if($getServiceDetail[0]->pictureFullPath != '')
	{
		$serviceImg = $getServiceDetail[0]->pictureFullPath;
	}
	else
	{
		$getImg = get_field('header_logo','options');
		$serviceImg = $getImg['url'];
	}
?>
<div class="main">
    <section class="order-wrapper section">
        <div class="container">
            <div class="sec-title">
                <h2>Order</h2>
            </div>
            <!-- sec-title -->
            <div class="order-info-wrap">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="order_item left_sec">
                            <figure style="background-image: url('<?php echo $serviceImg; ?>');"></figure>
                            <div class="top_sec">
                                <div class="title">
                                    <h4><?php echo $getServiceDetail[0]->name;?></h4>
                                    <span>By <?php echo $getServiceDetail[0]->firstName.' '.$getServiceDetail[0]->lastName;?></span>
                                </div>
                                <!-- title -->
                                <div class="range">
                                    <span>
                                        <?php 

                                            if($getServiceDetail[0]->totalRatingSum != '' && $getServiceDetail[0]->totalRatingSum != 0)
                                            {
                                                $totalRating = $getServiceDetail[0]->totalRatingSum/$getServiceDetail[0]->totalRating;
                                            }
                                            else
                                            {
                                                $totalRating = 0;
                                            }

                                            echo round($totalRating,1);
                                        ?>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <!-- range -->  
                            </div>
                            <!-- top_sec -->
                            <div class="bottom_sec">
                                <ul>
                                    <li>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <?php
                                            $duration = convertIntoMin($totalServiceDuration);
                                            echo $duration;
                                        ?> 
                                    </li>
                                    <li>
                                        <?php
                                            echo the_field('currency','option').' '.number_format($totalServicePrice,2);
                                        ?>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <?php echo $km;?> km
                                    </li>
                                </ul>
                            </div>
                            <!-- bottom_sec -->
                        </div>
                        <!-- order_item -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="order_item right_sec">
                            <h5>Additional options:</h5>
                            <ul>
                                <?php 
                                    if(count($getExtraServiceDetail) > 0)
                                    {
                                        foreach($getExtraServiceDetail as $val)
                                        {
                                            echo '<li>'.$val->name.'</li>';
                                        }
                                    }
                                    else
                                    {
                                        echo '- None';
                                    }

                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- row -->
            </div>
            <!-- order-info-wrap -->
            <div class="order-progressbar-wrap">
                <ul class="order-progressbar">
                    <li class="first-li active">
                        <span>1</span>
                    </li>
                    <li class="second-li">
                        <span>2</span>
                    </li>
                    <li class="third-li">
                        <span>3</span>
                    </li>
                </ul>
                <ul class="order-content">
                    <li class="active">
                        <cite><i class="fa fa-clock-o" aria-hidden="true"></i>Time</cite>
                    </li>
                    <li>
                        <cite><i class="fa fa-map-marker" aria-hidden="true"></i>Location</cite>
                    </li>
                </ul>
            </div>

            <!-- Select date time -->

            <div id="order-calender-wrap" class="order-content-wrap">
                <div class="title">
                    <h3>Please select your date and time</h3>
				</div>
				<div class="row">
					<div class="col-lg-7 col-md-7 col-xs-12">

					   <div class="availability-wrap">
							<div id="appointment_datepicker"></div>
						</div>
					</div>
					<div class="col-lg-5 col-md-5 col-xs-12">
						<div class="time-wrap">
							<div class="sec">
								<h5>Time</h5>
							</div>
							<ul class="selected_date_time_slot">
								<?php
									if(count($getTodayTimeSlot > 0))
									{
										$getBookingSlotTime = $getBookingDetail['booking_time'];
										foreach($getTodayTimeSlot as $timeSlot) 
										{
											if($timeSlot == $getBookingSlotTime)
											{
												$class = "active";
											}
											else
											{
												$class = "";
											}
											echo '<li class="'.$class.'"><a href="javascript:void(0)" class="book-service-time-slot" data-attr="'.$timeSlot.'">'.$timeSlot.'</a></li>'; 
										}
									}
									else
									{
										echo '<li>No time slot available...</li>'; 
									} 
								?>
							</ul>
						</div>
					</div>
				</div>
                
                <div class="order-btn-wrap">
                    <ul>
                        <li>
                            <a href="<?php echo get_site_url().'/service-detail/?serviceId='.$_SESSION['bookingDetails']['serviceId'];?>">Cancel</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="confirm-location">next</a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Confirm location -->

            <div id="order-location-wrap" class="order-content-wrap confirm-order-location" style="display:none;">
                <div class="title">
                    <h3>Please confirm your location and contact number</h3>
                </div>
                <form id="confirm_location" method="POST">
					<div class="form-group">
						<label>Name on the Bell:</label>
						
						<input class="form-control" type="text" placeholder="Name" name="name" value="<?php if($getBookingDetail['customer_name'] != ''){ echo $getBookingDetail['customer_name'];}else{ echo $customerDetail['name']; }?>">
                    </div>
					<div class="form-group">
						<label>Address:</label>
						
						<input type="hidden" name="customer_address" class="change_customer_address" value="<?php if($getBookingDetail['customer_address'] != ''){ echo $getBookingDetail['customer_address'];}else{echo $_COOKIE['address'];}?>">
						
						<input type="hidden" name="customer_address_latitude" class="customer_address_latitude" value="<?php if($getBookingDetail['customer_address_latitude'] != ''){ echo $getBookingDetail['customer_address_latitude'];}else{echo $_COOKIE['latitude'];}?>">
						
						<input type="hidden" name="customer_address_longitude" class="customer_address_longitude" value="<?php if($getBookingDetail['customer_address_longitude'] != ''){ echo $getBookingDetail['customer_address_longitude'];}else{echo $_COOKIE['longitude'];}?>">
						
						<span class="confirm_location_address_span">
						<?php 
							if($getBookingDetail['customer_address'] != '')
							{ 
								echo $getBookingDetail['customer_address'];
							}
							else
							{
								echo $_COOKIE['address'];
							}
					
						?>
						</span>
						<a href="javascript:void(0)" class="popup-close change-location" data-popup-open="changeloc_Modal">Change Location</a>
                    </div>
                    <div class="form-group">
						<label>Additional:</label>
						
						<input type="hidden" name="additional_address" class="change_customer_additional_address" value="<?php if($getBookingDetail['customer_additional_address'] != ''){ echo $getBookingDetail['customer_additional_address'];}?>">
						
						<span class="additional_address_span">
						<?php 
							if($getBookingDetail['customer_additional_address'] != '')
							{ 
								echo $getBookingDetail['customer_additional_address'];
							}
						?>
						</span>
                        
                    </div>
                
                    <div class="order-btn-wrap">
                        <input type="hidden" name="bookingId" value="<?php echo $getBookingDetail[0]->bookingId;?>">
                        <ul>
                            <li>
                                <a href="javascript:void(0)" class="confirm-time">Back</a>
                            </li>
                            <li>
                                <input type="submit" value="Next">
                            </li>
                        </ul>
                    </div>
                </form>
                
            </div>

            <!-- Order Confirm -->
			
			<form method="POST" id="order_now_confirm">

            <div id="order-location-wrap" class="order-content-wrap confirm-order-detail" style="display:none;">
                <div class="title">
                    <h3>Please confirm</h3>
                </div>
                <div class="order-confermation-wrap">
                    <div class="oredr_item">
                        <h5>Address:</h5>
                        <ul>
                            <li>
                                <figure>
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon1.png');" alt="featured-thumb" />
                                </figure>
                                <span class="address_cust_name"><?php echo $getBookingDetail['customer_name'];?></span>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon2.png');" alt="featured-thumb" />
                                </figure>
                                <span class="address_cust_addrress">
									<?php echo $getBookingDetail['customer_address'];?>
								</span>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon3.png');" alt="featured-thumb" />
                                </figure>
                                <a href="tel:<?php echo $customerDetail['phone'];?>" class="address_cust_phone"><?php echo $customerDetail['phone'];?></a>
                            </li>
							
							<li class="additional_address_li" <?php if($getBookingDetail['customer_additional_address'] == ''){ echo 'style="display:none"';}?>>
								<figure>
									<img src="<?php echo get_template_directory_uri()?>/assets/images/icon4.png');" alt="featured-thumb" />
								</figure>
								<span class="address_cust_additional"><?php echo $getBookingDetail['customer_additional_address'];?></span>
							</li>
								
							
                            
                        </ul>
                    </div>
					
                    <div class="order_item">
						
                        <h5>Time:</h5>
                        <ul>
                            <li>
                                <figure>
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon5.png');" alt="featured-thumb" />
                                </figure>
                                <span>
                                    <?php 
                                        $data = '';
                                        $currentDate = date('d.m.Y');
                                        $bookingDate = date('d.m.Y',strtotime($getBookingDetail['booking_date']));
                                        if($bookingDate == $currentDate)
                                        {
                                            $getDate .= 'Today - ';
                                        }
                                        $getDate .= date('l', strtotime($bookingDate)).' - '.$bookingDate;
                                        
                                        echo $getDate;
                                    ?>
                                   
                                </span>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon6.png');" alt="featured-thumb" />
                                </figure>
                                <span>
                                    <?php 
                                        echo $getBookingDetail['booking_time'];
                                    ?>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="order_item">
                        <h5>Payment:</h5>
                        <ul>
                            <li>
                                <figure>
                                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon7.png');" alt="featured-thumb" />
                                </figure>
                                <span>Cash on site</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="order-btn-wrap">
                    <ul>
                        <li>
                            <a href="javascript:void(0);" class="confirm-location">Back</a>
                        </li>
                        <li>
						
							<input type="hidden" name="send_order_notificatiom" value="<?php echo 'yes';?>">
                            <input type="submit" value="Order Now" class="order_now_confirm">
						
                        </li>
                    </ul>
                </div>
			</div>
			</form>
        </div>
    </section>
</div>

<!-- change location popup -->

<div class="popup confirm-popup" data-popup="changeloc_Modal">
    <div class="popup-inner">
        <div class="popup-inner-elem">
            <div class="popup-logo">
                <a href="<?php echo get_site_url();?>">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/logo.png" alt="logo" />
                </a>
            </div>
            <div class="title">
                <h3>Change location</h3>
            </div>
			<div class="content">
                <form method="POST" autocomplete="off">
                    <div class="form-group">
                        <label>Address</label>
						<input type="hidden" class="valid_location" value="<?php if($getBookingDetail['customer_address'] != ''){ echo $getBookingDetail['customer_address'];}else{echo $_COOKIE['address'];}?>">
						<input type="hidden" class="customer_lat" value="<?php if($getBookingDetail['customer_address_latitude'] != ''){ echo $getBookingDetail['customer_address_latitude'];}else{echo $_COOKIE['latitude'];}?>">
						<input type="hidden" class="customer_long" value="<?php if($getBookingDetail['customer_address_longitude'] != ''){ echo $getBookingDetail['customer_address_longitude'];}else{echo $_COOKIE['longitude'];}?>">
                        <input type="text" id="order_address" class="form-control" placeholder="Search Address" value="<?php if($getBookingDetail['customer_address'] != ''){ echo $getBookingDetail['customer_address'];}else{echo $_COOKIE['address'];}?>">
                    </div>
                    <div class="form-group">
					    <label>Address Additional</label>
                        <input type="text" id="address_additional" name="area" class="form-control" placeholder="ADDRESS ADDITIONAL" value="<?php echo $getBookingDetail['customer_additional_address'];?>">
                    </div>
					<div class="button-sec">
                        <button type="button" class="button update-confirm-location">Update</button>
                    </div>
                </form>
            </div>
                <a class="popup-close login_sign_up_close" data-popup-close="changeloc_Modal" href="javascript:void(0)">
                    <svg width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                        <g>
                            <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                        </g>
                    </svg>
                </a>
        </div>
    </div>
</div>

<?php get_footer(); ?>

<script>

	/*=======Highlight booking date for calendar=======*/
	
	/*============Booking Datepicker select current date and booking selected date==============*/

    // jQuery(function() {
		 
        // jQuery('#appointment_datepicker').datetimepicker({
            // inline: true,
            // sideBySide: true,
            // format: 'YYYY-MM-DD',
            // minDate: date,
            // maxDate: date,
		// });
        // jQuery('#appointment_datepicker').data("DateTimePicker").date(date);
    // });
	
	/*============Booking Datepicker==============*/

	jQuery(function () {
		var today = '<?php echo $date; ?>';
		 jQuery('#appointment_datepicker').datetimepicker({
			inline: true,
			sideBySide: true,
			format: 'YYYY-MM-DD',
			minDate:today,
			maxDate: today,
			<?php if($profWeekDays != ''){ ?>
			
				daysOfWeekDisabled: [<?php echo $profWeekDays;?>],
			<?php } ?>
			<?php if($profdayOff[0] != ''){ ?>
				disabledDates: [<?php echo $profdayOff[0];?>]
			<?php } ?>
		 });
		//jQuery('#appointment_datepicker').data("DateTimePicker").date(today);
	 });
	 
/*==========Highlight professional day off and weekday dates==============*/

	$('#appointment_datepicker').on('dp.show', function(e){
		<?php if($profdayOff[0] != ''){ ?>
			highlight();
		<?php } ?>
		<?php if($profWeekDays != ''){ ?>
			highlightDays();
		<?php } ?>
	})
	$('#appointment_datepicker').on('dp.update', function(e){
		<?php if($profdayOff[0] != ''){ ?>
			highlight();
		<?php } ?>
		<?php if($profWeekDays != ''){ ?>
			highlightDays();
		<?php } ?>
	});
	$('#appointment_datepicker').on('dp.change', function(e){
		<?php if($profdayOff[0] != ''){ ?>
			highlight();
		<?php } ?>
		<?php if($profWeekDays != ''){ ?>
			highlightDays();
		<?php } ?>
	});
		
/*===========Function to hightlight dates===============*/

	<?php if($profdayOff[0] != ''){ ?>
		
	
		function highlight(){
			var dateToHilight = [<?php echo $profdayOffForHighlight[0];?>];
			var array = $("#appointment_datepicker").find(".day").toArray();
			for(var i=0;i<array.length;i++)
			{
				var date = array[i].getAttribute("data-day");
				var currentDate = new Date();
				month = ("0" + (currentDate.getMonth() + 1)).slice(-2),
				day = ("0" + currentDate.getDate()).slice(-2);
				year = currentDate.getFullYear();
				getCurrentDate = month+'/'+day+'/'+year;
				var selectedDate = date;
				if(selectedDate >= getCurrentDate)
				{
					
					if (dateToHilight.indexOf(date) > -1) 
					{
						console.log('tetys');
						array[i].style.color="#000000";
						array[i].style.borderRadius="100%";
						array[i].style.background= "#dd0a1e";
					} 
				}
			}
		}
	<?php } ?>
		
/*============Function to highlight the days=============*/
	<?php if($profWeekDays != ''){ ?>
	
		function highlightDays(){
			var profWeekDays = [<?php echo $profWeekDays; ?>];
			var array = $("#appointment_datepicker").find(".day").toArray();
			for(var i=0;i<array.length;i++)
			{
				var date = array[i].getAttribute("data-day");
				var currentDate = new Date();
				month = ("0" + (currentDate.getMonth() + 1)).slice(-2),
				day = ("0" + currentDate.getDate()).slice(-2);
				year = currentDate.getFullYear();
				getCurrentDate = month+'/'+day+'/'+year;
				var selectedDate = date;
				if(selectedDate >= getCurrentDate)
				{
					var getDateDay = new Date(date);
					if(jQuery.inArray(getDateDay.getDay(), profWeekDays) >= 0) {
						array[i].style.color="#000000";
						array[i].style.borderRadius="100%";
						array[i].style.background= "#dd0a1e";
					}
				}
			}
		}
	<?php } ?>
	

	
/*============change address location=============*/

	jQuery(document).on('change','#order_address',function(){
		jQuery(".valid_location").val('');
	});

	var changeAddress = new google.maps.places.Autocomplete(jQuery("#order_address")[0], {});
		google.maps.event.addListener(changeAddress, 'place_changed', function() {
		var place = changeAddress.getPlace();
        var Latitude = place.geometry.location.lat();
        var latprecision =  Math.pow(10, 6);
		var new_Latitude = (latprecision * Latitude)/latprecision;
		
		var Longitude = place.geometry.location.lng();
		var lngprecision =  Math.pow(10, 6);
		var new_Longitude = (lngprecision * Longitude)/lngprecision;

        jQuery('.customer_lat').val(new_Latitude);
        jQuery('.customer_long').val(new_Longitude);
        jQuery('.valid_location').val(jQuery('#order_address').val());
    });
	
/*=========update confirm location on click change location===========*/

    jQuery(document).on('click','.update-confirm-location',function(){
		var valid_location =  jQuery('.valid_location').val();
		var change_address = jQuery('#order_address').val();
		var address = jQuery('.confirm_location_address_span').html();
		var address_additional = jQuery('#address_additional').val();
		var customerlat = jQuery('.customer_lat').val();
		var customerlong = jQuery('.customer_long').val();
		var proLat = '<?php echo $getProviderDetail[0]->latitude;?>';
		var proLong = '<?php echo $getProviderDetail[0]->longitude;?>';
		var customerMiles = distance(customerlat,customerlong,proLat,proLong);
		var proMiles = '<?php echo $providerRange;?>';
		console.log(customerMiles);
		if(valid_location == '')
		{
			toastr.error('Enter valid location');
		}
		else if(customerMiles > proMiles)
		{
			toastr.error('Select location in range '+proMiles+' km');
		}
		else
		{
			jQuery('.confirm_location_address_span').html(change_address);
			jQuery('.change_customer_address').val(change_address);
			jQuery('.additional_address_span').html(address_additional);
			jQuery('.change_customer_additional_address').val(address_additional);
			jQuery('.customer_address_latitude').val(customerlat);
			jQuery('.customer_address_longitude').val(customerlong);
			jQuery('.confirm-popup').css('display','none');
		}
	});
	
/*=======calculate distance=======*/
	
	function distance(lat1, lon1, lat2, lon2, unit) 
	{
		if ((lat1 == lat2) && (lon1 == lon2)) {
			return 0;
		}
		else {
			var radlat1 = Math.PI * lat1/180;
			var radlat2 = Math.PI * lat2/180;
			var theta = lon1-lon2;
			var radtheta = Math.PI * theta/180;
			var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			if (dist > 1) {
				dist = 1;
			}
			dist = Math.acos(dist);
			dist = dist * 180/Math.PI;
			dist = dist * 60 * 1.1515;
			dist = dist * 1.609344;
			return Math.round(dist);
		}
	}
	
	
</script>


