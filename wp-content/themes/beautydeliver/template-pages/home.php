<?php 
/*
Template Name: home
*/
get_header();
get_sidebar();
$categories = getCategories();
?>
	<section class="categories-wrapper section">
		<div class="container">
			<div class="sec-title">
				<h2><?php echo the_field('category_title'); ?></h2>
			</div>
			<div class="owl-carousel owl-theme categories-grid-wrap">

				<?php if(count($categories) > 0){ ?>
					<div class="grid-item">
						<a href="<?php echo get_permalink(61);?>">
							<figure>
								<img src="<?php echo get_template_directory_uri()?>/assets/images/category1.png" alt="featured-thumb" />
							</figure>
							<h5><?php echo the_field('no_limit_title'); ?></h5>
						</a>
					</div>
				<?php 
					foreach($categories as $cat){

						if($cat->pictureFullPath != '')
						{
							$img = $cat->pictureFullPath;
						}
						else
						{
							$getImg = get_field('header_logo','options');
							$img = $getImg['url'];
						}
				?>
						<div class="grid-item">
							<a href="<?php echo get_permalink(61).'?param='.$cat->slug;?>">
								<figure>

									<img src="<?php echo $img;?>" alt="featured-thumb" />
								</figure>
								<h5><?php echo $cat->name;?></h5>
							</a>
						</div>

				<?php 
						} 
					} 
					else
					{
						echo '<div class="grid-item"><h5>No category found...</h5></div>';
					}
				?>
				
			</div>
		</div>
	</section>

	<section class="services-wrapper section" style="background-image: url('<?php echo get_template_directory_uri()?>/assets/images/services-bg.jpg');">
		<div class="container">
			<div class="services-post getPopularServices"></div>
			<?php //echo do_shortcode( '[main_services type="popular"]' ); ?>
					
<!--  Recent Services -->
			<div class="services-post getRecentServices"></div>
			<?php //echo do_shortcode( '[main_services type="recent"]' ); ?>

		</div>
	</section>
	
<!--  Popular Professionals -->	

	<section class="professionals-wrapper section">
		<div class="container">
			<div class="services-post getPopularProfessionals"></div>
			<?php //echo do_shortcode( '[main_professionals type="popular"]' ); ?>
			
<!--  Recent Professionals -->
			<div class="services-post getNewProfessionals"></div>
			<?php //echo do_shortcode( '[main_professionals type="newjoined"]' ); ?>

		</div>
	</section>
</div>

<?php get_footer();?>

<script>
/*=========Set location for home page above search service=========*/
	var homAutocomplete = new google.maps.places.Autocomplete(jQuery("#location-test")[0], {});

    google.maps.event.addListener(homAutocomplete, 'place_changed', function() {

        var place = homAutocomplete.getPlace();
        var Latitude = place.geometry.location.lat();
        var latprecision =  Math.pow(10, 6);
		var new_Latitude = (latprecision * Latitude)/latprecision;
		
		var Longitude = place.geometry.location.lng();
		var lngprecision =  Math.pow(10, 6);
		var new_Longitude = (lngprecision * Longitude)/lngprecision;
		jQuery.cookie("current_location", 'yes', { path: '/' });
        jQuery.removeCookie("my_current_location", '', { path: '/' });
        jQuery.cookie("latitude", new_Latitude, { path: '/' });
        jQuery.cookie("longitude", new_Longitude, { path: '/' });
        jQuery.cookie("address", jQuery('#location-test').val(),{ path: '/' });
        jQuery('.currentLat').val(new_Latitude);
        jQuery('.currentLong').val(new_Longitude);
        jQuery('.currentLocation').val(jQuery.cookie('address'));
		location.reload();
	});

/*==========DateTime picker for find my service========*/
	jQuery('.service-timepicker').datetimepicker({
		sideBySide: true,
	});
	
	var dts = new Date();
	var fromtime = dts.getHours()+parseInt(1) + ":00";
	var totime = dts.getHours()+parseInt(3) + ":00";
	jQuery('.timeType').val(fromtime+' - '+totime);
	jQuery('.time_from_times option[value="'+fromtime+'"]').attr('selected','selected');		
	jQuery('.time_to_times option[value="'+totime+'"]').attr('selected','selected');
	
</script>

