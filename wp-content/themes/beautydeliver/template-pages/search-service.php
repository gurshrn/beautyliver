<?php 
/*
Template Name: search-service
*/

$keyword = $_POST['keyword'];
if($_POST['searchLat'] == '' && $_POST['searchLong'] == '' && $_POST['searchLoc'] != '')
{
	$latitude = '';
	$longitude = '';
}
else if($_POST['searchLat'] != '' && $_POST['searchLong'] != '')
{
	$latitude = $_POST['searchLat'];
	$longitude = $_POST['searchLong'];
}
else
{
	$latitude = $_COOKIE['latitude'];
	$longitude = $_COOKIE['longitude'];
}

if($latitude != '' && $longitude != '')
{
	$query = ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
	$distanceHaving = "having distance <= wp_amelia_users.rangeWilling";
}

if($_POST['type'] == '' || $_POST['type'] == 'searchService')
{
	$services = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.* ".$query." FROM wp_amelia_services LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' AND wp_amelia_services.name LIKE  '%".$keyword."%' GROUP BY wp_amelia_services.name ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
	$getService = $wpdb->get_results($services, OBJECT);
}
if($_POST['type'] == '')
{
	$categories = "SELECT * FROM wp_amelia_categories where status = 'visible' AND name LIKE  '%".$keyword."%' ORDER BY id DESC";
	$getCategories = $wpdb->get_results($categories, OBJECT);
}
if($_POST['type'] == '' || $_POST['type'] == 'search-professionals')
{
	$professionals = "SELECT wp_amelia_users.rangeWilling,wp_amelia_users.*,wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_services.status = 'visible' AND wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND (CONCAT( wp_amelia_users.firstName,  ' ', wp_amelia_users.lastName ) LIKE '%".$keyword."%') AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_users.id  ".$distanceHaving." ORDER BY wp_amelia_users.id DESC";
	$getProfessionals = $wpdb->get_results($professionals, OBJECT);
}


$html = '';
function highlight_word( $content, $word) {
    $replace = '<span class="bold-service">' . $word . '</span>'; // create replacement
    $content = str_replace( $word, $replace, lcfirst($content) ); // replace content
	return $content; // return highlighted data
}

$html .= '<ul>'; 

if($_POST['type'] == '')
{
	if(count($getCategories) > 0 || count($getService) > 0 || count($getProfessionals) > 0)
	{
		if(count($getCategories) > 0)
		{
			foreach($getCategories as $cat)
			{
				$html .= '<li><a href="javascript:void(0)" class="get-service-name" data-target="category" data-attr="'.$cat->name.'">'.highlight_word($cat->name,$keyword).' (category)</a></li>';
			}
		}
		if(count($getService) > 0)
		{
			foreach($getService as $val)
			{
				$html .= '<li><a href="javascript:void(0)" class="get-service-name" data-target="service" data-attr="'.$val->name.'">'.highlight_word($val->name,$keyword).' (service)</a></li>';
			}
			
		}
		if(count($getProfessionals) > 0)
		{
			foreach($getProfessionals as $pro)
			{
				$html .= '<li><a href="javascript:void(0)" class="get-service-name" data-target="professional" data-value="'.$pro->id.'" data-attr="'.ucfirst($pro->firstName).' '.ucfirst($pro->lastName).'">'.highlight_word($pro->firstName.' '.lcfirst($pro->lastName),$keyword).' (professional)</a></li>';
			}
		}
	}
	else
	{
		$html .= '<span class="no_search_service">No record found...</span>';
	}
}
else
{
	if($_POST['type'] == 'search-professionals')
	{
		if(count($getProfessionals) > 0)
		{
			foreach($getProfessionals as $pro)
			{
				$html .= '<li><a href="javascript:void(0)" class="get-search-name search-category" data-attr="'.ucfirst($pro->firstName).' '.ucfirst($pro->lastName).'">'.highlight_word($pro->firstName.' '.lcfirst($pro->lastName),$keyword).'</a></li>';
			}
		}
		else
		{
			$html .= '<span class="no_search_service">No professional found...</span>';
		}
	}
	else
	{
		if(count($getService) > 0)
		{
			foreach($getService as $val)
			{
				$html .= '<li><a href="javascript:void(0)" class="get-search-name search-category" data-attr="'.$val->name.'">'.highlight_word($val->name,$keyword).'</a></li>';
			}
		}
		else
		{
			$html .= '<span class="no_search_service">No service found...</span>';
		}
	}
}

$html .= '</ul>';
echo $html;

?>