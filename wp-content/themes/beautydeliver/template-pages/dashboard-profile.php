<?php 
/*
Template Name: dashboard-profile
*/
checkSession();

get_header();
get_sidebar('dashboard');

$customerId = $_SESSION['user_id'];
$getCustomerDetail = getLoggedInUserDetail($customerId);

?>
		<div class="dashboard_inner_wrap">
                <section class="dashboard-info">
                    <div class="title-sec">
                        <h5>User Profile</h5>
                    </div>
                    <div class="content-sec">
                        <div class="profle-content-wrap">
                            <form id="dashboard_profile" method="POST" enctype="multipart/form-data">
								<input type="hidden" name="update_pin" class="update_pin">
								<input type="hidden" name="updateLocation" class="updateLocation" value="<?php echo 'update';?>">
								<input type="hidden" name="user_id" value="<?php echo $getCustomerDetail[0]->id; ?>">
								<input type="hidden" name="confirmed" class="confirm_mobile_number">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <div class="left_sec">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text"  class="form-control firstCap" name="firstName" placeholder="First Name" value="<?php echo $getCustomerDetail[0]->firstName;?>" maxlength="25">
                                            </div>
											<div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text"  class="form-control firstCap" name="lastName" placeholder="Last Name" value="<?php echo $getCustomerDetail[0]->lastName;?>" maxlength="25">
                                            </div>
											<div class="form-group">
                                                <label>Email</label>
                                                <input type="email"  class="form-control" name="email" placeholder="Email" value="<?php echo $getCustomerDetail[0]->email;?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Address</label>
												<input type="hidden" name="latitude" class="profile_latitude" value="<?php echo $getCustomerDetail[0]->latitude;?>">
												<input type="hidden" name="longitude" class="profile_longitude" value="<?php echo $getCustomerDetail[0]->longitude;?>">
                                                <input type="text"  class="form-control profile-search-address" name="address" placeholder="Search Address" value="<?php echo $getCustomerDetail[0]->address;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <div class="right_sec">
                                            <div class="profile-img-sec">
                                                <label>Profile Picture</label>
                                                <div class="img-sec">
													<?php if($getCustomerDetail[0]->pictureFullPath == ''){ ?>
														<figure style="background-image: url(<?php  echo get_template_directory_uri().'/assets/images/demo3.jpg';?>);"></figure>
													<?php } else { ?>
														<figure style="background-image: url(<?php  echo $getCustomerDetail[0]->pictureFullPath;?>);"></figure>
													<?php } ?>
                                                    <span class="uploadButton"><i class="fa fa-camera" aria-hidden="true"></i>
                                                        <input type="file" name="profile_image">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <div class="pin-code-sec">
													
													<input type="tel" name="phone_number" class="form-control amelia_phone_number" placeholder="PHONE NO." value="<?php echo $getCustomerDetail[0]->phone;?>">

													<button type="submit" class="update_phone_number">send pin</button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Phone Pin</label>
                                                <input type="tel" name="phone_pin" class="form-control" placeholder="Phone Pin">
                                            </div>
                                            <div class="message">
                                                <span>Please be aware, after you change your phone number, you will be no longer be able to login to our system with your old phone number </span>
                                            </div>
                                            <div class="btn-wrap">
                                                <button type="submit" class="button confirm_new_phone_number">Confirm new phone number</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- row -->
                                <div class="button-wrap">
                                    <ul>
                                        <li>
                                            <a href="<?php get_site_url().'/dashboard-profile';?>"><button type="button" class="button">Cancel</button></a>
                                        </li><li>
                                            <button type="submit" class="button dashboard-profile">Save</button>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <!-- dashboard-info -->
            </div>
		</div>
 <?php get_footer();?>
<script>
	
	jQuery(function() {
		jQuery('.profile-datepicker').datetimepicker({
            format: 'DD/MM/YYYY',
            maxDate:new Date(),
        });
	});
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
          $('#blah').css('display','block');
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    jQuery("#imgInp").change(function() {
        var files = this.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) 
            {
                $('#blah').css('display','none');
                toastr.error('File type allowed jpeg,jpg,png only');
                jQuery('input[type=file]').val('');
                jQuery('#thumbnil').hide();
                return false;    
            } 
            else
            {

                readURL(this);
            }
        }
    });
	
	jQuery(document).on('change','.profile-search-address',function(){
		jQuery('.updateLocation').val('');
	});
	
	var changeAddress = new google.maps.places.Autocomplete(jQuery(".profile-search-address")[0], {});
		google.maps.event.addListener(changeAddress, 'place_changed', function() {
		var place = changeAddress.getPlace();
		var Latitude = place.geometry.location.lat();
        var latprecision =  Math.pow(10, 6);
		var new_Latitude = (latprecision * Latitude)/latprecision;
		
		var Longitude = place.geometry.location.lng();
		var lngprecision =  Math.pow(10, 6);
		var new_Longitude = (lngprecision * Longitude)/lngprecision;
		
		jQuery('.profile_latitude').val(new_Latitude);
		jQuery('.profile_longitude').val(new_Longitude);
		jQuery('.updateLocation').val('update');
    });
	$('input').on("keypress", function(e) {
            /* ENTER PRESSED*/
            if (e.keyCode == 13) {
                /* FOCUS ELEMENT */
                var inputs = $(this).parents("form").eq(0).find(":input");
                var idx = inputs.index(this);

                if (idx == inputs.length - 1) {
                    inputs[0].select()
                } else {
                    inputs[idx + 1].focus(); //  handles submit buttons
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
</script>