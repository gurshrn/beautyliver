<?php 
/*
Template Name: professional-detail
*/
get_header();

$latitude = $_COOKIE['latitude'];
$longitude = $_COOKIE['longitude'];

$profId = $_GET['profId'];
$getProfessionalDetail = getProfessionDetailById($profId);
$profCat = getProfessionalCat($profId);
if($getProfessionalDetail[0]->pictureFullPath != '')
{
	$proImg = $getProfessionalDetail[0]->pictureFullPath;
}
else
{
	$imgUrl = get_field('header_logo','options');
	$proImg = $imgUrl['url'];
}
?>
<div class="main">
    <section class="professional-single-wrap section">
        <div class="container">
            <div class="professional-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-12">
                        <div class="image-sec">
                            <figure style="background-image: url('<?php echo $proImg;?>')"></figure>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6 col-12">
                        <div class="content-sec">
                            <h3><?php echo $getProfessionalDetail[0]->firstName.' '.$getProfessionalDetail[0]->lastName;?></h3>
                            <ul>
                                <?php if($getProfessionalDetail[0]->email != ''){?>
                                    <li>
                                        Mail To:
                                        <a class="mail" href="mailto: <?php echo $getProfessionalDetail[0]->email;?>"><?php echo $getProfessionalDetail[0]->email;?></a>
                                    </li>
                                <?php } ?>
                                <li>
                                    Phone No:
                                    <a class="tel" href="tel: <?php $getProfessionalDetail[0]->phone;?>"><?php echo $getProfessionalDetail[0]->phone;?></a>
                                </li>
                                <li>
                                    Address:
                                    <a><?php echo $getProfessionalDetail[0]->address;?></a>
                                </li>
                            </ul>
                            <p><?php echo $getProfessionalDetail[0]->note;?></p>
                        </div>
                    </div>
                </div>
                <!-- row -->
            </div>
            <!-- professional-info -->
            <div class="service-tab-wrap">
                <ul class="nav nav-tabs">
                    <?php 
                        $i = 0;
                        foreach($profCat as $cat){ 
                    ?>

                            <li class="nav-item">
                                <a class="nav-link <?php if($i == 0) { echo 'active';}?>" data-toggle="tab" href="#services<?php echo $i;?>"><?php echo $cat->name;?></a>
                            </li>

                    <?php $i++; } ?>
                </ul>

                <!-- Services -->

                <div class="tab-content">
                    <?php 
                        $j = 0;
                        foreach($profCat as $cat)
                        { 
                            $getCatServices= getCategoryServices($cat->id,$profId);
                    ?>
                            <div id="services<?php echo $j;?>" class="tab-pane <?php if($j == 0) { echo 'active';}?>">
                                <div class="row">
                                    <?php 
                                        if(count($getCatServices) > 0){
                                            foreach($getCatServices as $ser){
                                                if($ser->pictureFullPath != '')
                                                {
                                                    $img = $ser->pictureFullPath;
                                                }
                                                else
                                                {
                                                    $getImg = get_field('header_logo','options');
                                                    $img = $getImg['url'];
                                                }
                                                if($ser->totalRatingSum != '' && $ser->totalRatingSum != 0)
                                                {
                                                    $totalRating = $ser->totalRatingSum/$ser->totalRating;
                                                }
                                                else
                                                {
                                                    $totalRating = 0;
                                                }
                                                if($latitude != '' && $longitude != '')
												{
													$point1 = array("lat" => $latitude, "long" => $longitude); 
													$point2 = array("lat" => $ser->latitude, "long" => $ser->longitude);
													$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km'; 
												}
												else
												{
													$getkm = explode(",",$ser->address);
													$km = end($getkm); 
												}
                                    ?>
                                                <div class="col-lg-4 col-md-4 col-12">
                                                    <div class="service-grid">
                                                        <a href="<?php echo get_permalink(63).'?serviceId='.$ser->id;?>">
                                                            <figure style="background-image: url('<?php echo $img;?>');"></figure>
                                                        </a>
                                                        <div class="content">
                                                            <div class="top-sec">
                                                                <div class="left-sec">
                                                                    <h4>
                                                                        <a href="<?php echo get_permalink(63).'?serviceId='.$ser->id;?>"><?php echo $ser->name;?></a>
                                                                    </h4>
                                                                    <span>By <?php echo ucfirst($ser->firstName);?></span>
                                                                </div>
                                                                <div class="right-sec">
                                                                    <span><?php echo round($totalRating,1); ?><i class="fa fa-star" aria-hidden="true"></i></span>
                                                                    <span><?php echo count(getServiceTotalBookings($ser->id))?> times sold</span>
                                                                </div>
                                                            </div>
                                                            <div class="bottom-sec">
                                                                <div class="price-sec">
                                                                    <span><?php echo get_field('currency',options);?> <?php echo $ser->price;?></span>
                                                                </div>
                                                                <div class="location">
                                                                    <span><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $km;?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } } ?>
                                </div>
                            </div>
                    <?php $j++; } ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>

