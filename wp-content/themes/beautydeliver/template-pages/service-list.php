<?php 
/*
Template Name: service-list
*/
get_header();

$latitude = $_COOKIE['latitude'];
$longitude = $_COOKIE['longitude'];

$currentUserLocation = array();
if(isset($_COOKIE['user_id']))
{
	$currentUserLocation = getLoggedInUserDetail($_SESSION['user_id']);
}

$param = $_GET['param'];
$profSlug = $_GET['profId'];
$getServices = getServices($param,$profSlug);
$categories = getCategories();
?>
<input type="hidden" class="service-limit" value="4">
<input type="hidden" class="service-offset" value="0">

<input type="hidden" class="pro-limit" value="1">
<input type="hidden" class="pro-offset" value="0">

<input type="hidden" name="param" value="<?php echo $param;?>" class="urlParm">
<input type="hidden" name="getService" value="<?php echo $_GET['service'];?>" class="getService">
<input type="hidden" name="professionalservice" value="<?php echo $_GET['professionalservice'];?>" class="getProfessionalService">
<input type="hidden" name="getDate" value="<?php if($_GET['date'] != ''){ echo $_GET['date'].' '.$_GET['time'];}?>" class="getDate">
<input type="hidden" name="search_service" class="search_service" value="searchService">
<input type="hidden" name="profId" value="<?php echo $profSlug;?>" class="profId">
<input type="hidden" class="searchLat">
<input type="hidden" class="searchLong">
<input type="hidden" class="searchLoc">
<div class="main">
    <section class="service-list-wrap">
        <div class="container">
            <div class="sidebar-wrap">
                <div class="alert-wrap">
                    <span id="count-services"></span>
                </div>
                <div class="sidebar-section">
					
					<!-- Location text box -->
					
						<div class="widget search-wrap">
							<div class="search-sec">
							
								<input type="hidden" class="service-current-location">
							
								<input type="text" class="form_control search-change-location" placeholder="Change Location" value="<?php echo $_COOKIE['address'];?>">
								
								<a href="javascript:void(0)" class="location_cross">
									<i class="fa fa-times" aria-hidden="true"></i>
								</a>
							</div>
							<div id="service_search_location">
								<button type="button" class="getServiceCurrentLocation" data-toggle="tooltip" title="Current Location">
								<i class="fa fa-compass" aria-hidden="true"></i>
							</div>
						</div>
						
                    <div class="widget calender-wrap">
						
						<!-- Choose date time text box -->
						
							<label class="title">Choose date/time</label>
							<div class="calender-sec">
							<input type="hidden" class="searchSelectedDate">
							<input type="hidden" class="searchSelectedTime">
						
							<div id="search-datepicker" style="display:none;"></div>
                       
							<?php if($_GET['date'] == 'anydate'){ ?>
								<input type="text" class="form-control select-search-date" placeholder="Choose date/time" value="<?php echo $_GET['time']; ?>">
							<?php }else if($_GET['date'] != 'anydate' && $_GET['date'] != ''){ ?>
								<input type="text" class="form-control select-search-date" placeholder="Choose date/time" value="<?php echo date('d/m/Y',strtotime($_GET['date'])).' '.$_GET['time']; ?>">
							<?php } else if($_GET['date'] == ''){?>
								<input type="text" class="form-control select-search-date" placeholder="Choose date/time">
							<?php } ?>
						
							<a href="javascript:void(0)" class="choose_date_cross"><i class="fa fa-times" aria-hidden="true"></i></a>
							
							<!-- Choose datepicker div -->
						
							<div class="choose-wrap choose-datepicker" style="display:none">
									
								<?php 
									$datetime = new DateTime('tomorrow');
								?>
								<input type="hidden" class="dateType" value="anydate">
								<input type="hidden" class="timeType" value="00:00 - 23:00">
								
								<label>Choose Date</label>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<a href="javascript:void(0)" class="active any_date" data-target="anydate">Any Date</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<a href="javascript:void(0)" class="today_date" data-target="today" data-attr="<?php echo date('Y-m-d'); ?>">Today</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<a href="javascript:void(0)" class="tommorrow_date" data-target="tomo" data-attr="<?php echo $datetime->format('Y-m-d');?>">Tomorrow</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<a href="javascript:void(0)" class="choose_date">Choose date..</a>
											<div id="choose-datepicker" style="display:none;"></div>
										</div>
									</div>
								</div>
								<!-- row -->
								<label>Choose Time</label>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<a href="javascript:void(0)" class="any_time" data-target="00:00 - 23:00" class="any_time">Any Time</a>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<a href="javascript:void(0)" class="now_time choose-time">Now</a>
										</div>
									</div>
									<?php 
										$open_time = strtotime("00:00");
										$close_time = strtotime("23:00");
										$now = time();
										$output = "";
										
									?>
									<div class="col-lg-6 col-md-6 col-sm-6 chose_time">
										<label>From</label>
										<div class="form-group">
											<select class="time_from_times">
												<?php 
													for( $i=$open_time; $i<=$close_time; $i+=3600) 
													{
														if( date("H:i",$i) == date("H").':00') 
														{
															$sel = 'selected="selected"';
														}
														else
														{
															$sel = '';
														}
												?>
													<option value="<?php echo date("H:i",$i);?>" <?php echo $sel;?>><?php echo date("H:i",$i); ?></option>
														
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 chose_time">
										<label>To</label>
										<div class="form-group">
											<select class="time_to_times">
												<?php 
													for( $i=$open_time; $i<=$close_time; $i+=3600) 
													{
														if( date("H:i",$i) == date("H")+'2'.':00') 
														{
															$sel = 'selected="selected"';
														}
														else
														{
															$sel = '';
														}
												?>
													<option value="<?php echo date("H:i",$i);?>" <?php echo $sel;?>><?php echo date("H:i",$i); ?></option>
														
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<!-- row -->
								<div class="button-wrap">
									<button type="button" class="search-date-time-picker">Done</button>
								</div>
							</div>
						</div>
					</div>
					
                 <!-- Sort by dropdown -->
				  
                    <div class="widget sortby-wrap">
                        <label class="title">Sort by</label>
                        <form>
                            <div class="form-group">
                                <select class="search-category sortBy">
                                    <option value="">Sort by</option>
                                    <option value="popularity">Recommended (Highest Rating)</option>
                                    <option value="high">Highest Price</option>
                                    <option value="low">Lowest Price</option>
                                    <option value="recent">Most recent</option>
                                    <option value="distance">Distance</option>
                                </select>
                            </div>
                        </form>
                    </div>
				
				<!-- Categories list with count services -->				
				
                    <div class="widget categories-wrap">
                        <label class="title">Categories</label>
                        <ul class="list catList">
                            <?php 
                                if(count($categories) > 0){
                                    foreach($categories as $cat){
										if(count(getCategoryServices($cat->id)) > 0){
                            ?>
											<li class="checkbox-wrap">
												<label>
													<input type="checkbox" name="cat[]" class="search-category service_categories" value="<?php echo $cat->id;?>" <?php if($cat->slug == $param){ echo "checked='checked'";}?>>
													<span class="new-checkbox"></span>
													<?php echo $cat->name;?>    
												</label>
												<label><?php echo count(getCategoryServices($cat->id));?></label>
											</li>
                            <?php   }}}
                                else
                                {
                                    echo '<li class="checkbox-wrap">No category found....</li>';
                                }
                            ?>
                        </ul>
                    </div>

                    <!-- Price range slider -->
					
                    <div class="widget widget_slidecontainer" id="price-range">
						<label class="title">Price Range:</label>
						<span class="price-range"></span>
                        <div class="label-sec">
                            <span id="minPrice">€ 0</span>
                            <span id="maxPrice">€ 500</span>
                        </div>
                        <form>
							<input type="hidden" class="service_time_rangepicker" name="service_time_rangepicker">
							<input type="text" class="span2 ex2 search-category get-price-range" value="0,1000" data-slider-min="0" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,1000]"/>
                        </form>
                    </div>
					
					<!-- Service time slider -->
					
					<div class="widget widget_slidecontainer" id="service-range">

                        <label class="title">Service Time:</label>
						<span class="service-time-range"></span>
                        <div class="label-sec">
                            <span>0 min</span>
                            <span>60 min</span>
                        </div>
                        <form>
							<input type="text" class="span2 ex1 search-category get-service-time-range" value="" data-slider-min="0" data-slider-max="60" data-slider-step="1" data-slider-value="[0,60]"/>
                        </form>
                    </div>
				</div>
            </div>
			
			<!-- Top search bar for service and professional both -->
			
				<div class="services-wrapper">
					<div class="search-wrap">
						<form>
							<input type="search" class="search-category-service" placeholder="Search Service">
							<div id="search_service_list"></div>
						</form>
					</div>
					
			<!-- Service and professional tab -->
			
					<div class="service-tab-wrap">
						<ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link search-tab <?php if($param == 'popular-professionals' || $param == 'recent-professionals') { echo '';} else { echo 'active';}?>" data-attr="searchService" data-toggle="tab" href="#services">Services</a>
							</li>
							<li class="nav-item">
								<a class="nav-link search-tab <?php if($param == 'popular-professionals' || $param == 'recent-professionals') { echo 'active';}?>" data-attr="search-professionals" data-toggle="tab" href="#professionals">Professionals</a>
							</li>
						</ul>
						<div class="tab-content">
						
							<!-- Services list -->
							
							<div id="services" class="tab-pane <?php if($param == 'popular-professionals' || $param == 'recent-professionals') { echo '';} else { echo 'active';}?>">
								<div id="search-category-list" class="row"></div>
								<div class="load_more_services" style="display:none;">
									<button type="button" class="service_load_more_btn">Load More</button>
								</div>
							</div>

							<!-- Professioanls -->

							<div id="professionals" class="tab-pane fade <?php if($param == 'popular-professionals' || $param == 'recent-professionals') { echo 'active show';}?>">
								<div class="row" id="search-professional-list"></div>
								<div class="load_more_pro" style="display:none;">
									<button type="button" class="pro_load_more_btn">Load More</button>
								</div>
							</div>
						</div>
					</div>
				</div>
        </div>
    </section>
 </div>
 
<script type="text/javascript">
     var first_time=0;
        
 </script>
<?php get_footer();?>

<script>
	<?php if($_GET['date'] != '') {?>
		jQuery.cookie("searchDate", '<?php echo $_GET['date'].' '.$_GET['time']; ?>', { path: '/' });
	<?php } ?>
	
/*============On click current location button get services==============*/
	
	jQuery(document).on('click','.getServiceCurrentLocation',function(){
		jQuery('.service-current-location').val('current-location');
		<?php if(count($currentUserLocation) > 0){?>
			jQuery('.getServiceCurrentLocation').attr('title','My Location');
			jQuery('.getServiceCurrentLocation').addClass('getServiceMyLocation');
			jQuery('.getServiceCurrentLocation').removeClass('getServiceCurrentLocation');
		<?php } ?>
		serviceGetCurrentLocation();
	});
	jQuery(document).on('click','.getServiceMyLocation',function(){
		jQuery('.service-current-location').val('my-location');
		jQuery('.getServiceMyLocation').attr('title','Current Location');
		jQuery('.getServiceMyLocation').addClass('getServiceCurrentLocation');
		jQuery('.getServiceMyLocation').removeClass('getServiceMyLocation');
		serviceGetCurrentLocation();
	});
	function serviceGetCurrentLocation()
	{
		var countryName = '<?php echo $countryName;?>';
		var x = document.getElementById("demo");
		if (navigator.geolocation) 
		{
			 
			navigator.geolocation.getCurrentPosition(showPosition);
		} 
		else 
		{ 
			x.innerHTML = "Geolocation is not supported by this browser.";
		}
		function showPosition(position) 
		{
			var  lat = position.coords.latitude;
			var lng = position.coords.longitude;
			codeLatLng(lat, lng);
		}
		function codeLatLng(lat, lng) 
		{
			if(jQuery('.service-current-location').val() == 'current-location')
			{
				jQuery('.searchLat').val(lat);
				jQuery('.searchLong').val(lng);
			}
			else
			{
				jQuery('.searchLat').val('<?php echo $currentUserLocation[0]->latitude;?>');
				jQuery('.searchLong').val('<?php echo $currentUserLocation[0]->longitude;?>');
			}
			
			getCategories();
			searchCat();
				
			var geocoder= new google.maps.Geocoder();
			var latlng = new google.maps.LatLng(lat, lng);
			geocoder.geocode({'latLng': latlng}, function(results, status) 
			{
				if (status == google.maps.GeocoderStatus.OK) 
				{
					if (results[1]) 
					{
						if(jQuery('.service-current-location').val() == 'current-location')
						{
							jQuery('.search-change-location').val(results[0].formatted_address);
						}
						else
						{
							jQuery('.search-change-location').val('<?php echo $currentUserLocation[0]->address;?>');
						}
					} 
					else 
					{
						toastr.error('No Location Found');
					}
				} 
				else 
				{
					toastr.warning("Geocoder failed due to: " + status);
				}
			});
		}
	}
		

/*=====On click tab get type service or professional on service list page===========*/

	jQuery(document).on('click','.search-tab',function(){
		var type = jQuery(this).attr('data-attr');
		jQuery('.search_service').val(type);
		if(type == 'searchService')
		{
			jQuery('.search-category-service').attr('placeholder','Search Service');
		}
		else
		{
			jQuery('.search-category-service').attr('placeholder','Search Professional');
		}
	});
	
/*============Search Datepicker==============*/

    jQuery(function() {
        jQuery('#search-datepicker').datetimepicker({
            inline: true,
            sideBySide: true,
            format: 'YYYY-MM-DD',
            minDate: new Date(),
			//daysOfWeekDisabled: [0, 6]
        });
        if(jQuery('.getDate').val().length!=0){
            var date = new Date(jQuery('.getDate').val());
            month = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
            year = date.getFullYear();
            jQuery('#search-datepicker').data("DateTimePicker").date(year+'-'+month+'-'+day);
        }
    });

	var dts = new Date();
	var fromtime = dts.getHours()+parseInt(1) + ":00";
	var totime = dts.getHours()+parseInt(3) + ":00";
	jQuery('.timeType').val(fromtime+' - '+totime);
	jQuery('.time_from_times option[value="'+fromtime+'"]').attr('selected','selected');		
	jQuery('.time_to_times option[value="'+totime+'"]').attr('selected','selected');
</script>