<?php 
/*
Template Name: service-detail
*/
get_header();

$latitude = $_COOKIE['latitude'];
$longitude = $_COOKIE['longitude'];

$userId = $_SESSION['user_id'];
$getCurrentUserDetail = getLoggedInUserDetail($userId);

$serviceId = $_GET['serviceId'];
$serviceDetail = getServiceDetail($serviceId);

$profWeekDays = getProfessionalWeekDays($serviceDetail[0]->profId);
$profdayOff = getProfDayOff($serviceDetail[0]->profId);
$profdayOffForHighlight = getProfDayOffNew($serviceDetail[0]->profId);
$proSpecialDays = getSpecialDays($serviceDetail[0]->profId);
$searchDate = '';
$searchTime = '';
if(isset($_GET['date']))
{
	$explodeSearchDate = explode(" ",$_GET['date']);
	$searchWeekDay = date('w', strtotime($explodeSearchDate[0]));
	$getWeekDays = explode(",",$profWeekDays);
	$getProDayOff = explode(",",$profdayOff);
	if (!in_array($getWeekDays, $getWeekDays) && !in_array('"'.$explodeSearchDate[0].'"',$getProDayOff) && $explodeSearchDate[0] != 'anydate')
	{
		$searchDate = $explodeSearchDate[0];
	}
	if($explodeSearchDate[1].' '.$explodeSearchDate[2].' '.$explodeSearchDate[3] != '00:00 - 23:00')
	{
		$searchTime = $explodeSearchDate[1].' '.$explodeSearchDate[2].' '.$explodeSearchDate[3];
	}
}

$serviceAdditionalOptions = getServiceAdditionalOptions($serviceId);
$serviceGallery = getServiceGallery($serviceId);
$serviceFeedback = getServiceFeedback($serviceId);
$professionalCat = getProfessionalCat($serviceDetail[0]->profId);
$getSiteLogo = get_field('header_logo','options');
$siteLogo = $getSiteLogo['url'];

$date = new DateTime();
$todayWeekDay = $date->format( 'N' );
$data = array(
    'selectedDate'=>date('Y-m-d'),
    'weekDay'=>$todayWeekDay,
    'serviceId'=>$serviceId,
    'duration'=>$serviceDetail[0]->duration,
    'profId' => $serviceDetail[0]->profId
);
$getTodayTimeSlot = getBookingTimeSlot($data);

$point1 = array("lat" => $latitude, "long" => $longitude); 
$point2 = array("lat" => $serviceDetail[0]->latitude, "long" => $serviceDetail[0]->longitude); 
$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']);

$totalExtraPrice = '';
if(count(serviceAdditionalOptions) > 0)
{
	foreach($serviceAdditionalOptions as $val)
	{
		$totalExtraPrice += $val->price;
	}
}
$completeServicePrice = number_format($totalExtraPrice+$serviceDetail[0]->price,2);
?>
<div class="main">
    <section class="service-single-wrap section">
        <div class="container">
            <div class="service-content-wrap">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <section class="left_sec">
                            <div class="service-slider-wrap">
                                <?php 
                                    if(count($serviceGallery) > 0){ 
                                ?>
                                        <div id="demo1" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <?php 
                                                    if(count($serviceGallery) > 0){
                                                        $i = 0;
                                                        foreach($serviceGallery as $gal){
                                                ?>
                                                         <div class="carousel-item <?php if($i == 0){ echo 'active';}?>" style="background-image: url('<?php echo $gal->pictureFullPath;?>');"></div>
                                                
                                                <?php  $i++; } } ?>
                                            </div>

                                            <ul class="carousel-indicators">
                                                <?php 
                                                    if(count($serviceGallery) > 0){
                                                        $i = 0;
                                                        foreach($serviceGallery as $gal){
                                                ?>
                                                            <li data-target="#demo1" data-slide-to="<?php echo $i;?>" class="<?php if($i == 0){ echo 'active';}?>"></li>
                                                            
                                                <?php $i++; } } ?>
                                            </ul>

                                            <a class="left carousel-control" href="#demo1" data-slide="prev">
                                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                            </a>
                                            <a class="right carousel-control" href="#demo1" data-slide="next">
                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </a>
                                        </div>

                                <?php } else { ?>

                                    <div id="demo1" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active" style="background-image: url('<?php echo $siteLogo;?>');"></div>
                                        </div>

                                        <ul class="carousel-indicators">
                                            <li data-target="#demo1" data-slide-to="0" class="active"></li>
                                        </ul>
                                    </div>

                                <?php } ?>
                            </div>
                            <!-- service-slider-wrap -->

                            <div class="customer-feedback-wrap">
                                <div class="title">
                                    <h5>Customer Feedback</h5>
                                </div>
                                <?php
                                    if(count($serviceFeedback) > 0){
										echo '<ul>';
										foreach($serviceFeedback as $feed)
										{
											$getFeedBackImage = getFeedbackImage($feed->id);
											if($feed->pictureFullPath == '')
											{
												$url = $siteLogo;
											}
											else
											{
												$url = $feed->pictureFullPath;
											}
								?>
											<li>
												<figure style="background-image: url('<?php echo $url; ?>');"></figure>
												<h5><?php echo $feed->firstName.' '.$feed->lastName;?></h5>
												<div class="range">
													<span><?php echo $feed->rating;?></span>
													<?php 
														for($i=1;$i<=$feed->rating;$i++)
														{
															echo '<i class="fa fa-star" aria-hidden="true"></i>';
														}
													?>
												</div>
												<p><?php echo ucfirst($feed->comment);?></p>
												<?php 
													if(count($getFeedBackImage) > 0)
													{
														echo '<ul>';
														foreach($getFeedBackImage as $val)
														{
												?>
														<li style="background-image: url('<?php echo $val->image;?>');">
															<a href="<?php echo $val->image;?>" data-lightbox="feedback"></a>
														</li>
												<?php 
														} 
														echo '</ul>';
													} 
												?>
											</li>
                                    <?php } echo '<ul>';} else{ ?>

                                        <div id="no-feedback"><span>No feedback found...</span></div>

                                    <?php } ?>
                                </ul>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <section class="right_sec">
                            <div class="service-content">
                                <div class="sec-title">
                                    <h2><?php echo $serviceDetail[0]->name; ?></h2>
                                </div>
                                <div class="service-info-wrap">
                                    <div class="service-info">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <ul>
                                                    <li>
                                                        <?php
                                                            if($serviceDetail[0]->totalRatingSum != '' && $serviceDetail[0]->totalRatingSum != 0)
                                                            {
                                                                $totalRating = $serviceDetail[0]->totalRatingSum/$serviceDetail[0]->totalRating;
                                                            }
                                                            else
                                                            {
                                                                $totalRating = 0;
                                                            }

                                                            echo round($totalRating,1);

                                                        ?>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </li>
                                                    <li><?php echo count(getServiceTotalBookings($serviceId));?> times sold</li>
                                                    <li>
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
														<?php 
															if($latitude != '' && $longitude != '')
															{
																$point1 = array("lat" => $latitude, "long" => $longitude); 
																$point2 = array("lat" => $serviceDetail[0]->latitude, "long" => $serviceDetail[0]->longitude);
																$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km'; 
															}
															else
															{
																$km = $serviceDetail[0]->address;
																$getkm = explode(",",$serviceDetail[0]->address);
																$km = end($getkm); 
															} 
														?>
                                                        <?php echo $km;?>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
														<input type="hidden" id="total_duration" value="<?php echo $serviceDetail[0]->duration;?>">
                                                        <?php
                                                            $init = $serviceDetail[0]->duration;
                                                            $duration = convertIntoMin($init);
                                                            echo '<span class="totalSertime">'.$duration.'<span>';
                                                        ?> 
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <div class="professional-wrap">

                                                    <h6>The Professional</h6>

                                                    <?php 
                                                        if($serviceDetail[0]->user_picture == '')
                                                        {
                                                            $profileImg = $siteLogo;
                                                        }
                                                        else
                                                        {
                                                            $profileImg = $serviceDetail[0]->user_picture;
                                                        }
                                                    ?>

                                                    <figure style="background-image: url('<?php echo $profileImg;?>');"></figure>
                                                    
                                                    <h6>By <?php echo $serviceDetail[0]->firstName.' '.$serviceDetail[0]->lastName;?></h6>
                                                    <ul>
                                                        <?php 
                                                            if(count($professionalCat) > 0)
                                                            {
                                                                foreach($professionalCat as $proCat)
                                                                {

                                                        ?>
                                                                    <li <?php if($proCat->id == $serviceDetail[0]->catId){ echo 'class="active"';}?>>
                                                                        <a href="<?php echo get_site_url().'/service-list?param='.$proCat->slug.'&profId='.$serviceDetail[0]->profId;?>"><?php echo $proCat->name;?></a>
                                                                    </li>
                                                                    
                                                        <?php  } } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
										<h4>Description</h4>
                                        <p>
											<?php echo $serviceDetail[0]->description;?>
										</p>
                                    </div> 
									
                                    <?php if(count($serviceAdditionalOptions) > 0){ ?>
									
                                            <div class="additional-info">
                                                <h4>Additional options</h4>
                                                <input type="hidden" id="booking_total_price" value="<?php echo $serviceDetail[0]->price;?>">
                                                <ul>
												<?php foreach($serviceAdditionalOptions as $add){ ?>
														<li class="checkbox-wrap">
															<label>
																
																<?php $addSerDuration = $add->duration;?>

																<input type="checkbox" class="additional_price" name="checkbox" data-target="<?php echo $add->id;?>" data-attr="<?php echo $addSerDuration;?>" value="<?php echo $add->price;?>">
																
																<span class="new-checkbox"></span>
																<span><?php echo $add->name;?></span>   
																<span class="vl"><?php echo number_format($add->price,2).' '.' '.the_field('currency','option');?></span>
																<span class="vl">
																	<?php
																		$addSerDuration = $add->duration;
																		$getSerDuration = convertIntoMin($addSerDuration);
																		echo $getSerDuration;
																	?>
																</span>  
															</label>
														</li>
												<?php } ?>
                                                </ul>
                                            </div>
                                    <?php } ?>
                                </div>  
                            </div>
                            <div class="service-price">
                                <div class="title">
                                    <h5>Select Date and Time</h5>
                                </div>
                                <form method="POST" id="booked_service">
                                    <div class="row">
                                        <div class="col-lg-7 col-md-7 col-xs-12">

                                           <div class="availability-wrap">
                                                <div id="booking_datepicker"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-12">
                                            <div class="time-wrap">
                                                <div class="sec">
                                                    <h5>Time</h5>
                                                </div>
                                                <ul class="selected_date_time_slot">
                                                    <?php
                                                        if(count($getTodayTimeSlot > 0))
                                                        {
                                                            foreach($getTodayTimeSlot as$timeSlot) 
                                                            {           
                                                                echo '<li><a href="javascript:void(0)" class="book-service-time-slot" data-attr="'.$timeSlot.'">'.$timeSlot.'</a></li>'; 
                                                            }
                                                        }
                                                        else
                                                        {
                                                            echo '<li>No time slot available...</li>'; 
                                                        } 
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" class="booking_date" name="booking_date" value="<?php echo date('Y-m-d');?>">
                                    <input type="hidden" class="booking_time" name="booking_time">
                                    <input type="hidden" class="booking_price" name="booking_price" value="<?php echo $serviceDetail[0]->price;?>">
                                    <input type="hidden" class="extra_service" name="extra_service">
                                    <input type="hidden" name="serviceId" value="<?php echo $_GET['serviceId'];?>">
                                    <input type="hidden" name="providerId" value="<?php echo $serviceDetail[0]->profId; ?>">
                                    <div class="btn-wrap">
                                        
                                        <input type="submit" id="booknow" value="Book now ( <?php echo number_format($serviceDetail[0]->price,2).' '.get_field('currency',option);?>)" class="book_customer_service book-now">
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>

<script>

/*==========function to convert seconds into hour and min===================*/

	function convertMinsToHrsMins(d) 
	{
		d = Number(d);
		var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);

		var hDisplay = h > 0 ? h + (h == 1 ? " hr " : " hrs ") : "";
		var mDisplay = m > 0 ? m + (m == 1 ? " min " : " min ") : "";
		var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
		jQuery('.totalSertime').html(hDisplay + mDisplay);
	}
	
/*=========Add additional option price to booking price===============*/

    jQuery(document).on('change','.additional_price',function(e){
        var extraPrice = jQuery(this).val();
        var serviceId = jQuery(this).attr('data-target');
        var bookingTotalPrice = jQuery('#booking_total_price').val();
        var totalDuration = jQuery('#total_duration').val();
        var selectedDate = jQuery('.booking_date').val();
        var duration = jQuery(this).attr('data-attr');
        var extra_service = jQuery('.extra_service').val();

        if (jQuery(this).is(':checked')) 
        {
           var totalPrice = parseFloat(bookingTotalPrice)+parseFloat(extraPrice);
           var bookingTotalPrice = totalPrice.toFixed(2);
            if(totalDuration == '')
            {
                var bookingTotalDuration = parseInt(duration);
            }
            else
            {

                var bookingTotalDuration = parseInt(totalDuration)+parseInt(duration);
            }
            if(extra_service == '')
            {
                var extraServiceId = serviceId;
            }
            else
            {

                var extraServiceId = extra_service+','+serviceId;
            }
        }
        else
        {
            var totalPrice = parseFloat(bookingTotalPrice)-parseFloat(extraPrice);
            var bookingTotalPrice = totalPrice.toFixed(2);
            if(totalDuration != '')
            {
                var bookingTotalDuration = parseInt(totalDuration)-parseInt(duration);
            }

            /*===add and remove from array with jquery===*/

            var splitExtraService = extra_service.split(",");
            var idx = $.inArray(serviceId, splitExtraService);
            if (idx !== -1)
            {
                splitExtraService.splice(idx, 1);
            }
            var extraServiceId = splitExtraService.join(", ")
        }
        if(bookingTotalDuration == 0)
        {
            jQuery('#total_duration').val('');
        }
        else
        {
            jQuery('#total_duration').val(bookingTotalDuration);
        }
		var init = bookingTotalDuration;
		convertMinsToHrsMins(init);
		jQuery('#booking_total_price').val(totalPrice);
        jQuery('.service_total_price').html(bookingTotalPrice);
        jQuery('.book_customer_service').val('Book Now (' +bookingTotalPrice+ ' € )');
        jQuery('.extra_service').val(extraServiceId);
        getTimeSlots(selectedDate);
    });

/*============Booking Datepicker==============*/
var today = new Date();
	var defaults = {
		<?php if($searchDate != ''){ ?>
			date: new Date('<?php echo $searchDate; ?>'),
		<?php } ?>
		inline: true,
		sideBySide: true,
		format: 'YYYY-MM-DD',
		minDate:today,
		<?php if($profWeekDays != ''){ ?>
			daysOfWeekDisabled: [<?php echo $profWeekDays;?>],
		<?php } ?>
		<?php if($profdayOff[0] != ''){ ?>
			disabledDates: [<?php echo $profdayOff[0];?>]
		<?php } ?>
	};
	$(function () {
		$('#booking_datepicker').datetimepicker( defaults);
	});
	 
/*==========Highlight professional day off and weekday dates==============*/
	$('#booking_datepicker').on('dp.show', function(e){
		<?php if($profdayOff[0] != ''){ ?>
			highlight();
		<?php } ?>
		<?php if($profWeekDays != ''){ ?>
			highlightDays();
		<?php } ?>
	})
	$('#booking_datepicker').on('dp.update', function(e){
		<?php if($profdayOff[0] != ''){ ?>
			highlight();
		<?php } ?>
		<?php if($profWeekDays != ''){ ?>
			highlightDays();
		<?php } ?>
	});
		
/*===========Function to hightlight dates===============*/

	<?php if($profdayOff[0] != ''){ ?>
	
		function highlight(){
			var dateToHilight = [<?php echo $profdayOffForHighlight[0];?>];
			var array = $("#booking_datepicker").find(".day").toArray();
			for(var i=0;i<array.length;i++)
			{
				var date = array[i].getAttribute("data-day");
				var currentDate = new Date();
				month = ("0" + (currentDate.getMonth() + 1)).slice(-2),
				day = ("0" + currentDate.getDate()).slice(-2);
				year = currentDate.getFullYear();
				getCurrentDate = month+'/'+day+'/'+year;
				var selectedDate = date;
				if(selectedDate >= getCurrentDate)
				{
					if (dateToHilight.indexOf(date) > -1) 
					{
						array[i].style.color="#000000";
						array[i].style.borderRadius="100%";
						array[i].style.background= "#dd0a1e";
					} 
				}
			}
		}
	<?php } ?>
		
/*============Function to highlight the days=============*/
	<?php if($profWeekDays != ''){ ?>
	
		function highlightDays(){
			var profWeekDays = [<?php echo $profWeekDays; ?>];
			var profSpecialDays = [<?php echo $proSpecialDays; ?>];
			var array = $("#booking_datepicker").find(".day").toArray();
			for(var i=0;i<array.length;i++)
			{
				var date = array[i].getAttribute("data-day");
				var currentDate = new Date();
				month = ("0" + (currentDate.getMonth() + 1)).slice(-2),
				day = ("0" + currentDate.getDate()).slice(-2);
				year = currentDate.getFullYear();
				getCurrentDate = month+'/'+day+'/'+year;
				var selectedDate = date;
				
				<?php if($proSpecialDays != ''){?>
				
					if(jQuery.inArray(selectedDate, profSpecialDays) >= 0) {
						jQuery(array[i]).removeClass('disabled');
					}
					
				<?php } ?>
				
				else if(selectedDate >= getCurrentDate)
				{
					var getDateDay = new Date(date);
					if(jQuery.inArray(getDateDay.getDay(), profWeekDays) >= 0) {
						array[i].style.color="#000000";
						array[i].style.borderRadius="100%";
						array[i].style.background= "#dd0a1e";
					}
				}
				
			}
		}
	<?php } ?>

/*==============On change datepicker get time slots===============*/

    jQuery("#booking_datepicker").on("dp.change", function(e) {
		
		<?php if($profdayOff != ''){ ?>
			highlight();
		<?php } ?>
		<?php if($profWeekDays != ''){ ?>
			highlightDays();
		<?php } ?>
		<?php //if($searchDate != ''){ ?>
			//var date = new Date('<?php echo $searchDate; ?>');
		<?php //} else { ?>
			var date = new Date(e.date);
		<?php //} ?>
		month = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
        year = date.getFullYear();
        var selectedDate = year+'-'+month+'-'+day;
		jQuery('.booking_date').val(selectedDate); 
        getTimeSlots(selectedDate,'<?php echo $searchTime;?>');
    });

/*============When click on time slot add active class===============*/

    jQuery(document).on('click','.book-service-time-slot',function(){
        var time = jQuery(this).attr('data-attr');
        jQuery('li').removeClass('active');
        jQuery(this).parent().addClass('active');
        jQuery('.booking_time').val(time);
    });

/*=============Common function to show time slots==============*/

    function getTimeSlots(selectedDate,searchTime)
    {
        var serviceId = '<?php echo $_GET['serviceId'];?>';
        var duration = jQuery('#total_duration').val();
        
        jQuery.ajax({
            url: site_url+'/booking-time-slots',
            type: "POST",
            data:{action:"get_time_slots",searchTime:searchTime,date:selectedDate,serviceId:serviceId,duration:duration},
            dataType: "html",
            success: function(response) 
            {
               jQuery('.selected_date_time_slot').html(response);
            }
        });
    }
	
	/*======================user login===================*/
	jQuery(document).on('click','.book_customer_service',function(){
		var bookingDate = jQuery('.booking_date').val();
		var bookingTime = jQuery('.booking_time').val();
		<?php if($_SESSION['user_id'] == ''){ ?>
			toastr.error('Kindly login or registered your mobile number');
			return false;
		<?php }else if($getCurrentUserDetail[0]->type == 'provider'){?>
			
			toastr.error('Kindly login or registered as a customer');
			return false;
	
		<?php } else{ ?>
		
				if(bookingDate == '' && bookingTime != '')
				{
					toastr.error('Kindly select the booking date', 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					return false;
				}
				else if(bookingTime == '' && bookingDate != '')
				{
					toastr.error('Kindly select the booking time', 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					return false;
				}
				else if(bookingTime == '' && bookingDate == '')
				{
					toastr.error('Kindly select the booking date and booking time', 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				}
				else if(bookingTime != '' && bookingDate != '')
				{
					jQuery('#booked_service').valid();
				}
		<?php } ?>
		
	});
	jQuery("#booked_service").validate({

		submitHandler: function(form) 
		{
			var fd = new FormData(form);
			fd.append( "action", 'book_customer_service');
			jQuery('.book_customer_service').find('button[type=submit]').attr('disabled',true);
			jQuery('.book_customer_service').find('button[type=submit]').html('Processing...');
			jQuery.ajax({

				url: site_url+'/wp-admin/admin-ajax.php',
				type: form.method,
				data: fd,
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				success: function(response) 
				{
					jQuery('.book_customer_service').find('button[type=submit]').removeAttr('disabled');
					jQuery('.book_customer_service').find('button[type=submit]').html('Book Now');
					if(response.success == true)
					{
						window.location.href = response.url;
					}
					else
					{
						toastr.error('Error in booking', 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
				}
			});
		}

	});

   
</script>

