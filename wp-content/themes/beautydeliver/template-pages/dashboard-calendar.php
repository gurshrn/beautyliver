<?php 
/*
Template Name: dashboard-calendar
*/

checkSession(); 

get_header();
get_sidebar('dashboard');

?>
<div class="dashboard_inner_wrap">
        <div class="dashboard-info">
            <div class="title-sec">
                <h5>Appointment</h5>
                <input type="hidden" class="booking_date" name="booking_date" value="<?php echo date('Y-m-d');?>">
            </div>
            <div class="dashboard-app-sec">
                <div class="calender-wrap">
                    <div class="calender-sec">
                        <form data-toggle="datepicker">
                            <div class="docs-datepicker-container"></div>
                        </form>
                    </div>
                    
                    <div id="customer_booking_datepicker"></div>
                   
                </div>
                <div class="tab-wrap customer-appointments">
                    <ul class="nav nav-tabs">
                        <li>
                            <a class="active" data-toggle="tab" href="#confirmed">Confirmed Appointments</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#request">Appointment Requests</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#cancel">Cancelled</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="confirmed" class="tab-pane in active">
                            <div id="accordion">
                                <?php 
                                    if(count($customerApprovedAppointment) > 0){
                                        $i = 0;
                                        foreach($customerApprovedAppointment as $appoint){
                                ?>
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link" data-toggle="collapse" href="#collapseOne<?php echo $i;?>">
                                                        <h6 class="panel-title">
                                                            <?php echo $appoint->totalServices;?> 
                                                            <?php if($appoint->totalServices == 1){ echo 'Appointment';} else { echo 'Appointments';}?> 
                                                            <?php 
                                                                if($currentDate == date('Y-m-d',strtotime($appoint->bookingStart)))
                                                                {
                                                                    echo 'Today';
                                                                }
                                                            ?>
                                                        </h6>
                                                        <cite>
                                                            <?php echo date('d M Y',strtotime($appoint->bookingStart));?>
                                                        </cite>
                                                    </a>
                                                </div>
                                                <div id="collapseOne<?php echo $i;?>" class="collapse <?php if($i == 0){ echo 'show';}?>" data-parent="#accordion">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <?php 
                                                                $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($appoint->bookingStart)),'approved',$getCustomerId);
                                                                foreach($bookedService as $ser){
                                                            ?>
                                                                    <li>
                                                                        <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                                        <span>User Name</span>
                                                                        <span>Location</span>
                                                                    </li>
                                                            <?php } ?>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php $i++; } } else{ 

                                        echo 'No confirmed appointments found...';
                                    } ?>
                            </div>
                        </div>
                        <div id="request" class="tab-pane fade">
                            <div id="accordion1">
                                <?php 
                                    if(count($customerAllAppointment) > 0){
                                        $j = 0;
                                        foreach($customerAllAppointment as $cusAppoint){
                                ?>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseFourth<?php echo $j;?>">
                                            <h6 class="panel-title">
                                                <?php echo $cusAppoint->totalServices.' '.'Request';?> 
                                                
                                                <?php 
                                                    if($currentDate == date('Y-m-d',strtotime($cusAppoint->bookingStart)))
                                                    {
                                                        echo 'for Today';
                                                    }
                                                ?>
                                            </h6>
                                            <cite>
                                                <?php echo date('d M Y',strtotime($cusAppoint->bookingStart));?>
                                            </cite>
                                        </a>
                                    </div>
                                    <div id="collapseFourth<?php echo $j;?>" class="collapse <?php if($j == 0){ echo 'show';}?>" data-parent="#accordion1">
                                        <div class="panel-body">
                                            <ul>
                                                <?php 
                                                    $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($cusAppoint->bookingStart)),'',$getCustomerId);

                                                    foreach($bookedService as $ser){

                                                ?>
                                                        <li>
                                                            <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                            <span>User Name</span>
                                                            <span>Location</span>
                                                            <ul class="buttons">
                                                                <?php 
                                                                    if($currentDateTime < $cusAppoint->bookingStart && $ser->status == 'approved') 
                                                                    {
                                                                        echo '<li><a href="javascript:void(0);" class="cancel-appoint" data-attr="'.$ser->id.'">cancel</a></li>'; 
                                                                    }
                                                                    else if($ser->status == 'pending')
                                                                    {
                                                                        echo '<li>Pending</li>';
                                                                    }
                                                                    else if($ser->status == 'rejected')
                                                                    {
                                                                        echo '<li>Rejected</li>';
                                                                    }
                                                                    else if($ser->status == 'canceled')
                                                                    {
                                                                        echo '<li>Cancelled</li>';
                                                                    }
                                                                    else if($currentDateTime > $cusAppoint->bookingEnd && $ser->status == 'approved')
                                                                    {
                                                                        echo '<li>Processing</li>';
                                                                    }
                                                                    else
                                                                    {
                                                                        $countServiceReview = getCustomerServiceReview($ser->serviceId,$ser->id);

                                                                        if(count($countServiceReview) == 0)
                                                                        {
                                                                            echo '<li><a href="javascript:void(0)" class="customer_review" data-target="'.$ser->serviceId.'" data-attr="'.$ser->id.'">Review</a></li>';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<li>Completed</li>';
                                                                        }
                                                                        
                                                                    }
                                                                ?>
                                                            </ul>
                                                        </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            <?php $j++;} } else{ 

                                echo 'No  appointment requests found...';
                            } ?>

                            </div>
                        </div>
                        <div id="cancel" class="tab-pane fade">
                            <div id="accordion2">
                                <?php 
                                    if(count($customerCancelAppointment) > 0){
                                        $k = 0;
                                        foreach($customerCancelAppointment as $cusCancelAppoint){
                                ?>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseSix<?php echo $k;?>">
                                            <h6 class="panel-title">
                                                <?php echo $cusCancelAppoint->totalServices.' '.'Cancelled';?> 
                                                <?php if($cusCancelAppoint->totalServices == 1){ echo 'Appointment';} else { echo 'Appointments';}?> 
                                                <?php 
                                                    if($currentDate == date('Y-m-d',strtotime($cusCancelAppoint->bookingStart)))
                                                    {
                                                        echo 'for Today';
                                                    }
                                                ?>
                                            </h6>
                                        </a>
                                    </div>
                                    <div id="collapseSix<?php echo $k;?>" class="collapse <?php if($k == 0){ echo 'show';}?>" data-parent="#accordion2">
                                        <div class="panel-body">
                                            <ul>
                                                <?php 
                                                    $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($cusCancelAppoint->bookingStart)),'canceled',$getCustomerId);

                                                    foreach($bookedService as $ser){
                                                ?>
                                                        <li>
                                                            <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                            <span>User Name</span>
                                                            <span>Location</span>
                                                        </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php $k++; } } else{ 

                                    echo 'No  cancelled appointment found...';
                                } ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
</section>
<?php get_footer();?>
<script>

/*============Booking Datepicker==============*/

    $(function() {
        $('#customer_booking_datepicker').datetimepicker({
            inline: true,
            sideBySide: true,
            format: 'YYYY-MM-DD',
           //daysOfWeekDisabled: [0, 6]
        });
    });

/*==============On change datepicker get customer appointments===============*/

    jQuery("#customer_booking_datepicker").on("dp.change", function(e) {
        var date = new Date(e.date);
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
        year = date.getFullYear();
        var selectedDate = year+'-'+month+'-'+day;
        jQuery('.booking_date').val(selectedDate); 
        getCustomerAppointments(selectedDate);
    });

/*============get customer appoitment function==========*/

	function getCustomerAppointments(bookingDate)
	{
		var date = bookingDate;
        jQuery.ajax({
            url: site_url+'/customer-appointments',
            type: "POST",
            data:{action:"get_customer_appointments",date:date},
            dataType: "html",
            success: function(response) 
            {
               jQuery('.customer-appointments').html(response);
            }
        });
	}
</script>