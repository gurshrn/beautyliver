<?php 
/*
Template Name: dashboard-appoitment
*/

checkSession();

get_header();
get_sidebar('dashboard');

$userId = $_SESSION['user_id'];
$currentDate = date('Y-m-d');
$currentDateTime = date('Y-m-d H:i:s');
$customerId = getLoggedInUserDetail($userId);
$getCustomerId = $customerId[0]->id;
$customerApprovedAppointment = getCustomerAppointmentsAccToDate($getCustomerId);
$customerPendingAppointment = getCustomerPendingAppointmentsAccToDate($getCustomerId);
$customerAllAppointment = getCustomerAllAppointments($getCustomerId);
$customerCancelAppointment = getCustomerCancelAppointments($getCustomerId);
?>
	<div class="dashboard_inner_wrap">
        <div class="dashboard-info">
            <div class="title-sec">
                <h5>Appointment</h5>
            </div>
            <div class="dashboard-app-sec">
                <div class="calender-wrap">
                    <div class="calender-sec">
                        <form data-toggle="datepicker">
                            <div class="docs-datepicker-container"></div>
                        </form>
                    </div>
                    <div class="button-wrap">
                        <ul>
                            <li>
                                <a href="<?php echo get_site_url();?>">New Appointment</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-wrap">
                    <ul class="nav nav-tabs">
                        <li>
                            <a class="active" data-toggle="tab" href="#confirmed">Confirmed Bookings</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#pending">Pending Bookings</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#cancel">Canceled Bookings</a>
                        </li>
						<li>
                            <a data-toggle="tab" href="#all">Past Bookings</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="confirmed" class="tab-pane in active">
                            <div id="accordion">
                                <?php 
                                    if(count($customerApprovedAppointment) > 0){
                                        $i = 0;
                                        foreach($customerApprovedAppointment as $appoint){
                                ?>
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link <?php if($i > 0){ echo 'collapsed';}?>" data-toggle="collapse" href="#collapseOne<?php echo $i;?>">
                                                        <h6 class="panel-title">
                                                            <?php echo date('d M Y',strtotime($appoint->bookingStart));?>
                                                        </h6>
                                                        <cite>
															<?php echo $appoint->totalServices;?> 
                                                            <?php if($appoint->totalServices == 1){ echo 'Appointment';} else { echo 'Appointments';}?> 
                                                            <?php 
                                                                if($currentDate == date('Y-m-d',strtotime($appoint->bookingStart)))
                                                                {
                                                                    echo 'Today';
                                                                }
                                                            ?>
                                                        </cite>
                                                    </a>
                                                </div>
                                                <div id="collapseOne<?php echo $i;?>" class="collapse <?php if($i == 0){ echo 'show';}?>" data-parent="#accordion">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <?php 
                                                                $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($appoint->bookingStart)),'approved',$getCustomerId);
                                                                foreach($bookedService as $ser){
                                                            ?>
                                                                    <li>
                                                                        <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                                        <span><?php echo ucfirst($ser->firstName).' '.ucfirst($ser->lastName);?></span>
                                                                        <span><?php echo $ser->address;?></span>
                                                                    </li>
                                                            <?php } ?>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php $i++; } } else{ 

                                        echo 'No confirmed appointments found...';
                                    } ?>
                            </div>
                        </div>
						<div id="pending" class="tab-pane fade">
                            <div id="accordion">
                                <?php 
                                    if(count($customerPendingAppointment) > 0){
                                        $i = 0;
                                        foreach($customerPendingAppointment as $pendAppoint){
                                ?>
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link <?php if($i > 0){ echo 'collapsed';}?>" data-toggle="collapse" href="#collapseTwo<?php echo $i;?>">
                                                        <h6 class="panel-title">
															<?php echo date('d M Y',strtotime($pendAppoint->bookingStart));?>
														</h6>
														<cite>
															<?php echo $pendAppoint->totalServices.' '.'Request';?> 
															
															<?php 
																if($currentDate == date('Y-m-d',strtotime($pendAppoint->bookingStart)))
																{
																	echo 'for Today';
																}
															?>
														</cite>
                                                    </a>
                                                </div>
                                                <div id="collapseTwo<?php echo $i;?>" class="collapse <?php if($i == 0){ echo 'show';}?>" data-parent="#accordion">
                                                    <div class="panel-body">
                                                        <ul>
                                                <?php 
                                                    $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($pendAppoint->bookingStart)),'pending',$getCustomerId);

                                                    foreach($bookedService as $ser){

                                                ?>
                                                        <li>
                                                            <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                            <span><?php echo ucfirst($ser->firstName).' '.ucfirst($ser->lastName);?></span>
                                                            <span><?php echo $ser->address;?></span>
                                                        </li>
                                                <?php } ?>
                                            </ul>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php $i++; } } else{ 

                                        echo 'No pending appointments found...';
                                    } ?>
                            </div>
                        </div>
                        <div id="cancel" class="tab-pane fade">
							<div id="accordion2">
                                <?php 
                                    if(count($customerCancelAppointment) > 0){
                                        $k = 0;
                                        foreach($customerCancelAppointment as $cusCancelAppoint){
                                ?>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link <?php if($i > 0){ echo 'collapsed';}?>" data-toggle="collapse" href="#collapseSix<?php echo $k;?>">
                                            <h6 class="panel-title">
												<?php echo date('d M Y',strtotime($cusCancelAppoint->bookingStart));?>
											</h6>
											<cite>
												<?php echo $cusCancelAppoint->totalServices.' '.'Cancelled';?> 
											</cite>
                                        </a>
                                    </div>
                                    <div id="collapseSix<?php echo $k;?>" class="collapse <?php if($k == 0){ echo 'show';}?>" data-parent="#accordion2">
                                        <div class="panel-body">
                                            <ul>
                                                <?php 
                                                    $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($cusCancelAppoint->bookingStart)),'canceled',$getCustomerId);

                                                    foreach($bookedService as $ser){
                                                ?>
                                                        <li>
                                                            <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                            <span><?php echo ucfirst($ser->firstName).' '.ucfirst($ser->lastName);?></span>
                                                            <span><?php echo $ser->address;?></span>
                                                        </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php $k++; } } else{ 

                                    echo 'No  canceled appointment found...';
                                } ?>
                                
                            </div>
                        </div>
                        <div id="all" class="tab-pane fade">
                            <div id="accordion1">
                                <?php 
                                    if(count($customerAllAppointment) > 0){
                                        $j = 0;
                                        foreach($customerAllAppointment as $cusAppoint){
                                ?>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link <?php if($i > 0){ echo 'collapsed';}?>" data-toggle="collapse" href="#collapseFourth<?php echo $j;?>">
                                            <h6 class="panel-title">
                                                <?php echo date('d M Y',strtotime($cusAppoint->bookingStart));?>
                                            </h6>
                                            <cite>
												<?php echo $cusAppoint->totalServices.' '.'Appointment';?> 
                                                
                                                <?php 
                                                    if($currentDate == date('Y-m-d',strtotime($cusAppoint->bookingStart)))
                                                    {
                                                        echo 'for Today';
                                                    }
                                                ?>
                                            </cite>
                                        </a>
                                    </div>
                                    <div id="collapseFourth<?php echo $j;?>" class="collapse <?php if($j == 0){ echo 'show';}?>" data-parent="#accordion1">
                                        <div class="panel-body">
                                            <ul>
                                                <?php 
                                                    $bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($cusAppoint->bookingStart)),'',$getCustomerId);
													

                                                    foreach($bookedService as $ser){

                                                ?>
                                                        <li>
                                                            <span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
                                                            <span><?php echo ucfirst($ser->firstName).' '.ucfirst($ser->lastName);?></span>
                                                            <span><?php echo $ser->address;?></span>
                                                            <ul class="buttons">
                                                                <?php
																
                                                                    if($currentDateTime < $cusAppoint->bookingStart && $ser->status == 'approved') 
                                                                    {
																		echo '<li>Approved</li>'; 
                                                                    }
                                                                    else if($ser->status == 'pending')
                                                                    {
                                                                        echo '<li>Pending</li>';
                                                                    }
                                                                    else if($ser->status == 'rejected')
                                                                    {
                                                                        echo '<li>Rejected</li>';
                                                                    }
                                                                    else if($ser->status == 'canceled')
                                                                    {
                                                                        echo '<li>Cancelled</li>';
                                                                    }
                                                                    else
                                                                    {
                                                                        $countServiceReview = getCustomerServiceReview($ser->serviceId,$ser->id);

                                                                        if(count($countServiceReview) == 0)
                                                                        {
                                                                            echo '<li><a href="javascript:void(0)" class="customer_review" data-target="'.$ser->serviceId.'" data-attr="'.$ser->id.'">Review</a></li>';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<li>Completed</li>';
                                                                        }
                                                                        
                                                                    }
                                                                ?>
                                                            </ul>
                                                        </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            <?php $j++;} } else{ 

                                echo 'No  appointment requests found...';
                            } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
</section>
<?php get_footer();?>
