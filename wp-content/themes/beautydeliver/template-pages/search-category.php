<?php 
/*
Template Name: search-category
*/

global $wpdb;

if($_POST['searchLat'] == '' && $_POST['searchLong'] == '' && $_POST['searchLoc'] != '')
{
	$latitude = '';
	$longitude = '';
}
else if($_POST['searchLat'] != '' && $_POST['searchLong'] != '')
{
	$latitude = $_POST['searchLat'];
	$longitude = $_POST['searchLong'];
}
else
{
	$latitude = $_COOKIE['latitude'];
	$longitude = $_COOKIE['longitude'];
}

$serlimit = 'LIMIT'.' '.$_POST['serLimit'];
$getSerOffset = $_POST['serOffset']*$_POST['serLimit'];
$seroffset = 'OFFSET'.' '.$getSerOffset;

$prolimit = 'LIMIT'.' '.$_POST['proLimit'];
$getProOffset = $_POST['proOffset']*$_POST['proLimit'];
$prooffset = 'OFFSET'.' '.$getProOffset;



$param = $_POST['param'];
$profId = $_POST['profId'];
$serviceWhere = '';
$proWhere = '';
$query = '';
$distanceHaving = '';

if($latitude != '' && $longitude != '')
{
	$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
	$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
}

/*-sort by category-*/
if($_POST['catId'] == '')
{
	$param = '';
}


if($_POST['catId'] != '')
{
	$param = '';
	$_POST['getService'] = '';
	$catId = implode(",",$_POST['catId']);
	$serviceWhere .= " AND wp_amelia_categories.id IN (".$catId.")";
    $proWhere .= " AND wp_amelia_categories.id IN (".$catId.")";
}

/*-search by price-*/

if($_POST['price'] != '')
{
    $explodePrice = explode(",",$_POST['price']);
    $serviceWhere .= " AND (wp_amelia_services.price >= '".$explodePrice[0]."' AND wp_amelia_services.price <= '".$explodePrice[1]."')";
    $proWhere .=  " AND (wp_amelia_services.price >= '".$explodePrice[0]."' AND wp_amelia_services.price <= '".$explodePrice[1]."')";
}

/*-search by service time range-*/

if($_POST['servicerange'] != '')
{
	$explodeService = explode(",",$_POST['servicerange']);
	$fromService = $explodeService[0] * 60;
	$toService = $explodeService[1] * 60;
	$serviceWhere .= " AND (wp_amelia_services.duration >= '".$fromService."' AND wp_amelia_services.duration <= '".$toService."')";
    $proWhere .=  " AND (wp_amelia_services.duration >= '".$fromService."' AND wp_amelia_services.duration <= '".$toService."')";
}

/*-search service by keyword-*/

if($_POST['service'] != '')
{
	if($_POST['searchservice'] == 'searchService')
	{
		$serviceWhere .= " AND wp_amelia_services.name LIKE '%".$_POST['service']."%'";
		$proWhere .= " AND wp_amelia_services.name LIKE '%".$_POST['service']."%'";
	}
	else
	{
		$serviceWhere .= " AND (CONCAT( wp_amelia_users.firstName,  ' ', wp_amelia_users.lastName ) LIKE '%".$_POST['service']."%')";
		$proWhere .= " AND (CONCAT( wp_amelia_users.firstName,  ' ', wp_amelia_users.lastName ) LIKE '%".$_POST['service']."%')";
	}
   
}

/*==search by date==*/

if($_POST['date'] == '' && $_POST['searchDate'] == '')
{
	$currentDate = date('Y-m-d');
	$currentTime = date('H:i:s');
	$currentWeekDay = date('w',strtotime($currentDate));
}

if($_POST['date'] != '' && $_POST['selectedTime'])
{
	$_POST['searchDate'] = '';
	$bookingDate = $_POST['date'];
	$bookingTime = explode("-",$_POST['selectedTime']);
	$fromTime = date('H:i:s',strtotime($bookingTime[0]));
	$toTime = date('H:i:s',strtotime($bookingTime[1]));
	$getFromTime = $bookingDate.' '.$fromTime;
	$getToTime = $bookingDate.' '.$toTime;
	$weekday = date('w',strtotime($_POST['date']));
	
	$currentDate = $bookingDate;
	$currentTime = $fromTime;
	$currentWeekDay = date('w',strtotime($currentDate));
	
	if($bookingDate == 'anydate')
	{
		$serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((TIME(`bookingStart`) >= '".$fromTime."' AND TIME(`bookingEnd`) <= '".$toTime."')))";
		
		$proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((TIME(`bookingStart`) >= '".$fromTime."' AND TIME(`bookingEnd`) <= '".$toTime."')))";
	}
	else
	{
		$serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$getFromTime."' AND `bookingEnd` <= '".$getToTime."') OR (`bookingStart` <= '".$getFromTime."' AND `bookingEnd` >= '".$getToTime."'))) AND wp_amelia_users.id NOT IN (Select userId from wp_amelia_providers_to_daysoff where ('".$_POST['date']."' BETWEEN wp_amelia_providers_to_daysoff.startDate and wp_amelia_providers_to_daysoff.endDate)) AND wp_amelia_users.id IN (Select userId from wp_amelia_providers_to_weekdays where ('".$fromTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime OR '".$toTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime) AND wp_amelia_providers_to_weekdays.dayIndex = ".$weekday.")";
		
		$proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$getFromTime."' AND `bookingEnd` <= '".$getToTime."') OR (`bookingStart` <= '".$getFromTime."' AND `bookingEnd` >= '".$getToTime."'))) AND wp_amelia_users.id NOT IN (Select userId from wp_amelia_providers_to_daysoff where ('".$_POST['date']."' BETWEEN wp_amelia_providers_to_daysoff.startDate and wp_amelia_providers_to_daysoff.endDate)) AND wp_amelia_users.id IN (Select userId from wp_amelia_providers_to_weekdays where ('".$fromTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime OR '".$toTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime) AND wp_amelia_providers_to_weekdays.dayIndex = ".$weekday.")";
	}
}

/*====search by date time===*/

if($_POST['searchDate'] != '')
{
	$getExplodeDates = explode(" ",$_POST['searchDate']);
	//$getExplodedtime = explode("-",$getExplodeDates[1]);
	$bookingFromTime = $getExplodeDates[0].' '.$getExplodeDates[1].':00';
	$bookingToTime = $getExplodeDates[0].' '.$getExplodeDates[3].':00';
	$startTime = $getExplodeDates[1].':00';
	$endTime = $getExplodeDates[3].':00';
	if($getExplodeDates[0] == 'anydate')
	{
		 $serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((TIME(`bookingStart`) >= '".$startTime."' AND TIME(`bookingEnd`) <= '".$endTime."')))";
		 
		 $proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$startTime."' AND `bookingEnd` <= '".$endTime."')))";
	}
	else
	{
		$serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$bookingFromTime."' AND `bookingEnd` <= '".$bookingToTime."') OR (`bookingStart` <= '".$bookingFromTime."' AND `bookingEnd` >= '".$bookingToTime."'))) AND wp_amelia_users.id NOT IN (Select userId from wp_amelia_providers_to_daysoff where ('".$_POST['date']."' BETWEEN wp_amelia_providers_to_daysoff.startDate and wp_amelia_providers_to_daysoff.endDate)) AND wp_amelia_users.id IN (Select userId from wp_amelia_providers_to_weekdays where ('".$fromTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime OR '".$toTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime) AND wp_amelia_providers_to_weekdays.dayIndex = ".$weekday.")";
		
		$proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$bookingFromTime."' AND `bookingEnd` <= '".$bookingToTime."') OR (`bookingStart` <= '".$bookingFromTime."' AND `bookingEnd` >= '".$bookingToTime."'))) AND wp_amelia_users.id NOT IN (Select userId from wp_amelia_providers_to_daysoff where ('".$_POST['date']."' BETWEEN wp_amelia_providers_to_daysoff.startDate and wp_amelia_providers_to_daysoff.endDate)) AND wp_amelia_users.id IN (Select userId from wp_amelia_providers_to_weekdays where ('".$fromTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime OR '".$toTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime and wp_amelia_providers_to_weekdays.endTime) AND wp_amelia_providers_to_weekdays.dayIndex = ".$weekday.")";
	}
}

/*-search by category and professioanl-*/

if($param != '' && $profId != '')
{
    $catId = implode(",",$_POST['catId']);
    $serviceWhere .= " AND wp_amelia_categories.id IN (".$catId.") AND wp_amelia_users.id = '".$profId."'";
    $proWhere .= " AND wp_amelia_categories.id IN (".$catId.") AND wp_amelia_users.id = '".$profId."'";
}

/*-popular service-*/

if($param == 'popular-services' && $_POST['sortBy'] == '')
{
    $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
    $proWhere .= " GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
}

/*-recent service-*/

if($param == 'recent-services' && $_POST['sortBy'] == '')
{
    $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
    $proWhere .= " GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
}

/*-popular professionals-*/

if($param == 'popular-professionals' && $_POST['sortBy'] == '')
{
    $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
    $proWhere .= " GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
}

/*-recent professionals-*/

if($param == 'recent-professionals' && $_POST['sortBy'] == '')
{
    $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
    $proWhere .= " GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_users.id DESC";
}

/*-get services with service name-*/

if($_POST['getService'] != '')
{
    $serviceWhere .= " AND wp_amelia_services.name = '".$_POST['getService']."'";
    $proWhere .= " AND wp_amelia_services.name = '".$_POST['getService']."'";
}

/*-get services with professional name-*/

if($_POST['professionalservice'] != '')
{
	$serviceWhere .= " AND wp_amelia_users.id = '".$_POST['professionalservice']."'";
    $proWhere .= " AND wp_amelia_users.id = '".$_POST['professionalservice']."'";
}

/*-sort by-*/

if($_POST['sortBy'] != '')
{
    if($_POST['sortBy'] == 'low')
    {
        $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving."  ORDER BY wp_amelia_services.price ASC";
        $proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_services.price ASC";
    }
    else if($_POST['sortBy'] == 'high')
    {
        $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.price DESC";
        $proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_services.price DESC";
    }
	else if($_POST['sortBy'] == 'popularity')
    {
        $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
        $proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
    }
	else if($_POST['sortBy'] == 'recent')
    {
        $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.price DESC";
        $proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_users.id DESC";
    }
}

/*-if sort by null normal order-*/

if($param == '' && $_POST['sortBy'] == '')
{
    $serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
    $proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_users.id DESC";
}

$cat = "SELECT wp_amelia_users.id as userId,wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_categories.status = 'visible' AND wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' ".$serviceWhere." ".$serlimit.' '.$seroffset."";
$getServices['services'] = $wpdb->get_results($cat, OBJECT);

$totalcat = "SELECT wp_amelia_users.id as userId,wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId LEFT JOIN wp_amelia_providers_to_daysoff ON wp_amelia_users.id = wp_amelia_providers_to_daysoff.userId WHERE wp_amelia_categories.status = 'visible' AND wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' ".$serviceWhere."";
$totalServices = $wpdb->get_results($totalcat, OBJECT);


$professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address ".$query.",(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRating FROM wp_amelia_users INNER JOIN wp_amelia_providers_to_services ON wp_amelia_users.id = wp_amelia_providers_to_services.userId INNER JOIN wp_amelia_services ON wp_amelia_providers_to_services.serviceId = wp_amelia_services.id
INNER JOIN wp_amelia_categories ON wp_amelia_services.categoryId = wp_amelia_categories.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' ".$proWhere." ".$prolimit.' '.$prooffset."";
$getServices['professionals'] = $wpdb->get_results($professionals, OBJECT);

$totalprofessionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address ".$query.",(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRating FROM wp_amelia_users INNER JOIN wp_amelia_providers_to_services ON wp_amelia_users.id = wp_amelia_providers_to_services.userId INNER JOIN wp_amelia_services ON wp_amelia_providers_to_services.serviceId = wp_amelia_services.id
INNER JOIN wp_amelia_categories ON wp_amelia_services.categoryId = wp_amelia_categories.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' ".$proWhere."";
$getPro = $wpdb->get_results($totalprofessionals, OBJECT);

 
$serviceList = '';
if(count($getServices['services']) > 0)
{
	foreach($getServices['services'] as $ser)
    {
		if($ser->pictureFullPath != '')
        {
            $img = $ser->pictureFullPath;
        }
        else
        {
            $getImg = get_field('header_logo','options');
            $img = $getImg['url'];
        }
        if($ser->totalRatingSum != '' && $ser->totalRatingSum != 0)
        {
            $totalRating = $ser->totalRatingSum/$ser->totalRating;
        }
        else
        {
            $totalRating = 0;
        }
		if($latitude != '' && $longitude != '')
		{
			$point1 = array("lat" => $latitude, "long" => $longitude); 
			$point2 = array("lat" => $ser->latitude, "long" => $ser->longitude);
			$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km'; 
		}
		else
		{
			$point1 = array("lat" => $_COOKIE['latitude'], "long" => $_COOKIE['longitude']); 
			$point2 = array("lat" => $ser->latitude, "long" => $ser->longitude);
			$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km';
		} 
        $serviceList .= '<div class="col-lg-4 col-md-4 col-12">
            <div class="service-grid">
                <a href="'.get_permalink(63).'?serviceId='.$ser->id.'">
                    <figure style="background-image: url('.$img.');"></figure>
                </a>
                <div class="content">
                    <div class="top-sec">
                        <div class="left-sec">
                            <h4>';
								if($_POST['date'] != '' && $_POST['selectedTime'])
								{
									$serviceList .='<a href="'.get_permalink(63).'?serviceId='.$ser->id.'&date='.$_POST['date'].' '.$_POST['selectedTime'].'">'.$ser->name.'</a>';
								}
								else if($_POST['searchDate'] != '')
								{
									$serviceList .='<a href="'.get_permalink(63).'?serviceId='.$ser->id.'&date='.$_POST['searchDate'].'">'.$ser->name.'</a>';
								}
								else
								{
									$serviceList .='<a href="'.get_permalink(63).'?serviceId='.$ser->id.'">'.$ser->name.'</a>';
								}
                                
                            $serviceList .='</h4>
                            <span>By '.ucfirst($ser->firstName).'</span>
                        </div>
                        <div class="right-sec">
                            <span>'.round($totalRating,1).'<i class="fa fa-star" aria-hidden="true"></i></span>
                            <span>'.count(getServiceTotalBookings($ser->id)).' times sold</span>
                        </div>
                    </div>
                    <div class="bottom-sec">
                        <div class="price-sec">
                            <span>'.get_field('currency',option).' '.$ser->price.'</span>
                        </div>
                        <div class="location">
                            <span><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km.'</span>
                        </div>
                    </div>
                </div>
            </div>';
			$serviceList .='</div>';
	}
}
else
{
    $serviceList .= '<div class="col-lg-4 col-md-4 col-12">No service found...</div>';
}



$professionalList = '';
if(count($getServices['professionals']) > 0)
{
	foreach($getServices['professionals'] as $pro)
	{
		if($currentWeekDay == 0)
		{
			$weekDay = 7;
		}
		else
		{
			$weekDay = $currentWeekDay;
		}
		$profdayOff = getProfDayOff($pro->id,$currentWeekDay,$currentTime);
		$getProfdayOff = explode(",",$profdayOff[0]);
		$profWeekDays = getProfWeekDaysWithTime($pro->id,$weekDay,$currentTime);
		$proSpecialDays = getSpecialDaysWithTime($pro->id,$currentDate,$currentTime);
		$getAppointments = getProApointmentWithTime($pro->id,$currentDate,$currentTime);
		if(in_array('"'.$currentDate.'"',$getProfdayOff))
		{
			$status = 'Day Off';
			$class = 'red-status';
		}
		else if(!in_array(1,$proSpecialDays['default']) && $proSpecialDays['type'] == 'avail')
		{
			$status = 'Away';
			$class = 'red-status';
		}
		else if(count($profWeekDays) == 0 && $proSpecialDays['type'] == 'notavail')
		{
			$status = 'Away';
			$class = 'red-status';
		}
		else if(count($getAppointments) > 0)
		{
			$status = 'Busy';
			$class = 'yellow-status';
		}
		else
		{
			$status = 'Available';
			$class = 'green-status';
		}
	    if($pro->pictureFullPath != '')
	    {
	        $proImg = $pro->pictureFullPath;
	    }
	    else
	    {
	        $getProImg = get_field('header_logo','options');
	        $proImg = $getProImg['url'];
	    }
        if($pro->totalRatingSum != '' && $pro->totalRatingSum != 0)
        {
            $totalRating = $pro->totalRatingSum/$pro->totalRating;
        }
        else
        {
            $totalRating = 0;
        }
        $professionalUrl = get_site_url().'/professional-detail.?profId='.$pro->id;
        if($latitude != '' && $longitude != '')
		{
			$point1 = array("lat" => $latitude, "long" => $longitude); 
			$point2 = array("lat" => $pro->latitude, "long" => $pro->longitude);
			$km1 = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km away'; 
		}
		else
		{
			$getkm1 = explode(",",$pro->address);
			$km1 = end($getkm1); 
		} 

	    $professionalList .= '<div class="col-lg-4 col-md-4 col-12">
            <div class="service-grid">
                <a href="'.$professionalUrl.'">
                    <figure style="background-image: url('.$proImg.')"></figure>
                </a>
                <div class="content">
                    <div class="top-sec">
                        <div class="left-sec">
							<span class="'.$class.'">'.$status.'</span>
                            <h4>
                                <a href="'.$professionalUrl.'">'.ucfirst($pro->firstName).' '.ucfirst($pro->lastName).'</a>
                            </h4>
                        </div>
                        <div class="right-sec">
                            <span>'.round($totalRating,1).'<i class="fa fa-star" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <!-- top-sec -->
                    <div class="bottom-sec">
                        <div class="location">
                            <span><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km1.'</span>
                        </div>
                    </div>
                </div>
            </div>';
			$professionalList .= '</div>';
    } 
}
else
{
    $professionalList .='<div class="col-lg-4 col-md-4 col-12">No professional found...</div>';
}

$data['serviceList'] =  $serviceList;
$data['professionalList'] =  $professionalList;

$data['totalSer'] =  count($totalServices);
$data['totalPro'] =  count($getPro);

if(($_POST['date'] == 'NaN-aN-aN' || $_POST['date'] == '') && $_POST['searchDate'] == '')
{
	$data['countServices'] =  count($totalServices).' '.'services available';
}
else if($_POST['searchDate'] != '')
{
	$getExplodeDate = explode(" ",$_POST['searchDate']);
	if($getExplodeDate[0] == 'anydate')
	{
		$datetime = $getExplodeDate[1].' - '.$getExplodeDate[3];
	}
	else
	{
		$datetime = date('d M Y',strtotime($getExplodeDate[0])).' '.$getExplodeDate[1].' - '.$getExplodeDate[3];
	}
	$data['countServices'] =  count($totalServices).' '.'services available on '.$datetime;
}
else
{
	if($_POST['date'] != '' && $_POST['selectedTime'])
	{
		if($_POST['date'] == 'anydate')
		{
			$datetime = $_POST['selectedTime'];
		}
		else
		{
			$datetime = date('d M Y',strtotime($_POST['date'])).' '.$_POST['selectedTime'];
		}
	}
	$data['countServices'] =  count($totalServices).' '.'services available on '.$datetime;
}
$greaterPrice = 0;
$lowestPrice = 0;
if($_POST['service_range'] == '')
{
	if(count($totalServices) > 0)
	{
		$greaterPrice = 0;
		$lowestPrice = $totalServices[0]->price;
		foreach($totalServices as $getPrice)
		{
			if($getPrice->price > $greaterPrice)
			{
				$greaterPrice = $getPrice->price;
			}
			if($getPrice->price < $lowestPrice)
			{
				$lowestPrice = $getPrice->price;
			}
		}
	}
}
$data['greaterPrice'] = $greaterPrice;
$data['lowestPrice'] = $lowestPrice;
$data['service_range'] = $_POST['service_range'];
echo json_encode($data);exit;

?>