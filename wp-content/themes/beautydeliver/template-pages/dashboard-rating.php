<?php 
/*
Template Name: dashboard-rating
*/

checkSession();

get_header();
get_sidebar('dashboard');

$userId = $_SESSION['user_id'];
$currentDate = date('Y-m-d');
$currentDateTime = date('Y-m-d H:i:s');
$customerId = getLoggedInUserDetail($userId);
$getCustomerId = $customerId[0]->id;
$customerCompletedAppointment = getCustomerCompletedAppointmentsAccToDate($getCustomerId);
?>
	<div class="dashboard_inner_wrap">
        <div class="dashboard-info">
            <div class="title-sec">
                <h5>Open Ratings</h5>
            </div>
			<div class="dashboard-app-sec">
				
                <div class="tab-wrap">
                    <div class="tab-content">
                        <div id="confirmed" class="tab-pane in active">
                            <div id="accordion">
                                <?php 
                                    if(count($customerCompletedAppointment) > 0){
                                        $i = 0;
                                        foreach($customerCompletedAppointment as $appoint){
								?>
												<div class="card">
													<div class="card-header">
														<a class="card-link" data-toggle="collapse" href="#collapseOne<?php echo $i;?>">
															<h6 class="panel-title">
																<?php echo date('d M Y',strtotime($appoint->bookingStart));?>
															</h6>
															<cite>
																<?php echo $appoint->totalServices;?> 
																<?php if($appoint->totalServices == 1){ echo 'Appointment';} else { echo 'Appointments';}?> 
															</cite>
														</a>
													</div>
													<div id="collapseOne<?php echo $i;?>" class="collapse <?php if($i == 0){ echo 'show';}?>" data-parent="#accordion">
														<div class="panel-body">
															<ul>
																<?php 
																	$bookedService = getServicesAppointmentByDate(date('Y-m-d',strtotime($appoint->bookingStart)),'',$getCustomerId);
																	foreach($bookedService as $ser){
																		$countServiceReview = getCustomerServiceReview($ser->serviceId,$ser->id);
																		
																		if(($currentDateTime > $ser->bookingStart && $ser->status == 'approved') OR ($ser->status == 'completed' && count($countServiceReview) == 0))
																		{
																?>
																		<li>
																			<span><?php echo date('h:i A',strtotime($ser->bookingStart));?></span>
																			<span><?php echo ucfirst($ser->firstName).' '.ucfirst($ser->lastName);?></span>
																			<span><?php echo ucfirst($ser->name);?></span>
																			<ul class="buttons">
																				<?php 
																					
																					echo '<li><a href="javascript:void(0)" class="customer_review" data-target="'.$ser->serviceId.'" data-attr="'.$ser->id.'">Review</a></li>';
																					
																				?>
																				
																			</ul>
																		</li>
																	<?php } } ?>
																
															</ul>
														</div>
													</div>
												</div>
										<?php $i++; } }  else{ 

                                        echo 'No open ratings found...';
                                    } ?>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>    
</section>
<?php get_footer();?>