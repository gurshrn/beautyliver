<?php
	$ip = "122.173.50.24";  
	$details = json_decode(file_get_contents("http://api.ipstack.com/".$ip."?access_key=0b6a03704b17bff833a5e20de557766d"));
	$countryCode = $details->country_code;
	date_default_timezone_set('Asia/Kolkata');
	
/*=====changes in css for admin=======*/

	add_action("admin_head","admin_css");
	function admin_css()
	{
		
		if(!current_user_can('administrator')){?>
		<style>
		
			.am-services-categories .am-categories-column button.el-button.am-dialog-create.el-button--primary.el-button--large,
			a.am-help-button,
			span#footer-thankyou,
			#am-employees .el-button.am-button-icon,.am-drop-create-item,
			.am-employees-filter.am-section
			,#wpfooter
			,a.am-help-button
			,div#tab-services
			,.am-locations-filter
			,.am-services-categories .am-categories-column .am-category-item .el-icon-picture
			,.am-services-categories .am-categories-column .am-category-item h3 .am-drag-handle
			,.align-right.category-actions.el-col.el-col-12
			,#menu-media
			,li.wp-not-current-submenu.wp-menu-separator
			,.update-nag
			,#menu-dashboard
			,#am-employees .am-page-header button.el-button.am-dialog-create.el-button--primary 
			,#amelia-app-backend .am-categories-column button.el-button.am-dialog-create.el-button--primary.el-button--large
			,#am-button-new .el-button.el-button--primary 
			,.am-dialog-footer .align-left,
			.hidden,#am-locations .am-pagination,
			#am-locations .am-page-header .am-dialog-create,
			#am-customers .am-customers-list .am-customer-data .el-button.el-button--default,
			#am-customers .am-customers .el-checkbox,
			#am-locations .am-page-header .am-page-title span
			{
				display:none!important;
			}
			#am-employees .am-dialog-footer .align-right, .am-align-right{
				float: right;
			}

			#amelia-app-backend .am-categories-column h2 {
				margin-bottom: 10px;
			}
			#wpadminbar #wp-admin-bar-my-account a.ab-item{font-size:0;}
			#wpadminbar #wp-admin-bar-my-account .ab-sub-wrapper a.ab-item{font-size: 13px;}
			
		</style>
		
<?php } ?>
	<style type="text/css">
		.am-confirm-phone{
			margin-top: 27px !important;
		}
	</style>

<script>
			jQuery(document).on('click','.el-popper',function(){
				return false;
			});
		</script>


<?php }
/*=====Wordpress by default redirect to another menu after login======*/ 
	
	if(!current_user_can('administrator'))
	{
		add_action ('init' , 'prevent_profile_access');
		 
		function prevent_profile_access() {
			if (current_user_can('manage_options')) return '';
			if (strpos ($_SERVER ['REQUEST_URI'] , 'wp-admin/profile.php' ))
			{
				wp_redirect (admin_url().'admin.php?page=wpamelia-appointments#/appointments');
				die();
			}
		}
	}
	
/*=======Remove admin bar from website==========*/
	
	add_filter('show_admin_bar', '__return_false');

/**************Remove top admin bar options***********************/
	function remove_admin_bar_links() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('updates');          // Remove the updates link
		$wp_admin_bar->remove_menu('comments');         // Remove the comments link
		$wp_admin_bar->remove_menu('new-content');      // Remove the content link
		$wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
	}
	add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

/*======change wordpress logout url========*/
	
	add_action( 'wp_logout', 'auto_redirect_external_after_logout');
	function auto_redirect_external_after_logout(){
	  wp_redirect( get_site_url() );
	  exit();
	}

/*=========Remove collapse menu from admin side bar=========*/

	function wpse_remove_collapse() 
	{
	   echo '<style type="text/css">#collapse-menu { display: none; visibility: hidden; }</style>';
	}
	add_action('admin_head', 'wpse_remove_collapse');
	
/*========Remove admin top bar logo=========*/
	
	add_action( 'wp_before_admin_bar_render', function() 
	{
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('wp-logo');
	}, 7 );
	add_action('admin_menu', 'remove_admin_menu_links');

/*==========remove profile menu from admin sidebar=======*/
	function remove_admin_menu_links()
	{
		remove_menu_page('profile.php');
	}
	
/*==========Remove edit profile option from admin=======*/

	remove_theme_support( 'genesis-admin-menu' );
	function ya_do_it_admin_bar_remove() 
	{
        global $wp_admin_bar;
		$wp_admin_bar->remove_menu('edit-profile');
	}

	add_action('wp_before_admin_bar_render', 'ya_do_it_admin_bar_remove', 0);
	
	
	
/*===========Start session for wordpress============*/
	
	add_action('init', 'myStartSession', 1);
	add_action('wp_logout', 'myEndSession');
	add_action('wp_login', 'myEndSession');

	function myStartSession() {
		if(!session_id()) {
			session_start();
		}
	}
	function myEndSession() {
		session_destroy ();
	}
	
/*=========Check session=========*/
	
	function checkSession()
	{
		if($_SESSION['user_id'] == '')
		{
			wp_redirect( get_site_url() ); 
		}
	}

/*============get logged in user detail===========*/
	
	function getLoggedInUserDetail($userId)
	{
		global $wpdb;
		$getUser = "SELECT wp_amelia_users.*,customer_address_detail.address,customer_address_detail.latitude,customer_address_detail.longitude FROM wp_amelia_users LEFT JOIN customer_address_detail ON wp_amelia_users.id = customer_address_detail.user_id WHERE wp_amelia_users.externalId='".$userId."' AND wp_amelia_users.status='visible'";
		$getUserDetail = $wpdb->get_results($getUser, OBJECT);
		return $getUserDetail;
	}

/*=====get provider detail=========*/

	function getProvideruserDetail($userId)
	{
		global $wpdb;
		$getUser = "SELECT wp_amelia_users.*,wp_amelia_locations.latitude,wp_amelia_locations.longitude FROM wp_amelia_users LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_users.id='".$userId."' AND wp_amelia_users.status='visible'";
		$getUserDetail = $wpdb->get_results($getUser, OBJECT);
		return $getUserDetail;
	}
	

/*==============Enable option page for ACF plugin===========*/

	if( function_exists('acf_add_options_page') ) 
	{
		acf_add_options_page();
	}

/*=============Add header and footer menu==========*/

	function register_my_menu() {

	  register_nav_menu('header-menu',__( 'Header Menu' ));

	}

	add_action( 'init', 'register_my_menu' );

	register_nav_menus( array(
		'footer'  => __( 'Footer Menu', 'beautydeliver' ),
	) );

/*=======convert into seconds into hrs,min and sec====*/

	function convertIntoMin($duration)
	{
		$hours = floor($duration / 3600);
	    $minutes = floor(($duration / 60) % 60);
	    $seconds = $duration % 60;
	    $html = '';
	    if($hours != 0)
	    {
	        $html .= $hours.' hrs ';
	    }
	    if($minutes != 0)
	    {
	        $html .= $minutes.' min ';
	    }
	    if($seconds != 0)
	    {
	        $html .= $seconds.' sec ';
	    }
	    return $html;
	}

/*=========calculate distance========*/

	function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 5) 
	{
		$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
 
		switch($unit) {
			case 'km':
				$distance = $degrees * 111.13384; 
				break;
			case 'mi':
				$distance = $degrees * 69.05482; 
				break;
			case 'nmi':
				$distance =  $degrees * 59.97662; 
		}
		
		return round(round($distance), $decimals);
	}

/*==========explode Url===========*/

	function explodeUrl()
	{
		$r = $_SERVER['REQUEST_URI'];
		$r = explode('/', $r);
		$r = array_filter($r);
		$r = array_merge($r, array()); 
		$r = preg_replace('/\?.*/', '', $r);
		return $r;
	}

/*==========get professional detail with professional id=========*/
	
	function getProfessionDetailById($profId)
	{
		global $wpdb;
		$prof = "SELECT wp_amelia_users.*,wp_amelia_locations.address FROM wp_amelia_users LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_users.id='".$profId."'";
		$getProf = $wpdb->get_results($prof, OBJECT);
		return $getProf;
	}	

/*==============get categories============*/

	function getCategories()
	{
		global $wpdb;
		$categories = "SELECT * FROM wp_amelia_categories WHERE status='visible' ORDER BY id DESC";
		$getCategories = $wpdb->get_results($categories, OBJECT);
		return $getCategories;
	}
	
	function getCategoriesNew()
	{
		global $wpdb;
		$latitude = $_COOKIE['latitude'];
		$longitude = $_COOKIE['longitude'];
		$distanceHaving = '';
		$query = '';
		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
		}
		$categories = "SELECT wp_amelia_users.rangeWilling,wp_amelia_categories.*,wp_amelia_locations.address ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_services.status = 'visible' AND wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_categories.id ".$distanceHaving." ORDER BY wp_amelia_categories.id DESC ";
		$getCategories = $wpdb->get_results($categories, OBJECT);
		return $getCategories;
	}

/*=============get service of category with category id============*/

	function getCategoryServices($catId,$profId='')
	{
		global $wpdb;
		$latitude = $_COOKIE['latitude'];
		$longitude = $_COOKIE['longitude'];
		$where = '';
		$query = '';
		$distanceHaving = '';
		if($profId != '')
		{
			$where .= " AND wp_amelia_users.id = '".$profId."'";
		}
		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
		}
		$services = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_categories.status = 'visible' AND wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' AND wp_amelia_categories.id='".$catId."' ".$where." GROUP BY wp_amelia_services.id ".$distanceHaving."";
		$getServices = $wpdb->get_results($services, OBJECT);
		return $getServices;
	}
	
/*========get services by search location in home page==========*/
	add_action('wp_ajax_getLocationServices', 'getLocationServices');
	add_action('wp_ajax_nopriv_getLocationServices', 'getLocationServices');
	function getLocationServices()
	{
		global $wpdb;
		$lang = $_COOKIE['lang'];
		$latitude = $_POST['lat'];
		$longitude = $_POST['lng'];
		$atts = $_POST['type'];
		$query = '';
		$distanceHaving = '';
		$html = '';
		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
		
			if($atts == 'popular')
			{
				$services = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName,wp_amelia_users.lastName ".$query." FROM wp_amelia_services LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_providers_to_services.userId = wp_amelia_providers_to_weekdays.userID WHERE wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_services.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC LIMIT 10";
			}
			if($atts == 'recent')
			{
				$services = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName,wp_amelia_users.lastName ".$query." FROM wp_amelia_services LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_providers_to_services.userId = wp_amelia_providers_to_weekdays.userID WHERE wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC LIMIT 10";
			}
			$getService = $wpdb->get_results($services, OBJECT);
			if(count($getService) > 0)
			{
				if($atts == 'popular')
				{
					$serviceType = 'Our Popular Services';
					if($lang != '')
					{
						$moreUrl = get_permalink(61).'?lang='.$lang.'&param=popular-services';
					}
					else
					{
						$moreUrl = get_permalink(61).'?param=popular-services';
					}
				}
				if($atts == 'recent')
				{
					$serviceType = 'Our Recent Services';
					if($lang != '')
					{
						$moreUrl = get_permalink(61).'?lang='.$lang.'&param=recent-services';
					}
					else
					{
						$moreUrl = get_permalink(61).'?param=recent-services';
					}
				}
				if($lang != '')
				{
					$moreUrl = get_permalink(61).'?lang='.$lang.'&param=popular-services';
				}
				else
				{
					$moreUrl = get_permalink(61).'?param=popular-services';
				}
				$html .= '<div class="sec-wrapper">
							<div class="sec-title">
								<h2>'.$serviceType.'</h2>
							</div>
							<div class="more-btn">
								<a href="'.$moreUrl.'">more</a>
							</div>
						</div>
						<div class="owl-carousel owl-theme service-slider">';
				foreach($getService as $ser)
				{
					if($ser->totalRatingSum != '' && $ser->totalRatingSum != 0)
					{
						$totalRating = $ser->totalRatingSum/$ser->totalRating;
					}
					else
					{
						$totalRating = 0;
					}
					if($latitude != '' && $longitude != '')
					{
						$point1 = array("lat" => $latitude, "long" => $longitude); 
						$point2 = array("lat" => $ser->latitude, "long" => $ser->longitude);
						$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km'; 
					}
					$html .= '<div class="slider-grid">
								<a href="'.get_permalink(63).'?serviceId='.$ser->id.'">';
								if($ser->pictureFullPath != '')
								{
									$html .='<figure style="background-image: url('.$ser->pictureFullPath.');"></figure>';
								}
								else
								{
									$getImg = get_field('header_logo','options');
									$html .='<figure class="by-default-image"><img src="'.$getImg['url'].'"></figure>';
								}
								
								$html .= '</a>
								<div class="content">
									<div class="top-sec">
										<div class="left-sec">
											<h4>
												<a href="'.get_permalink(63).'?serviceId='.$ser->id.'">'.ucfirst($ser->name).'</a>
											</h4>
											<span>By '.ucfirst($ser->firstName).' '.ucfirst($ser->lastName).'</span>
										</div>
										<div class="right-sec">
											<span>'.round($totalRating,1).'<i class="fa fa-star" aria-hidden="true"></i></span>
											<span>'.count(getServiceTotalBookings($ser->id)).' times sold</span>
										</div>
									</div>
									<div class="bottom-sec">
										<div class="price-sec">
											<span>'.get_field('currency',options).' '.$ser->price.'</span>
										</div>
										<div class="location">
											<span><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km.'</span>
										</div>
									</div>
								</div>
							</div>';
				}
				$html .= '</div>';
			}
			else
			{
				$html .= '<div class="slider-grid">No service found on your location...</div>';
			}
		}
		echo $html;die;		
	}
	
/*===========get professional in home page by search location===========*/

	add_action('wp_ajax_getLocationProfessionals', 'getLocationProfessionals');
	add_action('wp_ajax_nopriv_getLocationProfessionals', 'getLocationProfessionals');
	function getLocationProfessionals()
	{
		global $wpdb;
		
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i:s');
		$currentWeekDay = date('w',strtotime($currentDate));
		
		$latitude = $_POST['lat'];
		$longitude = $_POST['lng'];
		$atts = $_POST['type'];
		$query = '';
		$distanceHaving = '';
		$html = '';
		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
		
			if($atts == 'popular')
			{
				// $professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_services.status = 'visible' AND wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC LIMIT 10";
				
				$professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address ".$query.",(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRating FROM wp_amelia_users INNER JOIN wp_amelia_providers_to_services ON wp_amelia_users.id = wp_amelia_providers_to_services.userId INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC LIMIT 10";
			}
			if($atts == 'newjoined')
			{
				$professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address ".$query.",(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback INNER JOIN wp_amelia_providers_to_services ON wp_amelia_customer_feedback.entityId = wp_amelia_providers_to_services.serviceId WHERE wp_amelia_providers_to_services.userId = wp_amelia_users.id) as totalRating FROM wp_amelia_users INNER JOIN wp_amelia_providers_to_services ON wp_amelia_users.id = wp_amelia_providers_to_services.userId INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_users.id DESC LIMIT 10";
			}
			
			$getProfessionals = $wpdb->get_results($professionals, OBJECT);
			if(count($getProfessionals) > 0)
			{
				if($atts == 'popular')
				{
					$serviceType = 'Our Popular Professionals';
					$moreUrl = get_permalink(61).'?param=popular-professionals';
				}
				if($atts == 'newjoined')
				{
					$serviceType = 'New Joined Professionals';
					$moreUrl = get_permalink(61).'?param=recent-professionals';
				}
				$html .= '<div class="sec-wrapper">
							<div class="sec-title">
								<h2>'.$serviceType.'</h2>
							</div>
							<div class="more-btn">
								<a href="'.$moreUrl.'">more</a>
							</div>
						</div>
							<div class="owl-carousel owl-theme service-slider">';
				foreach($getProfessionals as $pro)
				{
					if($currentWeekDay == 0)
					{
						$weekDay = 7;
					}
					else
					{
						$weekDay = $currentWeekDay;
					}
					$profdayOff = getProfDayOff($pro->id,$currentWeekDay,$currentTime);
					$getProfdayOff = explode(",",$profdayOff[0]);
					$profWeekDays = getProfWeekDaysWithTime($pro->id,$weekDay,$currentTime);
					$proSpecialDays = getSpecialDaysWithTime($pro->id,$currentDate,$currentTime);
					$getAppointments = getProApointmentWithTime($pro->id,$currentDate,$currentTime);
					if(in_array('"'.$currentDate.'"',$getProfdayOff))
					{
						$status = 'Day Off';
						$class = 'red-status';
					}
					else if(!in_array(1,$proSpecialDays['default']) && $proSpecialDays['type'] == 'avail')
					{
						$status = 'Away';
						$class = 'red-status';
					}
					else if(count($profWeekDays) == 0 && $proSpecialDays['type'] == 'notavail')
					{
						$status = 'Away';
						$class = 'red-status';
					}
					else if(count($getAppointments) > 0)
					{
						$status = 'Busy';
						$class = 'yellow-status';
					}
					else
					{
						$status = 'Available';
						$class = 'green-status';
					}
					if($pro->totalRatingSum != '' && $pro->totalRatingSum != 0)
					{
						$totalRating = $pro->totalRatingSum/$pro->totalRating;
					}
					else
					{
						$totalRating = 0;
					}
					if($latitude != '' && $longitude != '')
					{
						$point1 = array("lat" => $latitude, "long" => $longitude); 
						$point2 = array("lat" => $pro->latitude, "long" => $pro->longitude);
						$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km away'; 
					}
					
					$professionalUrl = get_site_url().'/professional-detail.?profId='.$pro->id;
					$html .= '<div class="slider-grid">
								<a href="'.$professionalUrl.'">';
									if($pro->pictureFullPath != '')
									{
										$html .= '<figure style="background-image: url('.$pro->pictureFullPath.');"></figure>';
									}
									else
									{
										$imgUrl = get_field('header_logo','options');
										$html .= '<figure class="by-default-image"><img src="'.$imgUrl['url'].'"></figure>';
									}
									
								$html .= '</a>
								<div class="content">
									<div class="top-sec">
										<div class="left-sec">
											<span class="'.$class.'">'.$status.'</span>
											<h4>
												<a href="'.$professionalUrl.'">'.ucfirst($pro->firstName).' '.ucfirst($pro->lastName).'</a>
											</h4>
										</div>
										<div class="right-sec">
											<span>'.round($totalRating,1).'<i class="fa fa-star" aria-hidden="true"></i></span>
										</div>
									</div>
									<div class="bottom-sec">
										<div class="location">
											<span><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km.'</span>
										</div>
									</div>
								</div>
							</div>';
				}
				$html .= '</div>';
			}
		}
		echo $html;die;
	}


/*============Short code for get services in home page=============*/

	function main_services_func($atts)
	{
		global $wpdb;
		$latitude = $_COOKIE['latitude'];
		$longitude = $_COOKIE['longitude'];
		$query = '';
		$distanceHaving = '';
		
		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
		}
		
		if($atts['type'] == 'popular')
		{
			$services = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName,wp_amelia_users.lastName ".$query." FROM wp_amelia_services LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_providers_to_services.userId = wp_amelia_providers_to_weekdays.userID WHERE wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_services.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC LIMIT 10";
			
		}
		if($atts['type'] == 'recent')
		{
			$services = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName,wp_amelia_users.lastName ".$query." FROM wp_amelia_services LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_providers_to_services.userId = wp_amelia_providers_to_weekdays.userID WHERE wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC LIMIT 10";
		}
		$getService = $wpdb->get_results($services, OBJECT);
		$html = '';
		if(count($getService) > 0)
		{
			if($atts['type'] == 'popular')
			{
				$serviceType = 'Our Popular Services';
				$moreUrl = get_permalink(61).'?param=popular-services';
			}
			if($atts['type'] == 'recent')
			{
				$serviceType = 'Our Recent Services';
				$moreUrl = get_permalink(61).'?param=recent-services';
			}
			$html .= '<div class="services-post">
						<div class="sec-wrapper">
							<div class="sec-title">
								<h2>'.$serviceType.'</h2>
							</div>
							<div class="more-btn">
								<a href="'.$moreUrl.'">more</a>
							</div>
						</div>
						<div class="owl-carousel owl-theme service-slider">';
			foreach($getService as $ser)
			{
				if($ser->pictureFullPath != '')
				{
					$img = $ser->pictureFullPath;
				}
				else
				{
					$getImg = get_field('header_logo','options');
					$img = $getImg['url'];
				}
				if($ser->totalRatingSum != '' && $ser->totalRatingSum != 0)
				{
					$totalRating = $ser->totalRatingSum/$ser->totalRating;
				}
				else
				{
					$totalRating = 0;
				}
				if($latitude != '' && $longitude != '')
				{
					$point1 = array("lat" => $latitude, "long" => $longitude); 
					$point2 = array("lat" => $ser->latitude, "long" => $ser->longitude);
					$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km'; 
				}
				
				
				$html .= '<div class="slider-grid">
							<a href="'.get_permalink(63).'?serviceId='.$ser->id.'">
								<figure style="background-image: url('.$img.');"></figure>
							</a>
							<div class="content">
								<div class="top-sec">
									<div class="left-sec">
										<h4>
											<a href="'.get_permalink(63).'?serviceId='.$ser->id.'">'.ucfirst($ser->name).'</a>
										</h4>
										<span>By '.ucfirst($ser->firstName).' '.ucfirst($ser->lastName).'</span>
									</div>
									<div class="right-sec">
										<span>'.$totalRating.'<i class="fa fa-star" aria-hidden="true"></i></span>
										<span>'.count(getServiceTotalBookings($ser->id)).' times sold</span>
									</div>
								</div>
								<div class="bottom-sec">
									<div class="price-sec">
										<span>'.get_field('currency',options).' '.$ser->price.'</span>
									</div>
									<div class="location">
										<span><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km.'</span>
									</div>
								</div>
							</div>
						</div>';
			}
			$html .= '</div></div>';
			echo $html;
		}	
	}
	add_shortcode( 'main_services', 'main_services_func' );

/*============Short code for get professionals in home page=============*/

	function main_professionals_func($atts)
	{
		global $wpdb;
		$latitude = $_COOKIE['latitude'];
		$longitude = $_COOKIE['longitude'];
		$query = '';
		$distanceHaving = '';
		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
			//$distanceHaving .= 'having distance <= 1';
		}
		if($atts['type'] == 'popular')
		{
			$professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_services.status = 'visible' AND wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC LIMIT 10";
		}
		if($atts['type'] == 'newjoined')
		{
			$professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_services.status = 'visible' AND wp_amelia_users.status = 'visible' AND wp_amelia_users.type = 'provider' AND wp_amelia_locations.status = 'visible' GROUP BY wp_amelia_users.id  ".$distanceHaving." ORDER BY wp_amelia_users.id DESC LIMIT 10";
		}
		
		$getProfessionals = $wpdb->get_results($professionals, OBJECT);
		$html = '';
		if(count($getProfessionals) > 0)
		{
			if($atts['type'] == 'popular')
			{
				$serviceType = 'Our Popular Professionals';
				$moreUrl = get_permalink(61).'?param=popular-professionals';
			}
			if($atts['type'] == 'newjoined')
			{
				$serviceType = 'New Joined Professionals';
				$moreUrl = get_permalink(61).'?param=recent-professionals';
			}
			$html .= '<div class="services-post">
						<div class="sec-wrapper">
							<div class="sec-title">
								<h2>'.$serviceType.'</h2>
							</div>
							<div class="more-btn">
								<a href="'.$moreUrl.'">more</a>
							</div>
						</div>
						<div class="owl-carousel owl-theme service-slider">';
			foreach($getProfessionals as $pro)
			{
				if($pro->pictureFullPath != '')
				{
					$proImg = $pro->pictureFullPath;
				}
				else
				{
					$imgUrl = get_field('header_logo','options');
					$proImg = $imgUrl['url'];
				}
				if($pro->totalRatingSum != '' && $pro->totalRatingSum != 0)
				{
					$totalRating = $pro->totalRatingSum/$pro->totalRating;
				}
				else
				{
					$totalRating = 0;
				}
				if($latitude != '' && $longitude != '')
				{
					$point1 = array("lat" => $latitude, "long" => $longitude); 
					$point2 = array("lat" => $pro->latitude, "long" => $pro->longitude);
					$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']).' '.'km away'; 
				}
				
				$professionalUrl = get_site_url().'/professional-detail.?profId='.$pro->id;
				$html .= '<div class="slider-grid">
							<a href="'.$professionalUrl.'">
								<figure style="background-image: url('.$proImg.');"></figure>
							</a>
							<div class="content">
								<div class="top-sec">
									<div class="left-sec">
										<span>Now available</span>
										<h4>
											<a href="'.$professionalUrl.'">'.ucfirst($pro->firstName).' '.ucfirst($pro->lastName).'</a>
										</h4>
									</div>
									<div class="right-sec">
										<span>'.$totalRating.'<i class="fa fa-star" aria-hidden="true"></i></span>
									</div>
								</div>
								<div class="bottom-sec">
									<div class="location">
										<span><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km.'</span>
									</div>
								</div>
							</div>
						</div>';
			}
			$html .= '</div></div>';
			echo $html;
		}
	}
	add_shortcode( 'main_professionals', 'main_professionals_func' );

/*==============get services in service detail page============*/

	function getServices($param,$profId)
	{
		global $wpdb;
		$latitude = $_COOKIE['latitude'];
		$longitude = $_COOKIE['longitude'];
		$where = '';
		if($latitude != '' && $longitude != '')
		{
			$query = ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN((".$latitude." - abs(latitude))*pi()/180/2),2)+COS(".$latitude."*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN((".$longitude."-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving = "having distance <= 4";
		}
		if($param == 'popular-services')
		{
			$serviceWhere .= $distanceHaving." ORDER BY wp_amelia_services.id DESC";
			$proWhere .= "  GROUP BY wp_amelia_users.id having ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
		}
		else if($param == 'recent-services')
		{
			$serviceWhere .= " having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " GROUP BY wp_amelia_users.id having distance <= 4 ORDER BY wp_amelia_services.id DESC";
		}
		else if($param == 'popular-professionals')
		{
			$serviceWhere .= " having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " GROUP BY wp_amelia_users.id having distance <= 4 ORDER BY wp_amelia_users.id DESC";
		}
		else if($param == 'recent-professionals')
		{
			$serviceWhere .= " having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " GROUP BY wp_amelia_users.id having distance <= 4 ORDER BY wp_amelia_users.id DESC";
		}
		else if($param != '' && $profId != '')
		{
			$serviceWhere .= " AND wp_amelia_categories.slug ='".$param."' AND wp_amelia_users.id = '".$profId."' having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " AND wp_amelia_categories.slug ='".$param."' AND wp_amelia_users.id = '".$profId."'  GROUP BY having distance <= 4 wp_amelia_users.id ORDER BY wp_amelia_services.id DESC";
		}
		
		else if($param != '')
		{
			$serviceWhere .= " AND wp_amelia_categories.slug ='".$param."' having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " AND wp_amelia_categories.slug ='".$param."' GROUP BY wp_amelia_users.id having distance <= 4 ORDER BY wp_amelia_services.id DESC";
		}
		else if(($param != 'recent-professionals' || $param == 'recent-professionals' || $param == 'recent-professionals' || $param == 'recent-professionals' || $param == 'recent-professionals') && $param != '')
		{
		    $serviceWhere .= " AND wp_amelia_services.name LIKE '%".$param."%' having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " AND wp_amelia_services.name LIKE '%".$param."%' having distance <= 4 ORDER BY wp_amelia_services.id DESC";
		}
		else
		{
			$serviceWhere .= " having distance <= 4 ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " GROUP BY wp_amelia_users.id having distance <= 4 ORDER BY wp_amelia_services.id DESC";
		}
		
		$cat = "SELECT wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_categories.status = 'visible' AND wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' ".$serviceWhere."";
		$getServices['services'] = $wpdb->get_results($cat, OBJECT);

		$professionals = "SELECT wp_amelia_users.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_services.status = 'visible' AND wp_amelia_locations.status = 'visible' ".$proWhere."";

		$getServices['professionals'] = $wpdb->get_results($professionals, OBJECT);

		return $getServices;
	}

/*==============Count all approved bookings with service id==============*/

	function getServiceTotalBookings($serviceId)
	{
		global $wpdb;
		$totalServiceBookings = "SELECT * FROM  wp_amelia_appointments WHERE serviceId = '".$serviceId."' AND status = 'approved'";
		$gettotalServiceBookings = $wpdb->get_results($totalServiceBookings, OBJECT);
		return $gettotalServiceBookings;
	}

/*===============Get service detail for service detaill page=============*/

	function getServiceDetail($serviceId)
	{
		global $wpdb;
		$services = "SELECT wp_amelia_services.*,wp_amelia_categories.id as catId,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.id as profId,wp_amelia_locations.latitude,wp_amelia_locations.longitude,wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.pictureFullPath as user_picture FROM wp_amelia_services LEFT JOIN wp_amelia_categories ON wp_amelia_services.categoryId = wp_amelia_categories.id LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId LEFT JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_services.id = '".$serviceId."'";
		$getServices = $wpdb->get_results($services, OBJECT);
		return $getServices;
	}

/*==============get service additional options=============*/

	function getServiceAdditionalOptions($serviceId)
	{
		global $wpdb;
		$additionalOptions = "SELECT * FROM wp_amelia_extras WHERE serviceId = '".$serviceId."' ORDER BY id DESC";
		$getAdditionalOptions = $wpdb->get_results($additionalOptions, OBJECT);
		return $getAdditionalOptions;
	}

/*==============get service gallery============*/

	function getServiceGallery($serviceId)
	{
		global $wpdb;
		$serviceGalery = "SELECT * FROM wp_amelia_galleries WHERE entityId = '".$serviceId."' AND entityType = 'service' ORDER BY id DESC";
		$getServiceGallery = $wpdb->get_results($serviceGalery, OBJECT);
		return $getServiceGallery;
	}

/*============get service feedback============*/

	function getServiceFeedback($serviceId)
	{
		global $wpdb;
		$serviceFeedback = "SELECT wp_amelia_customer_feedback.*,wp_amelia_users.firstName,wp_amelia_users.lastName,wp_amelia_users.pictureFullPath FROM wp_amelia_customer_feedback LEFT JOIN wp_amelia_users ON wp_amelia_customer_feedback.userId = wp_amelia_users.externalId WHERE wp_amelia_customer_feedback.entityId = '".$serviceId."' AND wp_amelia_customer_feedback.entityType = 'service' ORDER BY wp_amelia_customer_feedback.id DESC";
		$getServiceFeedback = $wpdb->get_results($serviceFeedback, OBJECT);
		return $getServiceFeedback;
	}

/*============get service feedback image=========*/

	function getFeedbackImage($feedId)
	{
		global $wpdb;
		$feedbackImage = "SELECT * FROM wp_customer_feedback_image WHERE feedbackId = '".$feedId."' ORDER BY id DESC";
		$getFeedbackImage = $wpdb->get_results($feedbackImage, OBJECT);
		return $getFeedbackImage;
	}

/*===========get professional categories===========*/

	function getProfessionalCat($profId)
	{
		global $wpdb;
		$professionalCat = "SELECT wp_amelia_categories.* FROM wp_amelia_providers_to_services LEFT JOIN wp_amelia_services ON wp_amelia_providers_to_services.serviceId = wp_amelia_services.id LEFT JOIN wp_amelia_categories ON wp_amelia_services.categoryId = wp_amelia_categories.id WHERE wp_amelia_providers_to_services.userId = '".$profId."' AND wp_amelia_services.status = 'visible' GROUP BY wp_amelia_categories.id";
		$getProfessionalCat = $wpdb->get_results($professionalCat, OBJECT);
		return $getProfessionalCat;
	}

/*==============Sign Up functionality=============*/

	add_action('wp_ajax_sign_up', 'amelia_sign_up');
	add_action('wp_ajax_nopriv_sign_up', 'amelia_sign_up');

	function amelia_sign_up()
	{
		global $wpdb;
		$data = $_POST;
		$role = $_POST['role'];  
		$email = $data['email'];
	    $phone_number = $data['phone_number'];

	    $checkUserDetail = check_user_exist($phone_number);
	    $checkUserEmail = check_user_email($email);
	    
	    if(count($checkUserDetail) > 0)
	    {
	    	$result['success'] = false;
	    	$result['msg'] = 'You have already registered';
	    }
	    else
	    {
	    	if(count($checkUserEmail) > 0)
	    	{
	    		$result['success'] = false;
	    		$result['msg'] = 'Email already exist';
	    	}
	    	else
	    	{
	    		$currentDateTime = date('Y-m-d H:i:s');
				$getPhonePin = get_phone_pin($phone_number);
				$pinCreatedAt = $getPhonePin[0]->created_at;
				$addFiveMin = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($pinCreatedAt)));
				if(count($getPhonePin) > 0)
			    {
					if($currentDateTime > $addFiveMin)
					{
						$result['success'] = false;
						$result['msg'] = 'Your OTP expired';
					}
					else
					{
						if($data['phone_pin'] == $getPhonePin[0]->sms_pin)
						{
							$user_id = wp_create_user( $phone_number, '', $phone_number);
							update_user_meta( $user_id, 'name', $data['firstName'].' '.$data['lastName'] );
							wp_update_user(array ('ID' => $user_id,  'display_name' => $data['firstName'])) ;
							update_user_meta( $user_id, 'address', $data['address'] );
							
							$user = new WP_User( $user_id );
							if($role == 'customer')
							{
								$wp_role = 'wpamelia-customer';
							}
							if($role == 'provider')
							{
								$wp_role = 'wpamelia-provider';
							}
							$user->set_role( $wp_role );

							if($user_id != '')
							{
								$wpdb->insert('wp_amelia_users', array(
									'status' => 'visible',
									'type' => $role, 
									'externalId' => $user_id, 
									'firstName' => $data['firstName'], 
									'lastName' => $data['lastName'], 
									'email' => $data['email'], 
									'phone' => $phone_number, 
								));
								$amelia_user_id = $wpdb->insert_id;
								
								if($role == 'provider')
								{
									$wpdb->insert('wp_amelia_locations', array(
									'status' => 'visible',
									'name' => 'My Location', 
									'description' => '', 
									'address' => $data['address'], 
									'phone' => '', 
									'latitude' => $data['address_latitude'], 
									'longitude' => $data['address_longitude'], 
									));
									$location_id = $wpdb->insert_id;
									
									$wpdb->insert('wp_amelia_providers_to_locations', array(
										'userId' => $amelia_user_id,
										'locationId' => $location_id, 
									));
								}
								
								if($role == 'customer')
								{
									$wpdb->insert('customer_address_detail', array(
										'user_id' => $amelia_user_id,
										'address' => $data['address'], 
										'latitude' => $data['address_latitude'], 
										'longitude' => $data['address_longitude'], 
									));
									$location_id = $wpdb->insert_id;
								}
								
								$updatePhoneNumber = $phone_number;
								$wpUserEmail = $data['email'];
								wp_set_password( $updatePhoneNumber, $user_id );
								$wpdb->query('UPDATE wp_users SET user_login = "'.$updatePhoneNumber.'", user_email = "'.$wpUserEmail.'" WHERE id ="'.$user_id.'"');
								$wpdb->query('DELETE FROM wp_user_sms_pin WHERE phone_number = "'.$phone_number.'"');
								
								$result['success'] = true;
								$result['msg'] = 'User registered successfully';
								
								if($role == 'customer')
								{
									$_SESSION['user_id'] = $user_id;
									if($_SESSION['user_id'] != '')
									{
										$result['url'] = get_site_url();
									}
									
								}
								else if($role == 'provider')
								{
									$getWordpressUserDetail = "SELECT * FROM wp_users WHERE id = '".$user_id."'";
									$getWpUser = $wpdb->get_results($getWordpressUserDetail, OBJECT);
									$creds = array();
									$creds['user_login'] = $getWpUser[0]->user_login;
									$creds['user_password'] = $updatePhoneNumber;
									$login_data['remember'] = true;
									$getWpUser = wp_signon( $creds, true );
									wp_clear_auth_cookie();
									wp_set_current_user($user_id);
									wp_set_auth_cookie($user_id);
									$url = get_admin_url().'admin.php?page=wpamelia-appointments#/appointments';
									$result['url'] = $url;
								}
							}
							else
							{
								$result['success'] = false;
								$result['msg'] = 'User not registered successfully';
							}
						}
						else
						{
							$result['success'] = false;
							$result['msg'] = 'Invalid pin';
						}
					}
			    }
				else
				{
					$result['success'] = false;
				    $result['msg'] = 'Kindly send pin';
				}
	    	}
			
		}
		echo json_encode($result);exit;                         
	}

/*==============Login functionality=============*/

	add_action('wp_ajax_amelia_login', 'amelia_user_login');
	add_action('wp_ajax_nopriv_amelia_login', 'amelia_user_login');

	function amelia_user_login()
	{
		global $wpdb;
		$data = $_POST;
		$phone_number = $data['amelia_phone_number'];
		$checkUserDetail = check_user_exist($phone_number);
		if(count($checkUserDetail) == 0)
	    {
	    	$result['success'] = false;
	    	$result['msg'] = 'Phone number not exist';
	    }
	    else
	    {
	    	$currentDateTime = date('Y-m-d H:i:s');
			$getPhonePin = get_phone_pin($phone_number);
			$pinCreatedAt = $getPhonePin[0]->created_at;
			$addFiveMin = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($pinCreatedAt)));
			if(count($getPhonePin) > 0)
		    {
				
				if($currentDateTime > $addFiveMin)
				{
					$result['success'] = false;
					$result['msg'] = 'Your OTP expired';
				}
				else
				{
					if($data['phone_pin'] == $getPhonePin[0]->sms_pin)
					{
						wp_set_password( $checkUserDetail[0]->phone, $checkUserDetail[0]->externalId );
						if($checkUserDetail[0]->type == 'customer')
						{
							setcookie('user_id', $checkUserDetail[0]->externalId,time()+31556926 , "/");
							setcookie('current_location', "",time()+31556926 , "/");
							$result['success'] = true;
							$result['msg'] = 'Login successfully';
							$result['url'] = get_site_url();
						}
						else if($checkUserDetail[0]->type == 'provider')
						{
							setcookie('user_id', $checkUserDetail[0]->externalId,time()+31556926 , "/");
							setcookie('current_location', "",time()+31556926 , "/");
							$getWordpressUserDetail = "SELECT * FROM wp_users WHERE id = '".$checkUserDetail[0]->externalId."'";
							$getWpUser = $wpdb->get_results($getWordpressUserDetail, OBJECT);
							$creds = array();
							$creds['user_login'] = $getWpUser[0]->user_login;
							$creds['user_password'] = $checkUserDetail[0]->phone;
							$login_data['remember'] = true;
							$getWpUser = wp_signon( $creds, true );
							wp_clear_auth_cookie();
							wp_set_current_user($getWpUser->ID);
							wp_set_auth_cookie($getWpUser->ID);
							$url = get_admin_url().'admin.php?page=wpamelia-appointments#/appointments';
							$result['success'] = true;
							$result['msg'] = 'Login successfully';
							$result['url'] = $url;
						}
						else
						{
							$result['success'] = false;
							$result['msg'] = 'Kindly register as a Professional or Customer';
						}
					}
					else
					{
						$result['success'] = false;
						$result['msg'] = 'Invalid pin';
					}
				}
			}
			else
			{
				$result['success'] = false;
			    $result['msg'] = 'Kindly send pin';
			}
		}
		echo json_encode($result);exit;                         
	}
/*=========Check User already exist or not=============*/

	function check_user_exist($phone_number)
	{
		global $wpdb;
		$checkUser = "SELECT * FROM wp_amelia_users WHERE phone = '".$phone_number."'";
	    $getUserDetail = $wpdb->get_results($checkUser, OBJECT);
	    return $getUserDetail;
	}

/*=========Check User email exist or not=============*/

	function check_user_email($email)
	{
		global $wpdb;
		$checkUser = "SELECT * FROM wp_amelia_users WHERE email = '".$email."'";
	    $getUserDetail = $wpdb->get_results($checkUser, OBJECT);
	    return $getUserDetail;
	}


/*==========Get user pin detail===========*/

	function get_phone_pin($phone_number)
	{
		global $wpdb;
		$phonePin = "SELECT * FROM wp_user_sms_pin WHERE phone_number = '".$phone_number."'";
		$getPhonePinDetail = $wpdb->get_results($phonePin, OBJECT);
		return $getPhonePinDetail;
	}

/*===========Send phone pin==========*/

	add_action('wp_ajax_send_phone_pin', 'amelia_send_phone_pin');
	add_action('wp_ajax_nopriv_send_phone_pin', 'amelia_send_phone_pin');

	function amelia_send_phone_pin()
	{
		include plugins_url().'/sendex/admin/class-sendex-admin.php';
	}

/*===============Book customer service=============*/

	add_action('wp_ajax_book_customer_service', 'customer_booked_service');
	add_action('wp_ajax_nopriv_book_customer_service', 'customer_booked_service');

	function customer_booked_service()
	{
		global $wpdb;
		$userId = $_SESSION['user_id'];
		$data = $_POST;
		$data['customer_address'] = '';
		$data['customer_additional_address'] = '';
		$data['customer_name'] = '';
		$data['customer_address_latitude'] = '';
		$data['customer_address_longitude'] = '';
		unset($_SESSION["bookingDetails"]);
		$_SESSION["bookingDetails"] = $data;
		$result['success'] = true;
		$result['url'] = get_site_url().'/order/?orderId='.base64_encode($userId);
		echo json_encode($result);exit;
	}

	function getServiceExtraPrice($extraId)
	{
		global $wpdb;
		$extraService = "SELECT * FROM  wp_amelia_extras WHERE id = '".$extraId."'";
		$getExtraService = $wpdb->get_results($extraService, OBJECT);
		return $getExtraService;
	}
	
/*=======Get extra service detail with extra service Id for order page=========*/

	function extraServiceDetail($extraId)
	{
		global $wpdb;
		$extraService = "SELECT * FROM  wp_amelia_extras WHERE id IN (".$extraId.")";
		$getExtraService = $wpdb->get_results($extraService, OBJECT);
		return $getExtraService;
		
	}

	function getCustomerBookingDetail($appointmentId)
	{
		global $wpdb;

		$appointmentId = base64_decode($appointmentId);


		$bookingDetail = "SELECT wp_amelia_appointments.*,wp_amelia_customer_bookings.*,wp_amelia_customer_bookings.id as bookingId FROM  wp_amelia_appointments LEFT JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId WHERE wp_amelia_appointments.id = '".$appointmentId."'";
		$getBookingDetail = $wpdb->get_results($bookingDetail, OBJECT);
		return $getBookingDetail;
	}

	function getExtraServiceDetail($bookingId)
	{
		global $wpdb;
		$extraService = "SELECT (SELECT SUM(wp_amelia_customer_bookings_to_extras.price) FROM wp_amelia_customer_bookings_to_extras WHERE wp_amelia_customer_bookings_to_extras.customerBookingId = $bookingId) AS totalExtraPrice, wp_amelia_extras.* FROM  wp_amelia_customer_bookings_to_extras LEFT JOIN wp_amelia_extras ON wp_amelia_customer_bookings_to_extras.extraId = wp_amelia_extras.id WHERE wp_amelia_customer_bookings_to_extras.customerBookingId = '".$bookingId."'";
		$getExtraService = $wpdb->get_results($extraService, OBJECT);
		return $getExtraService;
	}

/*================get time slot on click date==============*/

	function getSpecialDays($profId)
	{
		global $wpdb;
		$specialDays = "SELECT * FROM wp_amelia_providers_to_specialdays WHERE userId = '".$profId."'";
		$getSpecialDays = $wpdb->get_results($specialDays, OBJECT);
		$specialDay = array();
		if(count($getSpecialDays) > 0)
		{
			$Date1 = $getSpecialDays[0]->startDate; 
			$Date2 = $getSpecialDays[0]->endDate; 
			$array = array(); 
			$Variable1 = strtotime($Date1); 
			$Variable2 = strtotime($Date2); 
			for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) 
			{ 
				if(date('Y-m-d', $currentDate) >= date('Y-m-d'))
				{
					$Store = date('m/d/Y', $currentDate);
					$array[] = $Store; 
				}				
				
			}
			$specialDay = '"'.implode('","',$array).'"';
		}			
		return $specialDay;
	}

	function getBookingTimeSlot($param)
	{
		
		global $wpdb;
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i');

		/*==========get weekdays slots=========*/
		
		$timeSlot = "SELECT * FROM  wp_amelia_providers_to_weekdays WHERE userId = '".$param['profId']."' AND dayIndex = '".$param['weekDay']."'";
		$getTimeSlot = $wpdb->get_results($timeSlot, OBJECT);
		
		
		/*=====professional timeout======*/

		$timeOut = "SELECT * FROM  wp_amelia_providers_to_timeouts WHERE weekDayId='".$getTimeSlot[0]->id."'";
		$getTimeOut = $wpdb->get_results($timeOut, OBJECT);
		if(count($getTimeOut) > 0)
		{
			$startTime = date('H:i',strtotime($getTimeOut[0]->startTime));
			$endTime = date('H:i',strtotime($getTimeOut[0]->endTime));
			$totalSlots[] = array('startTime'=>$startTime,'endTime'=>$endTime);
		}
		
		/*=========get service appointmnets========*/

		$bookings = "SELECT * FROM `wp_amelia_appointments` WHERE (DATE(`bookingStart`)='".$param['selectedDate']."' AND DATE(`bookingEnd`)>='".$param['selectedDate']."') AND `status`= 'approved' AND `serviceId` = '".$param['serviceId']."'";
		$getBookings = $wpdb->get_results($bookings, OBJECT);
		if(count($getBookings) > 0)
		{
			foreach($getBookings as $book_slots)
			{
				$startTime = date('H:i',strtotime($book_slots->bookingStart));
				$endTime = date('H:i',strtotime($book_slots->bookingEnd));
				$totalSlots[] = array('startTime'=>$startTime,'endTime'=>$endTime);
			}
		}
		/*========professional special days=========*/

		$specialDays = "SELECT wp_amelia_providers_to_specialdays_periods.* FROM  wp_amelia_providers_to_specialdays INNER JOIN wp_amelia_providers_to_specialdays_periods ON wp_amelia_providers_to_specialdays.id = wp_amelia_providers_to_specialdays_periods.specialDayId WHERE (wp_amelia_providers_to_specialdays.startDate<='".$param['selectedDate']."' AND endDate>='".$param['selectedDate']."') AND wp_amelia_providers_to_specialdays.userId = '".$param['profId']."'";
		$getSpecialDays = $wpdb->get_results($specialDays, OBJECT);
		
		/*============create time slot for booking=========*/
		
		if(count($getSpecialDays) > 0)
		{
			foreach($getSpecialDays as $special_slots)
			{
				$startTime = date('H:i',strtotime($special_slots->startTime));
				$endTime = date('H:i',strtotime($special_slots->endTime));
				$specialSlots[] = array('startTime'=>$startTime,'endTime'=>$endTime);
			}
			$booking_frequency           = $param['duration']/60;             
			$day_format                    = 1; 
			foreach($specialSlots as $val)
			{
				$j=0;  
				for($i = strtotime($val['startTime']); $i< strtotime($val['endTime']); $i = $i + $booking_frequency * 60) 
				{
					$finish_time = strtotime(date("H:i", $i)) + $booking_frequency * 60;
					
					if(date("H:i", $finish_time) <= $val['endTime'])
					{
						$slot[$j]['startTime'] = date("H:i", $i);
						$slot[$j]['endTime'] = date("H:i", $finish_time);
					}
					$j++;
				}
				foreach($slot as $slotVal)
				{
					$slots[] = $slotVal;
				}
				
			}
		}
		else 
		{
			if(count($getTimeSlot) > 0)
			{
				$booking_start_time          = $getTimeSlot[0]->startTime;         
				$booking_end_time            = $getTimeSlot[0]->endTime;
			}
			else
			{
				$booking_start_time          = '09:00';         
				$booking_end_time            = '17:00';
			}
			$booking_frequency           = $param['duration']/60;             
			$day_format                    = 1;  
			$j=0;  
			for($i = strtotime($booking_start_time); $i< strtotime($booking_end_time); $i = $i + $booking_frequency * 60) 
			{
				$finish_time = strtotime(date("H:i", $i)) + $booking_frequency * 60;
				if(date("H:i", $finish_time) < $booking_end_time)
				{
					$slots[$j]['startTime'] = date("H:i", $i);
					$slots[$j]['endTime'] = date("H:i", $finish_time);
				}
				$j++;
			}
		}
		$data= array();
		
		foreach($slots as $start) 
		{
			$disable = false;
			if($currentTime == $start['startTime'] && $currentDate == $param['selectedDate'])
			{
				$disable = true;
			}
			if($currentTime >= $start['startTime'] && $currentDate == $param['selectedDate'])
			{
				$disable = true;
			}
			if(count($totalSlots) > 0)  
			{
				
				foreach($totalSlots as $i=>$val)
				{
					if(($val['startTime'] >= $start['startTime'] && $val['endTime'] <= $start['endTime']) || ($val['startTime'] <= $start['startTime'] && $val['endTime'] >= $start['endTime']))
					{
						$disable = true;
					}
				}
			} 
			if($disable == false)
			{
				$data[] = $start['startTime'] .' - '.$start['endTime'];
			}
		}
		
		return $data;
    }

    add_action('wp_ajax_get_time_slots', 'time_slots');
	add_action('wp_ajax_nopriv_book_get_time_slots', 'time_slots');

    function time_slots()
    {
    	echo 'hello';
    }

 /*=============get customer detail with customer id================*/

 	function getCustomerDetail($customerId)
 	{
 		global $wpdb;
 		$customerDetail = "SELECT * FROM wp_amelia_users WHERE id = '".$customerId."'";
		$getCustomerDetail = $wpdb->get_results($customerDetail, OBJECT);
 		$data['name'] = $getCustomerDetail[0]->firstName.' '.$getCustomerDetail[0]->lastName;
 		$data['phone'] = $getCustomerDetail[0]->phone;
		return $data;
 		
 	}

/*==============Add customer confirm location for booking==========*/

 	add_action('wp_ajax_add_customer_address', 'customer_address');
 	add_action('wp_ajax_nopriv_add_customer_address', 'customer_address');

 	function customer_address()
 	{
		global $wpdb;
 		$data = $_POST;
		$getProviderDetail = getProvideruserDetail($_SESSION['bookingDetails']['providerId']);
		$providerRange = $getProviderDetail[0]->rangeWilling;
		$lat1 = $data['customer_address_latitude'];
		$lon1 = $data['customer_address_longitude'];
		$lat2 = $getProviderDetail[0]->latitude;
		$lon2 = $getProviderDetail[0]->longitude;
		if (($lat1 == $lat2) && ($lon1 == $lon2)) 
		{
			$kmmiles = 0;
		}
		else 
		{
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$kmmiles = $miles * 1.609344;
		}
		if($kmmiles > $providerRange)
		{
			$result['msg'] = 'Kindly select location in range '.$providerRange.' km';
			$result['success'] = false;
		}
		else
		{
			$_SESSION['bookingDetails']['customer_address'] = $data['customer_address'];
			$_SESSION['bookingDetails']['customer_address_latitude'] = $data['customer_address_latitude'];
			$_SESSION['bookingDetails']['customer_address_longitude'] = $data['customer_address_longitude'];
			$_SESSION['bookingDetails']['customer_additional_address'] = $data['additional_address'];
			$_SESSION['bookingDetails']['customer_name'] = $data['name'];
			$result['data'] = $data;
			$result['success'] = true;
		}
		echo json_encode($result);exit;
	}

/*==============Update profile in dashboard==========*/

 	add_action('wp_ajax_update_dashboard_profile', 'dashboard_profile');
 	add_action('wp_ajax_nopriv_update_dashboard_profile', 'dashboard_profile');

 	function dashboard_profile()
 	{
		global $wpdb;
		$userId = $_SESSION['user_id'];
		$getCurrentUserDetail = getLoggedInUserDetail($userId);
		if($_POST['update_pin'] && $_POST['update_pin'] == 'yes')
		{
			include plugins_url().'/sendex/admin/class-sendex-admin.php';
		}
		else if($_POST['update_pin'] == 'confirm')
		{
			if($_POST['phone_pin'] == '')
			{
				$result['success'] = false;
				$result['msg'] = 'Kindly enter phone pin';
			}
			else
			{
				$checkPhoneNumberUser = "SELECT * FROM wp_amelia_users WHERE id= '".$_POST['user_id']."' AND phone = ".$_POST['phone_number'];
				$getCheckPhoneNumberUser = $wpdb->get_results($checkPhoneNumberUser, OBJECT);
				if(count($getCheckPhoneNumberUser) > 0)
				{
					$result['success'] = false;
					$result['msg'] = 'You have already login with this phone number';
				}
				else
				{
					$checkPhoneNumberUser1 = "SELECT * FROM wp_amelia_users WHERE id != '".$_POST['user_id']."' AND phone = ".$_POST['phone_number'];
					$getCheckPhoneNumberUser1 = $wpdb->get_results($checkPhoneNumberUser1, OBJECT);
					if(count($getCheckPhoneNumberUser1) > 0)
					{
						$result['success'] = false;
						$result['msg'] = 'Phone number already exist';
					}
					else
					{ 
						$currentDateTime = date('Y-m-d H:i:s');
						$getPhonePin = get_phone_pin($_POST['phone_number']);
						$pinCreatedAt = $getPhonePin[0]->created_at;
						$addFiveMin = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($pinCreatedAt)));
						if(count($getPhonePin) > 0)
						{
							if($currentDateTime > $addFiveMin)
							{
								$result['success'] = false;
								$result['msg'] = 'Your OTP expired';
							}
							else
							{
								if($_POST['phone_pin'] == $getPhonePin[0]->sms_pin)
								{
									$result['success'] = true;
									$result['msg'] = 'Phone number confirmed successfully';
									$result['confirmed'] = 'confirmed';
								}
								else
								{
									$result['success'] = false;
									$result['msg'] = 'Invalid pin';
								}
							}
						}
						else
						{
							$result['success'] = false;
							$result['msg'] = 'Kindly send pin';
						}
					}
				}
			}
			echo json_encode($result);exit;
		}
		else
		{
			$checkPhoneNumberUser = "SELECT * FROM wp_amelia_users WHERE id= '".$_POST['user_id']."' AND phone = ".$_POST['phone_number'];
			$getCheckPhoneNumberUser = $wpdb->get_results($checkPhoneNumberUser, OBJECT);
			if(count($getCheckPhoneNumberUser) == 0)
			{
				if($_POST['confirmed'] == '')
				{
					$result['success'] = false;
					$result['msg'] = 'Kindly confirm your new number';
				}
				else
				{
					$checkEmail = "SELECT * FROM wp_amelia_users WHERE id != '".$_POST['user_id']."' AND email = '".$_POST['email']."'";
					$checkUserEmail = $wpdb->get_results($checkEmail, OBJECT);
					if(count($checkUserEmail) > 0)
					{
						$result['success'] = false;
						$result['msg'] = 'Email already exist';
					}
					else
					{
						$customerId = $_SESSION['user_id'];
						$path = wp_upload_dir();
						$param = array(
							'firstName' => $_POST['firstName'],
							'lastName' => $_POST['lastName'],
							'phone' => $_POST['phone_number'],
							'email' => $_POST['email'],
						);
						if(isset($_FILES['profile_image']) && $_FILES['profile_image']['name'] != '')
						{
							$file_name = $_FILES['profile_image']['name'];
							$file_size =$_FILES['profile_image']['size'];
							$file_tmp =$_FILES['profile_image']['tmp_name'];
							$file_type=$_FILES['profile_image']['type'];
							$file_ext=strtolower(end(explode('.',$_FILES['profile_image']['name'])));
							$temp_name = $file_tmp;
							$fileName = time().'.'.$file_name;
							$target_path = $path['path'].'/'.$fileName;
							move_uploaded_file($temp_name,$target_path);
							$param['pictureFullPath'] = $path['url'].'/'.$fileName;
						}
						if($wpdb->update('wp_amelia_users', $param,array('externalId' => $customerId)) === False)
						{
							$result['success'] = false;
							$result['msg'] = 'Profile not updated successfully';
						}
						else
						{
							wp_update_user( array ( 'ID' => $customerId, 'user_login' => $_POST['phone_number'] ) ) ;
							
							$checkAddress = "SELECT * FROM customer_address_detail WHERE user_id = '".$getCurrentUserDetail[0]->id."'";
							$userAddressDetail = $wpdb->get_results($checkAddress, OBJECT);
							if(count($userAddressDetail) == 0)
							{
								$wpdb->insert('customer_address_detail', array(
									'user_id' => $getCurrentUserDetail[0]->id,
									'latitude' => $_POST['latitude'], 
									'longitude' => $_POST['longitude'], 
									'address' => $_POST['address'], 
								));
							}
							else
							{
								$wpdb->query('UPDATE customer_address_detail SET address = "'.$_POST['address'].'", latitude = "'.$_POST['latitude'].'", longitude = "'.$_POST['longitude'].'" WHERE user_id ="'.$getCurrentUserDetail[0]->id.'"');
							}
							$result['success'] = true;
							$result['msg'] = 'Profile updated successfully';
						}

					}
					
				}
			}
			else
			{
				$checkEmail = "SELECT * FROM wp_amelia_users WHERE id != '".$_POST['user_id']."' AND email = '".$_POST['email']."'";
				$checkUserEmail = $wpdb->get_results($checkEmail, OBJECT);
				if(count($checkUserEmail) > 0)
				{
					$result['success'] = false;
					$result['msg'] = 'Email already exist';
				}
				else
				{
					$customerId = $_SESSION['user_id'];
					$path = wp_upload_dir();
					$param = array(
						'firstName' => $_POST['firstName'],
						'lastName' => $_POST['lastName'],
						'phone' => $_POST['phone_number'],
						'email' => $_POST['email'],
					);
					if(isset($_FILES['profile_image']) && $_FILES['profile_image']['name'] != '')
					{
						$file_name = $_FILES['profile_image']['name'];
						$file_size =$_FILES['profile_image']['size'];
						$file_tmp =$_FILES['profile_image']['tmp_name'];
						$file_type=$_FILES['profile_image']['type'];
						$file_ext=strtolower(end(explode('.',$_FILES['profile_image']['name'])));
						$temp_name = $file_tmp;
						$fileName = time().'.'.$file_name;
						$target_path = $path['path'].'/'.$fileName;
						move_uploaded_file($temp_name,$target_path);
						$param['pictureFullPath'] = $path['url'].'/'.$fileName;
					}


					if($wpdb->update('wp_amelia_users', $param,array('externalId' => $customerId)) === False)
					{
						$result['success'] = false;
						$result['msg'] = 'Profile not updated successfully';
					}
					else
					{
						setcookie('current_location', "",time()+31556926 , "/");
						wp_update_user( array ( 'ID' => $customerId, 'user_login' => $_POST['phone_number'] ) ) ;
						
						$checkAddress = "SELECT * FROM customer_address_detail WHERE user_id = '".$getCurrentUserDetail[0]->id."'";
						$userAddressDetail = $wpdb->get_results($checkAddress, OBJECT);
						if(count($userAddressDetail) == 0)
						{
							$wpdb->insert('customer_address_detail', array(
								'user_id' => $getCurrentUserDetail[0]->id,
								'latitude' => $_POST['latitude'], 
								'longitude' => $_POST['longitude'], 
								'address' => $_POST['address'], 
							));
						}
						else
						{
							$wpdb->query('UPDATE customer_address_detail SET address = "'.$_POST['address'].'", latitude = "'.$_POST['latitude'].'", longitude = "'.$_POST['longitude'].'" WHERE user_id ="'.$getCurrentUserDetail[0]->id.'"');
						}
						$result['success'] = true;
						$result['msg'] = 'Profile updated successfully';
					}
				}
			}
			echo json_encode($result);exit;
		}
	}

/*===========Customer Approved Appointments==========*/

	function getCustomerAppointmentsAccToDate($customerId,$date='')
	{
		global $wpdb;
		$where = '';
		if($date != '')
		{
			$where .= " AND date(wp_amelia_appointments.bookingStart) = '".$date."'";
		}
		$appointment = "SELECT wp_amelia_appointments.*,count(wp_amelia_appointments.id) as totalServices FROM  wp_amelia_appointments INNER JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId WHERE wp_amelia_customer_bookings.customerId = '".$customerId."' AND wp_amelia_appointments.status = 'approved' ".$where."  GROUP BY date(wp_amelia_appointments.bookingStart) ORDER BY date(wp_amelia_appointments.bookingStart) ASC";
		$getAppointment = $wpdb->get_results($appointment, OBJECT);
		return $getAppointment;
	}
	
/*=============Customer Pending Appointments===========*/

	function getCustomerPendingAppointmentsAccToDate($customerId,$date='')
	{
		global $wpdb;
		$where = '';
		if($date != '')
		{
			$where .= " AND date(wp_amelia_appointments.bookingStart) = '".$date."'";
		}
		$appointment = "SELECT wp_amelia_appointments.*,count(wp_amelia_appointments.id) as totalServices FROM  wp_amelia_appointments INNER JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId WHERE wp_amelia_customer_bookings.customerId = '".$customerId."' AND wp_amelia_appointments.status = 'pending' ".$where."  GROUP BY date(wp_amelia_appointments.bookingStart) ORDER BY date(wp_amelia_appointments.bookingStart) ASC";
		$getAppointment = $wpdb->get_results($appointment, OBJECT);
		return $getAppointment;
	}
	
/*============Customer completed appointments=============*/

	function getCustomerCompletedAppointmentsAccToDate($customerId)
	{
		global $wpdb;
		$where = '';
		$currentTime = date('Y-m-d H:i:s');
		$appointment = "SELECT wp_amelia_customer_feedback.id as entityId,wp_amelia_appointments.*,count(wp_amelia_appointments.id) as totalServices FROM  wp_amelia_appointments INNER JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId  LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_appointments.id = wp_amelia_customer_feedback.appointmentId WHERE wp_amelia_customer_bookings.customerId = '".$customerId."' AND (wp_amelia_appointments.bookingEnd <= '".$currentTime."' AND wp_amelia_customer_feedback.entityId is NULL AND wp_amelia_appointments.status = 'approved') OR (wp_amelia_customer_feedback.entityId is NULL AND wp_amelia_appointments.status = 'completed') GROUP BY date(wp_amelia_appointments.bookingStart) ORDER BY date(wp_amelia_appointments.bookingStart) ASC";
		$getAppointment = $wpdb->get_results($appointment, OBJECT);
		return $getAppointment;
	}

/*===========Customer All Appointments==========*/

	function getCustomerAllAppointments($customerId,$date='')
	{
		global $wpdb;
		$currentdate = date('Y-m-d');
		$where = '';
		if($date != '')
		{
			$where .= " AND date(wp_amelia_appointments.bookingStart) = '".$date."'";
		}
		$appointment = "SELECT wp_amelia_appointments.*,count(wp_amelia_appointments.id) as totalServices FROM  wp_amelia_appointments INNER JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId WHERE wp_amelia_customer_bookings.customerId = '".$customerId."' AND date(wp_amelia_appointments.bookingStart) <= '".$currentdate."' ".$where." GROUP BY date(wp_amelia_appointments.bookingStart) ORDER BY date(wp_amelia_appointments.bookingStart) DESC";
		$getAppointment = $wpdb->get_results($appointment, OBJECT);
		return $getAppointment;
	}

/*===========Customer Cancel Appointments==========*/

	function getCustomerCancelAppointments($customerId,$date='')
	{
		global $wpdb;
		$where = '';
		if($date != '')
		{
			$where .= " AND date(wp_amelia_appointments.bookingStart) = '".$date."'";
		}
		$appointment = "SELECT wp_amelia_appointments.*,count(wp_amelia_appointments.id) as totalServices FROM  wp_amelia_appointments INNER JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId WHERE wp_amelia_customer_bookings.customerId = '".$customerId."' AND (wp_amelia_appointments.status = 'canceled' OR wp_amelia_appointments.status = 'rejected') ".$where." GROUP BY date(wp_amelia_appointments.bookingStart) ORDER BY date(wp_amelia_appointments.bookingStart) ASC";
		$getAppointment = $wpdb->get_results($appointment, OBJECT);
		return $getAppointment;
	}

/*============Get services appointment by date==========*/

	function getServicesAppointmentByDate($bookingDate,$status,$customerId)
	{
		global $wpdb;
		$where = '';
		if($status != '' && $status == 'canceled')
		{
			$where .= ' And (wp_amelia_appointments.status="'.$status.'" OR wp_amelia_appointments.status="rejected")';
		}
		else if($status != '' && $status != 'canceled')
		{
			$where .= ' And wp_amelia_appointments.status="'.$status.'"';
		}

		$services = "SELECT wp_amelia_services.name,wp_amelia_appointments.*,wp_amelia_locations.address,wp_amelia_users.firstName,wp_amelia_users.lastName FROM  wp_amelia_appointments LEFT JOIN wp_amelia_users ON wp_amelia_appointments.providerId = wp_amelia_users.id LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id LEFT JOIN wp_amelia_services ON wp_amelia_appointments.serviceId = wp_amelia_services.id INNER JOIN wp_amelia_customer_bookings ON wp_amelia_appointments.id = wp_amelia_customer_bookings.appointmentId WHERE wp_amelia_customer_bookings.customerId = '".$customerId."' AND date(wp_amelia_appointments.bookingStart) = '".$bookingDate."' ".$where." ORDER By wp_amelia_appointments.bookingStart ASC";
		$getServices = $wpdb->get_results($services, OBJECT);
		return $getServices;
	}

/*===========Cancel appointment by customer============*/

	add_action('wp_ajax_cancel_appointment', 'customer_cancel_appointment');
 	add_action('wp_ajax_nopriv_cancel_appointment', 'customer_cancel_appointment');

	function customer_cancel_appointment()
	{
		include plugins_url().'/sendex/admin/class-sendex-admin.php';
	}

/*===========Service rating by customer============*/

	add_action('wp_ajax_service_rating', 'customer_service_rating');
 	add_action('wp_ajax_nopriv_service_rating', 'customer_service_rating');

	function customer_service_rating()
	{
		global $wpdb;
		$userId = $_SESSION['user_id'];
		$path = wp_upload_dir();
		$wpdb->insert('wp_amelia_customer_feedback', array(
			'rating' => $_POST['rating'],
			'comment' => $_POST['comment'], 
			'entityId' => $_POST['serviceId'], 
			'appointmentId' => $_POST['appointmentId'], 
			'entityType' => 'service', 
			'userId' => $userId, 
		));
		$ratingId = $wpdb->insert_id;
		if($ratingId != '' || $ratingId != 0)
		{
			foreach($_FILES['service_images']['name'] as $key=>$val)
			{
				$file_name = $val;
				$file_size =$_FILES['service_images']['size'][$key];
				$file_tmp =$_FILES['service_images']['tmp_name'][$key];
			    $file_type=$_FILES['service_images']['type'][$key];
			    $file_ext=strtolower(end(explode('.',$val)));
			    $temp_name = $file_tmp;
			    $fileName = time().'.'.$file_name;
			    $target_path = $path['path'].'/'.$fileName;
			    move_uploaded_file($temp_name,$target_path);
			    $wpdb->insert('wp_customer_feedback_image', array(
					'feedbackId' => $ratingId,
					'image' => $path['url'].'/'.$fileName,
				));
			}
			$result['success'] = true;
			$result['url'] = get_site_url().'/dashboard-appointment';
			$result['msg'] = 'Rating successfully';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Rating not successfully';
		}
		echo json_encode($result);exit;
	}
	
/*============Get service detail when click on review=========*/
	add_action('wp_ajax_get_service_detail_for_review', 'get_service_detail_for_review');
 	add_action('wp_ajax_nopriv_get_service_detail_for_review', 'get_service_detail_for_review');
	function get_service_detail_for_review()
	{
		$serviceId = $_POST['serviceId'];
		global $wpdb;
		$html = '';
		$services = "SELECT wp_amelia_services.*,wp_amelia_categories.id as catId,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.id as profId,wp_amelia_locations.latitude,wp_amelia_locations.longitude,wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.pictureFullPath as user_picture FROM wp_amelia_services LEFT JOIN wp_amelia_categories ON wp_amelia_services.categoryId = wp_amelia_categories.id LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId LEFT JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id LEFT JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId LEFT JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id WHERE wp_amelia_services.id = '".$serviceId."'";
		$getServiceDetail = $wpdb->get_results($services, OBJECT);
		$getExtraServiceDetail = extraServiceDetail($getBookingDetail['extra_service']);
		$extraServiceDuration = '';
		$extraServicePrice = '';
		$latitude = $_COOKIE['latitude'];
		$longitude = $_COOKIE['longitude'];
		$point1 = array("lat" => $latitude, "long" => $longitude); 
		$point2 = array("lat" => $getServiceDetail[0]->latitude, "long" => $getServiceDetail[0]->longitude); 
		$km = distanceCalculation($point1['lat'], $point1['long'], $point2['lat'], $point2['long']);
		if(count($getExtraServiceDetail)>0)
		{
			foreach($getExtraServiceDetail as $val)
			{
				$extraServiceDuration += $val->duration;
				$extraServicePrice += $val->price;
			}
		}
		$totalServiceDuration = $getServiceDetail[0]->duration+$extraServiceDuration;
		$totalServicePrice = $getServiceDetail[0]->price+$extraServicePrice;
		if($getServiceDetail[0]->pictureFullPath != '')
		{
			$serviceImg = $getServiceDetail[0]->pictureFullPath;
		}
		else
		{
			$getImg = get_field('header_logo','options');
			$serviceImg = $getImg['url'];
		}
		$html = '<div class="order-info-wrap">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="order_item left_sec">
                            <figure style="background-image: url('.$serviceImg.')"></figure>
                            <div class="top_sec">
                                <div class="title">
                                    <h4>'.ucfirst($getServiceDetail[0]->name).'</h4>
                                    <span>By '.$getServiceDetail[0]->firstName.' '.$getServiceDetail[0]->lastName.'</span>
                                </div>
                                <!-- title -->
                                <div class="range">
                                    <span>';
											if($getServiceDetail[0]->totalRatingSum != '' && $getServiceDetail[0]->totalRatingSum != 0)
                                            {
                                                $totalRating = $getServiceDetail[0]->totalRatingSum/$getServiceDetail[0]->totalRating;
                                            }
                                            else
                                            {
                                                $totalRating = 0;
                                            }

                                            $html .= round($totalRating,1);
                                        
                                    $html.='<i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <!-- range -->  
                            </div>
                            <!-- top_sec -->
                            <div class="bottom_sec">
                                <ul>
                                    <li>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>';
                                        
                                            $duration = convertIntoMin($totalServiceDuration);
                                            $html .= $duration;
                                         
                                    $html .= '</li>
										<li>'.get_field('currency','option').' '.number_format($totalServicePrice,2).'</li>
										<li><i class="fa fa-map-marker" aria-hidden="true"></i>'.$km.' km</li>
                                </ul>
                            </div>
                            <!-- bottom_sec -->
                        </div>
                        <!-- order_item -->
                    </div>
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="order_item right_sec">
                            <h5>Additional options:</h5>
                            <ul>';
                                 
								if(count($getExtraServiceDetail) > 0)
								{
									foreach($getExtraServiceDetail as $val)
									{
										$html .= '<li>'.$val->name.'</li>';
									}
								}
								else
								{
									$html .= '- None';
								}
							$html .= '</ul>
                        </div>
                    </div>
                </div>
            </div>';
		echo $html;die;
	}

/*===========get customer service review with booking id==========*/

	function getCustomerServiceReview($serviceId,$bookingId)
	{
		global $wpdb;
		$userId = $_SESSION['user_id'];
		$rating = "SELECT *  FROM  wp_amelia_customer_feedback  WHERE appointmentId = '".$bookingId."' AND entityId ='".$serviceId."' AND userId = '".$userId."'";
		$getRating = $wpdb->get_results($rating, OBJECT);
		return $getRating;
	}

	add_action('wp_ajax_confirm_order_now', 'confirm_order');
 	add_action('wp_ajax_nopriv_confirm_order_now', 'confirm_order');

/*=========confirm order now==========*/

	function confirm_order()
	{
		include plugins_url().'/sendex/admin/class-sendex-admin.php';
	}

	function employee_registration_redirect() {
		return home_url( '/wp-admin/admin.php?page=wpamelia-calendar' );
	}

	add_filter( 'registration_redirect', 'employee_registration_redirect' );
	
	function getCountryList()
	{
		global $wpdb;
		$countries = "SELECT *  FROM  countries";
		$getCountries = $wpdb->get_results($countries, OBJECT);
		return $getCountries;
	}
	
	function getCities()
	{
		global $wpdb;
		$cities = "SELECT *  FROM  cities";
		$getCities = $wpdb->get_results($cities, OBJECT);
		return $getCities;
	}
	
	function getProfessionalWeekDays($profId)
	{
		global $wpdb;
		$weekDays = "SELECT *  FROM  wp_amelia_providers_to_weekdays WHERE userId =".$profId;
		$getWeekDays = $wpdb->get_results($weekDays, OBJECT);
		
		if(count($getWeekDays) > 0)
		{
			foreach($getWeekDays as $val)
			{
				if($val->dayIndex != 7)
				{
					$weekdays[] = $val->dayIndex;
				}
				if($val->dayIndex == 7)
				{
					$weekdays[]  = 0;
				}
			}
			for($i=0;$i<=6;$i++)
			{
				if(!in_array($i, $weekdays))
				{
					$weekDay[] = $i;
				}
			}
			$allWeekDays = implode(",",$weekDay);
		}
		else
		{
			$allWeekDays = '0,1,2,3,4,5,6';
		}
		
		return $allWeekDays;
	}
	
	function getProfWeekDaysWithTime($proId,$currentWeekDay,$currentTime)
	{
		global $wpdb;
		$weekDays = "SELECT *  FROM  wp_amelia_providers_to_weekdays WHERE userId ='".$proId."' AND dayIndex = '".$currentWeekDay."' AND ('".$currentTime."' BETWEEN wp_amelia_providers_to_weekdays.startTime AND wp_amelia_providers_to_weekdays.endTime)";
		$getWeekDays = $wpdb->get_results($weekDays, OBJECT);
		return $getWeekDays;
	}
	
	function getProApointmentWithTime($profId,$currentDate,$currentTime)
	{
		global $wpdb;
		$booking = "SELECT *  FROM  wp_amelia_appointments WHERE providerId ='".$profId."' AND status = 'approved' AND ('".$currentDate.' '.$currentTime."' BETWEEN bookingStart AND bookingEnd)";
		$getBooking = $wpdb->get_results($booking, OBJECT);
		return $getBooking;
	}
	
	function getSpecialDaysWithTime($profId,$currentDate,$currentTime)
	{
		global $wpdb;
		$specialDays = "SELECT wp_amelia_providers_to_specialdays_periods.*  FROM  wp_amelia_providers_to_specialdays INNER JOIN wp_amelia_providers_to_specialdays_periods ON wp_amelia_providers_to_specialdays.id = wp_amelia_providers_to_specialdays_periods.specialDayId WHERE wp_amelia_providers_to_specialdays.userId ='".$profId."' AND ('".$currentDate."' BETWEEN wp_amelia_providers_to_specialdays.startDate AND wp_amelia_providers_to_specialdays.endDate) ";
		$getSpecialDays = $wpdb->get_results($specialDays, OBJECT);
		$default[] = 0;
		$type = 'notavail';
		if(count($getSpecialDays))
		{
			$type = 'avail';
			foreach($getSpecialDays as $val)
			{
				if($val->startTime <= $currentTime && $val->endTime >= $currentTime)
				{
					$default[] = 1;
				}
			}
		}
		$response['default'] =  $default;
		$response['type'] =  $type;
		return $response;
	}
	
	function getProfDayOffNew($profId)
	{
		global $wpdb;
		$offDays = "SELECT *  FROM  wp_amelia_providers_to_daysoff WHERE userId =".$profId;
		$getProOffDays = $wpdb->get_results($offDays, OBJECT);
		
		$getdayoff = '';
		if(count($getProOffDays) > 0)
		{
			foreach($getProOffDays as $val)
			{
				$startDate = date('m/d/Y',strtotime($val->startDate));
				$endDate = date('m/d/Y',strtotime($val->endDate));
				if($startDate != $endDate)
				{
					$period = new DatePeriod(
						 new DateTime($startDate),
						 new DateInterval('P1D'),
						 new DateTime($endDate)
					);
					foreach ($period as $key => $value) {
						$dates[] = $value->format('m/d/Y');      
					}
					
					$dates[] = $endDate;
				}
				if($startDate == $endDate)
				{
					
					$dates[] = $startDate;
				}
			}
			$getdayoff[] = '"'.implode('","',$dates).'"';
		}
		return $getdayoff;
	}
	
	function getProfDayOff($profId)
	{
		global $wpdb;
		$offDays = "SELECT *  FROM  wp_amelia_providers_to_daysoff WHERE userId =".$profId;
		$getProOffDays = $wpdb->get_results($offDays, OBJECT);
		
		$getdayoff = '';
		if(count($getProOffDays) > 0)
		{
			foreach($getProOffDays as $val)
			{
				$startDate = $val->startDate;
				$endDate = $val->endDate;
				if($startDate != $endDate)
				{
					$period = new DatePeriod(
						 new DateTime($startDate),
						 new DateInterval('P1D'),
						 new DateTime($endDate)
					);
					foreach ($period as $key => $value) {
						$dates[] = $value->format('Y-m-d');      
					}
					$dates[] = $endDate;
				}
				if($startDate == $endDate)
				{
					$dates[] = $startDate;
				}
			}
			$getdayoff[] = '"'.implode('","',$dates).'"';
		}
		return $getdayoff;
	}
	add_filter( 'ajax_query_attachments_args', 'show_current_user_attachments' );

	function show_current_user_attachments( $query ) {
		$user_id = get_current_user_id();
		if ( $user_id ) {
			$query['author'] = $user_id;
		}
		return $query;
	}
	
	add_action('wp_ajax_getCatByLocation', 'getCatByLocation');
 	add_action('wp_ajax_nopriv_getCatByLocation', 'getCatByLocation');
	
	function getCatByLocation()
	{
		global $wpdb;
		// if($_POST['searchLoc'] == '')
		// {
			// $lat = $_POST['latitude'];
			// $long = $_POST['longitude'];
		// }
		// else
		// {
			// $lat = '';
			// $long = '';
		// }
		$categories = "SELECT * FROM wp_amelia_categories WHERE status='visible' ORDER BY id DESC";
		$getCategories = $wpdb->get_results($categories, OBJECT);
		$html = '';
		if(count($getCategories) > 0)
		{
			foreach($getCategories as $cat)
			{
				if(count(getCategoryServicesByLocation($cat->id,$_POST)) > 0)
				{
					if($cat->slug == $_POST['param'])
					{ 
						$sel = "checked='checked'";
					}
					else
					{
						$sel = '';
					}
	
					$html .= '<li class="checkbox-wrap">
							<label>
								<input type="checkbox" name="cat[]" class="search-category service_categories" value="'.$cat->id.'" '.$sel.'>
								<span class="new-checkbox"></span>'.$cat->name.'</label>
							<label>'.count(getCategoryServicesByLocation($cat->id,$_POST)).'</label>
						</li>';
				}
			}
		}
		
		
		echo $html;die;
	}
	
	function getCategoryServicesByLocation($catsId,$params)
	{
		global $wpdb;

		if($params['searchLat'] == '' && $params['searchLong'] == '' && $params['searchLoc'] != '')
		{
			$latitude = '';
			$longitude = '';
		}
		else if($params['searchLat'] != '' && $params['searchLong'] != '')
		{
			$latitude = $params['searchLat'];
			$longitude = $params['searchLong'];
		}
		else
		{
			$latitude = $_COOKIE['latitude'];
			$longitude = $_COOKIE['longitude'];
		}

		$param = $params['param'];
		$profId = $params['profId'];
		$serviceWhere = '';
		$proWhere = '';
		$query = '';
		$distanceHaving = '';

		if($latitude != '' && $longitude != '')
		{
			$query .= ',wp_amelia_locations.latitude, wp_amelia_locations.longitude, (3956 * 2 * ASIN(SQRT( POWER(SIN(("'.$latitude.'" - abs(latitude))*pi()/180/2),2)+COS("'.$latitude.'"*pi()/180 )*COS(abs(latitude)*pi()/180)*POWER(SIN(("'.$longitude.'"-longitude)*pi()/180/2),2)))) as distance';
			$distanceHaving .= 'having distance <= wp_amelia_users.rangeWilling';
		}

		/*-sort by category-*/
		if($params['catId'] == '')
		{
			$param = '';
		}

		if($params['catId'] != '')
		{
			$param = '';
			$params['getService'] = '';
			$catId = implode(",",$params['catId']);
			$serviceWhere .= " AND wp_amelia_categories.id IN (".$catId.")";
			$proWhere .= " AND wp_amelia_categories.id IN (".$catId.")";
		}

		/*-search by price-*/

		if($params['price'] != '')
		{
			$explodePrice = explode(",",$params['price']);
			$serviceWhere .= " AND (wp_amelia_services.price >= '".$explodePrice[0]."' AND wp_amelia_services.price <= '".$explodePrice[1]."')";
			$proWhere .=  " AND (wp_amelia_services.price >= '".$explodePrice[0]."' AND wp_amelia_services.price <= '".$explodePrice[1]."')";
		}

		/*-search by service time range-*/

		if($params['servicerange'] != '')
		{
			$explodeService = explode(",",$params['servicerange']);
			$fromService = $explodeService[0] * 3600;
			$toService = $explodeService[1] * 3600;
			$serviceWhere .= " AND (wp_amelia_services.duration >= '".$fromService."' AND wp_amelia_services.duration <= '".$toService."')";
			$proWhere .=  " AND (wp_amelia_services.duration >= '".$fromService."' AND wp_amelia_services.duration <= '".$toService."')";
		}

		/*-search service by keyword-*/

		if($params['service'] != '')
		{
			if($params['searchservice'] == 'searchService')
			{
				$serviceWhere .= " AND wp_amelia_services.name LIKE '%".$params['service']."%'";
				$proWhere .= " AND wp_amelia_services.name LIKE '%".$params['service']."%'";
			}
			else
			{
				$serviceWhere .= " AND (CONCAT( wp_amelia_users.firstName,  ' ', wp_amelia_users.lastName ) LIKE '%".$params['service']."%')";
				$proWhere .= " AND (CONCAT( wp_amelia_users.firstName,  ' ', wp_amelia_users.lastName ) LIKE '%".$params['service']."%')";
			}
		   
		}

		/*==search by date==*/

		if($params['date'] != '' && $params['selectedTime'])
		{
			$params['searchDate'] = '';
			$bookingDate = $params['date'];
			$bookingTime = explode("-",$params['selectedTime']);
			$fromTime = date('H:i:s',strtotime($bookingTime[0]));
			$toTime = date('H:i:s',strtotime($bookingTime[1]));
			$getFromTime = $bookingDate.' '.$fromTime;
			$getToTime = $bookingDate.' '.$toTime;
			if($bookingDate == 'anydate')
			{
				 $serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((TIME(`bookingStart`) >= '".$fromTime."' AND TIME(`bookingEnd`) <= '".$toTime."')))";
				 $proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((TIME(`bookingStart`) >= '".$fromTime."' AND TIME(`bookingEnd`) <= '".$toTime."')))";
			}
			else
			{
				$serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$getFromTime."' AND `bookingEnd` <= '".$getToTime."') OR (`bookingStart` <= '".$getFromTime."' AND `bookingEnd` >= '".$getToTime."')))";
				$proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$getFromTime."' AND `bookingEnd` <= '".$getToTime."') OR (`bookingStart` <= '".$getFromTime."' AND `bookingEnd` >= '".$getToTime."')))";
			}
		}

		/*====search by date time===*/

		if($params['searchDate'] != '')
		{
			$getExplodeDates = explode(" ",$params['searchDate']);
			//$getExplodedtime = explode("-",$getExplodeDates[1]);
			$bookingFromTime = $getExplodeDates[0].' '.$getExplodeDates[1].':00';
			$bookingToTime = $getExplodeDates[0].' '.$getExplodeDates[3].':00';
			$startTime = $getExplodeDates[1].':00';
			$endTime = $getExplodeDates[3].':00';
			if($getExplodeDates[0] == 'anydate')
			{
				 $serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((TIME(`bookingStart`) >= '".$startTime."' AND TIME(`bookingEnd`) <= '".$endTime."')))";
				 $proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$startTime."' AND `bookingEnd` <= '".$endTime."')))";
			}
			else
			{
				$serviceWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$bookingFromTime."' AND `bookingEnd` <= '".$bookingToTime."') OR (`bookingStart` <= '".$bookingFromTime."' AND `bookingEnd` >= '".$bookingToTime."')))";
				$proWhere .= " AND wp_amelia_services.id NOT IN (SELECT serviceId FROM wp_amelia_appointments WHERE status = 'approved' AND ((`bookingStart` >= '".$bookingFromTime."' AND `bookingEnd` <= '".$bookingToTime."') OR (`bookingStart` <= '".$bookingFromTime."' AND `bookingEnd` >= '".$bookingToTime."')))";
			}
		}

		/*-search by category and professioanl-*/

		if($param != '' && $profId != '')
		{
			$catId = implode(",",$params['catId']);
			$serviceWhere .= " AND wp_amelia_categories.id IN (".$catId.") AND wp_amelia_users.id = '".$profId."'";
			$proWhere .= " AND wp_amelia_categories.id IN (".$catId.") AND wp_amelia_users.id = '".$profId."'";
		}

		
		/*-get services with service name-*/

		if($params['getService'] != '')
		{
			$serviceWhere .= " AND wp_amelia_services.name = '".$params['getService']."'";
			$proWhere .= " AND wp_amelia_services.name = '".$params['getService']."'";
		}

		/*-get services with professional name-*/

		if($params['professionalservice'] != '')
		{
			$serviceWhere .= " AND wp_amelia_users.id = '".$params['professionalservice']."'";
			$proWhere .= " AND wp_amelia_users.id = '".$params['professionalservice']."'";
		}

		/*-sort by-*/

		if($params['sortBy'] != '')
		{
			if($params['sortBy'] == 'low')
			{
				$serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving."  ORDER BY wp_amelia_services.price ASC";
				$proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_services.price ASC";
			}
			else if($params['sortBy'] == 'high')
			{
				$serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.price DESC";
				$proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_services.price DESC";
			}
			else if($params['sortBy'] == 'popularity')
			{
				$serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
				$proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY totalRatingSum/totalRating DESC";
			}
			else if($params['sortBy'] == 'recent')
			{
				$serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.price DESC";
				$proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_users.id DESC";
			}
		}

		/*-if sort by null normal order-*/

		if($param == '' && $params['sortBy'] == '')
		{
			$serviceWhere .= " GROUP BY  wp_amelia_services.id ".$distanceHaving." ORDER BY wp_amelia_services.id DESC";
			$proWhere .= " GROUP BY  wp_amelia_users.id ".$distanceHaving." ORDER BY wp_amelia_users.id DESC";
		}

		$cat = "SELECT wp_amelia_users.rangeWilling,wp_amelia_services.*,wp_amelia_locations.address,(SELECT SUM(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRatingSum,(SELECT COUNT(wp_amelia_customer_feedback.rating) FROM wp_amelia_customer_feedback WHERE wp_amelia_customer_feedback.entityId = wp_amelia_services.id) as totalRating, wp_amelia_users.firstName , wp_amelia_users.lastName, wp_amelia_users.firstName , wp_amelia_users.lastName ".$query." FROM wp_amelia_categories INNER JOIN wp_amelia_services ON wp_amelia_categories.id = wp_amelia_services.categoryId LEFT JOIN wp_amelia_customer_feedback ON wp_amelia_services.id = wp_amelia_customer_feedback.entityId LEFT JOIN wp_amelia_providers_to_services ON wp_amelia_services.id = wp_amelia_providers_to_services.serviceId INNER JOIN wp_amelia_users ON wp_amelia_providers_to_services.userId = wp_amelia_users.id INNER JOIN wp_amelia_providers_to_locations ON wp_amelia_users.id = wp_amelia_providers_to_locations.userId INNER JOIN wp_amelia_locations ON wp_amelia_providers_to_locations.locationId = wp_amelia_locations.id INNER JOIN wp_amelia_providers_to_weekdays ON wp_amelia_users.id = wp_amelia_providers_to_weekdays.userId WHERE wp_amelia_categories.status = 'visible' AND wp_amelia_services.status = 'visible' AND wp_amelia_categories.id = '".$catsId."' AND wp_amelia_locations.status = 'visible' ".$serviceWhere."";
		$getServices = $wpdb->get_results($cat, OBJECT);
		
		return $getServices;
	}
	
	function wpml_shortcode_lang() {
 
    $languages = icl_get_languages('skip_missing=0');
 
    if( 1 < count( $languages ) ) {
        $s = "";
        $s = '<ul>';
        foreach( (array)$languages as $language ) {
			$s .= '<li><a id="language_change" data-attr="'.$language['code'].'" href="' . $language['url'] . '"><img src="' . $language['country_flag_url'] . '" alt="" /> ' . $language['native_name'] . '</a></li> ';
        }
        $s .= '</ul>';
    }
 
    return $s;
}
add_shortcode( 'wpml_translate', 'wpml_shortcode_lang' );
add_filter( 'if_menu_conditions', 'wpb_new_menu_conditions' );
 
function wpb_new_menu_conditions( $conditions ) {
	
  $conditions[] = array(
    'name'    =>  'If it is Custom Post Type archive', // name of the condition
    'condition' =>  function($item) {          // callback - must return TRUE or FALSE
      return is_post_type_archive();
    }
  );
 
  return $conditions;
  }


// Limit media library access
 function my_files_only( $wp_query ) {
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/upload.php' ) !== false ) {
        if ( !current_user_can('administrator') ) {
            global $current_user;
            $wp_query->set( 'author', $current_user->id );
        }
    }
}
 
add_filter('parse_query', 'my_files_only' );

