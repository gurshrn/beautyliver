    <section class="banner-wrapper">
        <div id="demo" class="carousel slide" data-ride="carousel">
            <?php 

                $images = get_field('banner_images','options');
                $size = 'full'; 
            ?>
            <ul class="carousel-indicators">
                <?php 

                    if( $images ): 
                        $i=0;
                        foreach( $images as $image ):
                ?>
                            <li data-target="#demo" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo 'active';}?>"></li>

                <?php $i++; endforeach; endif; ?>

            </ul>
            <div class="carousel-inner">
                <?php 

                    if( $images ): 
                        $i=0;
                        foreach( $images as $image ):
                ?>
                            <div class="carousel-item <?php if($i == 0){ echo 'active';}?>" style="background-image: url('<?php echo $image['url']; ?>')"></div>

                <?php $i++; endforeach; endif;?>
                       
            </div>

            <!-- carousel-inner -->

            <div class="banner-caption">
                <div class="container">
                    <div class="banner-form-wrap">
                        <h2><?php echo the_field('search_block_title'); ?></h2>
                        <form>
							
                            <div class="form-group">
                                <input type="hidden" class="form-control search-service-name">
                                <input type="text" class="form-control search-service" id="service" placeholder="<?php echo the_field('service_placeholder'); ?>">
                                <div id="search_service_list"></div>
                            </div>
                            <div class="form-group relative">
                                <input type="text" class="form-control" id="location-test" placeholder="<?php echo the_field('location_placeholder'); ?>" value="<?php echo $_COOKIE['address'];?>">
								
									<?php 
										if(isset($_COOKIE['current_location']) && isset($_COOKIE['user_id']))
										{
											if($_COOKIE['current_location'] == 'yes')
											{
												echo '<button type="button" class="getMyLocation" data-toggle="tooltip" title="'.get_field('my_location_button_text').'">';
											}
											else
											{
												echo '<button type="button" class="getCurrentLocation" data-toggle="tooltip" title="'.get_field('current_location_button_text').'">';
											}
											echo '<i class="fa fa-compass" aria-hidden="true"></i>';
										}
										else
										{
											echo '<button type="button" class="getCurrentLocation" data-toggle="tooltip" title="'.get_field('current_location_button_text').'">';
											echo '<i class="fa fa-compass" aria-hidden="true"></i>';
										}
									
									?>
								</button>
                            </div>
                            <div class="form-group select-search-date-wrap">
                                <input type="text" class="form-control select-search-date" placeholder="<?php echo the_field('time_placeholder'); ?>">
                                <div class="choose-wrap choose-datepicker" style="display:none">
								
									<?php 
										$datetime = new DateTime('tomorrow');
									?>
									<input type="hidden" class="dateType" value="anydate">
									<input type="hidden" class="timeType" value="00:00 - 23:00">
									
                                    <label>Choose Date</label>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <a href="javascript:void(0)" class="active any_date" data-target="anydate">Any Date</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <a href="javascript:void(0)" class="today_date" data-target="today" data-attr="<?php echo date('Y-m-d'); ?>">Today</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <a href="javascript:void(0)" class="tommorrow_date" data-target="tomo" data-attr="<?php echo $datetime->format('Y-m-d');?>">Tomorrow</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <a href="javascript:void(0)" class="choose_date">Choose date..</a>
												<div id="choose-datepicker" style="display:none;"></div>
                                            </div>
										</div>
                                    </div>
                                    <!-- row -->
                                    <label>Choose Time</label>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <a href="javascript:void(0)" class="any_time" data-target="00:00 - 23:00" class="any_time">Any Time</a>
                                            </div>
                                        </div>
										
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <a href="javascript:void(0)" class="now_time choose-time">Now</a>
                                            </div>
                                        </div>
										<?php 
											$open_time = strtotime("00:00");
											$close_time = strtotime("23:00");
											$now = time();
											$output = "";
											
										?>
                                        <div class="col-lg-6 col-md-6 col-sm-6 chose_time">
                                            <label>From</label>
                                            <div class="form-group">
                                                <select class="time_from_times">
                                                    <?php 
														for( $i=$open_time; $i<=$close_time; $i+=3600) 
														{
															
													?>
														<option value="<?php echo date("H:i",$i);?>"><?php echo date("H:i",$i); ?></option>
															
													<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 chose_time">
                                            <label>To</label>
                                            <div class="form-group">
                                                <select class="time_to_times">
                                                    <?php 
														for( $i=$open_time; $i<=$close_time; $i+=3600) 
														{
													?>
														<option value="<?php echo date("H:i",$i);?>"><?php echo date("H:i",$i); ?></option>
															
													<?php } ?>
                                                </select>
                                            </div>
                                        </div>
										
                                    </div>
                                    <!-- row -->
                                    <div class="button-wrap">
                                        <button type="button" class="home-date-time-picker">Done</button>
                                    </div>
                                </div>
                                <!-- choose-wrap -->
                            </div>
							
                            <a href="<?php echo get_site_url().'/service-list';?>" class="service-search-btn">
							
								<input type="hidden" class="searchSelectedDate">
								<input type="hidden" class="searchSelectedTime">
								<input type="hidden" class="to_time">
							
                                <button type="button" class="btn button"><?php echo the_field('search_button_text'); ?></button>
                            </a>
                        </form>
                    </div>
                    <div class="banner-content">
                        <h1>Beauty Services <br> At Your Finger Tip</h1>
						<?php if($_SESSION['user_id'] == ''){ ?>
							<a class="signup theme-btn" href="javascript:void(0)" data-attr="customer" id="sign_up_btn">Sign Up</a>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>