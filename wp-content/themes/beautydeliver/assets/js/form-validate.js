jQuery(document).ready(function(){

	
	jQuery(document).on('click','.login_sign_up_close',function(){
		var type = jQuery(this).attr('data-attr');
		if(type == 'login_close')
		{
			jQuery('#login_popup').css('display','none');
		}
		else
		{
			jQuery('#professional_modal').css('display','none');
		}
	});

	jQuery(document).on('click','#sign_up_btn',function(){
		var type = jQuery(this).attr('data-attr');
		jQuery('#amelia_sign_up').trigger('reset');
		var validator = $( "#amelia_sign_up" ).validate();
		validator.resetForm();
		if(type == 'customer')
		{
			jQuery('.user_role').val('customer');
			jQuery('#sign_up_type').html('Sign up - Customer');
			jQuery('#or_apply_as').html('or apply as<a href="javascript:void(0)" data-attr="professional" id="sign_up_btn">Professional</a>.');
		}
		if(type == 'professional')
		{
			jQuery('.user_role').val('provider');
			jQuery('#sign_up_type').html('Sign up - Professional');
			jQuery('#or_apply_as').html('or apply as<a href="javascript:void(0)" data-attr="customer" id="sign_up_btn">Customer</a>.');
		}
		jQuery('#professional_modal').css('display','block');
		jQuery('#login_popup').css('display','none');
	});

	jQuery(document).on('click','#login_modal',function(){
		jQuery('#amelia_login').trigger('reset');
		jQuery( "#amelia_login" ).validate().resetForm();
		jQuery('#professional_modal').css('display','none');
		jQuery('#login_popup').css('display','block');
	});

/*==========Customer Dahboard Appointment jquery===========*/

 	/*=====Show stars for rating=======*/

 	jQuery('#star1').starrr({
        change: function(e, value){
            jQuery('.rating').val(value);
        }
    });

    /*=======Cancel appointment by customer=========*/

    jQuery(document).on('click','.cancel-appoint',function(){
        var appointmentId = jQuery(this).attr('data-attr');
        jQuery('.appointId').val(appointmentId);
        jQuery('#cancelModal').modal('show');
    });

    /*======rating to the service by customer======*/

    jQuery(document).on('click','.customer_review',function(){
        var appointmentId = jQuery(this).attr('data-attr');
        var serviceId = jQuery(this).attr('data-target');
        jQuery('.appointmentId').val(appointmentId);
        jQuery('.serviceId').val(serviceId);
		jQuery.ajax({
			url: site_url+'/wp-admin/admin-ajax.php',
            type: "POST",
            data: {action:'get_service_detail_for_review',serviceId:serviceId},
			dataType: "html",
			success: function(response) {
				jQuery('.review_service_detail').html(response);
				jQuery('#reviewModal').modal('show');
			}
		});
        
    });


/*===========form validations========*/

	/*======================sign up===================*/

	jQuery("#amelia_sign_up").validate({
		rules:
		{
			firstName: 
			{
				required: true,
			},
			lastName: 
			{
				required: true,
			},
			email: 
			{
				required: true,
			},
			address: 
			{
				required: true,
			},
			phone_number: 
			{
				required: true,
			},
			phone_pin: 
			{
				required: true,
			},
		},
		messages: 
		{
		
		},

		submitHandler: function(form) 
		{
			var email = jQuery('#user_email').val();
			var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		    var valid = emailReg.test(email);
			if (!valid && email != '') 
			{
				toastr.error('Invalid email', 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			} 
		    else
		    {
				var fd = new FormData(form);
				fd.append( "action", 'sign_up');
				jQuery('#amelia_sign_up').find('button[type=submit]').attr('disabled',true);
				jQuery('#amelia_sign_up').find('button[type=submit]').html('Processing...');

				jQuery.ajax({
					url: site_url+'/wp-admin/admin-ajax.php',
					type: form.method,
					data: fd,
					contentType: false,       
					cache: false,             
					processData:false, 
					dataType: "json",
					success: function(response) {
						jQuery('#amelia_sign_up').find('button[type=submit]').removeAttr('disabled');
						jQuery('#amelia_sign_up').find('button[type=submit]').html('Sign Up');
						if(response.success == true)
						{
							toastr.success(response.msg, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
							window.setTimeout(function() {
							    window.location.href = response.url;
							}, 2000);
						}
						if(response.success == false)
						{

							toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						}
					}
				});
			}
		}
	});

	/*======================user login===================*/

	jQuery("#amelia_login").validate({
		rules:
		{
			amelia_phone_number: 
			{
				required: true,
			},
			phone_pin: 
			{
				required: true,
			},

		},
		messages: 
		{
		
		},
		submitHandler: function(form) 
		{
			var fd = new FormData(form);
			fd.append( "action", 'amelia_login');
			jQuery('#amelia_login').find('button[type=submit]').attr('disabled',true);
			jQuery('#amelia_login').find('button[type=submit]').html('Processing...');
			jQuery.ajax({

				url: site_url+'/wp-admin/admin-ajax.php',
				type: form.method,
				data: fd,
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				success: function(response) 
				{
					jQuery('#amelia_login').find('button[type=submit]').removeAttr('disabled');
					jQuery('#amelia_login').find('button[type=submit]').html('Login');
					if(response.success == true)
					{
						toastr.success(response.msg, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						window.setTimeout(function() {
						    window.location.href = response.url;
						}, 2000);
					}
					else
					{

						toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
				}
			});
		}
	});

	

	/*======================Confirm Location===================*/

	jQuery("#confirm_location").validate({
		rules:
		{
			name: 
			{
				required: true,
			},
		},
		messages: 
		{
		
		},
		submitHandler: function(form) 
		{
			var fd = new FormData(form);
			fd.append( "action", 'add_customer_address');
			jQuery('#confirm_location').find('input[type=submit]').attr('disabled',true);
			jQuery('#confirm_location').find('input[type=submit]').val('Processing...');
			jQuery.ajax({

				url: site_url+'/wp-admin/admin-ajax.php',
				type: form.method,
				data: fd,
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				success: function(response) 
				{
					jQuery('#confirm_location').find('input[type=submit]').removeAttr('disabled');
					jQuery('#confirm_location').find('input[type=submit]').val('Next');
					if(response.success == true)
					{
						jQuery('#confirm_location').find('input[type=submit]').removeAttr('disabled');
						jQuery('#confirm_location').find('input[type=submit]').val('Next');
						jQuery('.confirm-order-location').css('display','none');
						jQuery('.confirm-order-detail').css('display','block');
						jQuery('.third-li').addClass('active');
						if(response.data.additional_address == '')
						{
							jQuery('.additional_address_li').css('display','none');
						}
						else
						{
							jQuery('.additional_address_li').css('display','block');
						}
						jQuery('.address_cust_name').html(response.data.name);
						jQuery('.address_cust_addrress').html(response.data.customer_address);
						jQuery('.address_cust_additional').html(response.data.additional_address);
					}
					else
					{
						toastr.error(response.msg)
					}
					
				}
			});
		}
	});
	
	/*======================Order Now===================*/

	jQuery("#order_now_confirm").validate({
		submitHandler: function(form) 
		{
			var fd = new FormData(form);
			fd.append( "action", 'confirm_order_now');
			jQuery('.order_now_confirm').attr('disabled',true);
			jQuery('.order_now_confirm').val('Processing...');
			jQuery.ajax({

				url: site_url+'/wp-admin/admin-ajax.php',
				type: form.method,
				data: fd,
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				success: function(response) 
				{
					
					if(response.success == true)
					{
						toastr.success(response.msg, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						window.setTimeout(function() {
							window.location.href = response.url;
						}, 2000);
					}
					else
					{
						jQuery('.order_now_confirm').removeAttr('disabled');
						jQuery('.order_now_confirm').val('Order Now');
						toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
				}
			});
		}
	});
	
	

	/*======================Dashboard Profile===================*/
	jQuery("#dashboard_profile").validate({
		rules:
		{
			firstName: 
			{
				required: true,
			},
			lastName:
			{
				required: true,
			},
			phone:
			{
				required: true,
			},
			address:
			{
				required:true,
			},
		},
		messages: 
		{
		
		},
		submitHandler: function(form) 
		{
			if(jQuery('.updateLocation').val() == '')
			{
				toastr.error('Kindly select valid location');
				return false;
			}
			else
			{
				var fd = new FormData(form);
				fd.append( "action", 'update_dashboard_profile');
				jQuery('.dashboard-profile').attr('disabled',true);
				jQuery('.dashboard-profile').html('Processing...');
				jQuery.ajax({

					url: site_url+'/wp-admin/admin-ajax.php',
					type: form.method,
					data: fd,
					contentType: false,       
					cache: false,             
					processData:false, 
					dataType: "json",
					success: function(response) 
					{
						jQuery('.dashboard-profile').removeAttr('disabled');
						jQuery('.dashboard-profile').html('Save');
						if(response.success == true)
						{
							if(response.confirmed)
							{
								jQuery('.confirm_mobile_number').val(response.confirmed);
								jQuery('.update_pin').val('');
							}
							toastr.success(response.msg, 'Success', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						}
						else
						{

							toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						}
					}
				});
			}
		}
	});
	
	/*======================Cancel Appointment===================*/

	jQuery("#cancel_apppointment").validate({
		
		submitHandler: function(form) 
		{
			var fd = new FormData(form);
			fd.append( "action", 'cancel_appointment');
			jQuery('#cancel_appointment').find('button[type=submit]').attr('disabled',true);
			jQuery('#cancel_appointment').find('button[type=submit]').html('Processing...');
			jQuery.ajax({

				url: site_url+'/wp-admin/admin-ajax.php',
				type: form.method,
				data: fd,
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				success: function(response) 
				{
					jQuery('#cancel_appointment').find('button[type=submit]').removeAttr('disabled');
					jQuery('#cancel_appointment').find('button[type=submit]').html('Yes');
					if(response.success == true)
					{
						toastr.success(response.msg, 'Success', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						window.setTimeout(function() {
						    window.location.href = response.url;
						}, 2000);
					}
					else
					{

						toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
				}
			});
		}
	});

	/*===============Rating Service=========*/

	jQuery("#service_review").validate({
		rules:
		{
			comment: 
			{
				required: true,
			},
		},
		messages: 
		{
		
		},
		submitHandler: function(form) 
		{
			if(jQuery('.rating').val() == '')
			{
				toastr.error('Rating required');
			}
			else
			{
				var fd = new FormData(form);
				fd.append( "action", 'service_rating');
				jQuery('#service_review').find('button[type=submit]').attr('disabled',true);
				jQuery('#service_review').find('button[type=submit]').html('Processing...');
				jQuery.ajax({

					url: site_url+'/wp-admin/admin-ajax.php',
					type: form.method,
					data: fd,
					contentType: false,       
					cache: false,             
					processData:false, 
					dataType: "json",
					success: function(response) 
					{
						jQuery('#dashboard_profile').find('button[type=submit]').removeAttr('disabled');
						jQuery('#dashboard_profile').find('button[type=submit]').html('Submit');
						if(response.success == true)
						{
							toastr.success(response.msg, 'Success', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
							window.setTimeout(function() {
							   location.reload();
							}, 2000);
						}
						else
						{

							toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						}
					}
				});
			}
		}
	});
	
	/*===============Profile phone pin=========*/

	jQuery("#send_profile_phone_pin").validate({
		rules:
		{
			phone_number: 
			{
				required: true,
			},
		},
		messages: 
		{
		
		},
		submitHandler: function(form) 
		{
			var fd = new FormData(form);
			fd.append( "action", 'update_phone_number');
			jQuery('#service_review').find('button[type=submit]').attr('disabled',true);
			jQuery('#service_review').find('button[type=submit]').html('Processing...');
			jQuery.ajax({

				url: site_url+'/wp-admin/admin-ajax.php',
				type: form.method,
				data: fd,
				contentType: false,       
				cache: false,             
				processData:false, 
				dataType: "json",
				success: function(response) 
				{
					jQuery('#dashboard_profile').find('button[type=submit]').removeAttr('disabled');
					jQuery('#dashboard_profile').find('button[type=submit]').html('Submit');
					if(response.success == true)
					{
						toastr.success(response.msg, 'Success', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						window.setTimeout(function() {
							window.location.href = response.url;
						}, 2000);
					}
					else
					{

						toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
				}
			});
		}
	});

	
	

});





