/*========================trim space validation=======================*/

	jQuery('body').on('keyup blur', 'input[type = "number"],input[type = "text"],input[type = "email"],input[type = "password"]', function (eve) 
	{
		if ((eve.which != 37) && (eve.which != 38) && (eve.which != 39) && (eve.which != 40)) 
		{
			var text = jQuery(this).val();
			text = text.trim();
			if (text == '') {
				jQuery(this).val('');
			}
			var string = jQuery(this).val();
			if (string != "") {
				string = string.replace(/\s+/g, " ");
				jQuery(this).val(string);
			}
		}
	});
	
/*====================only Alphabet validation====================*/

	jQuery(document).on('keypress', '.alphabets', function (event) 
	{
		var inputValue = event.charCode;
		if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) 
		{
			event.preventDefault();
		}
	});
	
/*================only Numeric validation with tab working====================*/

	jQuery(document).on('keypress', '.onlyvalidNumber', function (eve) 
	{
		if (eve.which == 0) 
		{
			return true;
		}
		else
		{
			if ((eve.which != 46 || $(this).val().indexOf('.') == -1) && (eve.which < 48 || eve.which > 57))
			{
				if (eve.which != 8)
				{
					eve.preventDefault();
				}
			}
		}
	});

	
/*============Send otp when update profile============*/

    jQuery(document).on('click','.update_phone_number',function(){
        jQuery('.update_pin').val('yes');
	});
	
	jQuery(document).on('click','.confirm_new_phone_number',function(){
		jQuery('.update_pin').val('confirm');
	});
	
	jQuery(document).on('click','.dashboard-profile',function(){
		jQuery('.update_pin').val('');
	});
	
/*============Send otp for login and signup============*/

    jQuery(document).on('click','.send_pin',function(){
        var phone_number = jQuery(this).parent().find('.amelia_phone_number ').val();
        var type = jQuery(this).attr('data-attr');
        if(phone_number == '')
        {
            toastr.error('Kindly enter the phone number', 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
        }
        if(phone_number != '')
        {
            if(type == 'amelia_sign_up')
            {
                ameliaAction = "send_phone_pin";
            }
			else if(type == 'amelia_profile')
			{
				ameliaAction = "send_profile_pin";
			}
            else
            {
                ameliaAction = "send_login_phone_pin";
            }
			jQuery('.send_pin').attr('disabled',true);
            jQuery('.send_pin').html('Processing...');
            jQuery.ajax({
                url: site_url+'/wp-admin/admin-ajax.php',
                type: "POST",
                data: {action:ameliaAction,phone_number:phone_number,send_pin_type:ameliaAction},
                dataType: "json",
                success: function(response) {
                    jQuery('.send_pin').removeAttr('disabled');
                    jQuery('.send_pin').html('SEND PIN');
                    jQuery('#amelia_phone_number').attr('disabled',true);
					if(response.success == true)
					{
						toastr.success(response.msg, 'Success!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
					else
					{
						toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					}
                    
                }
            });
        }
        
    });

/*==========Start Seductive search for service,category and professional for textfield in home page=========*/
	
	jQuery('body').click(function(){
		jQuery('#search_service_list').css('display','none');
		if(jQuery('.search-service-name').val() == '')
		{
			jQuery('.search-service').val('');
		}
	});
	
	/*===========On keyup service textfield Seductive search in home page==========*/

    jQuery(document).on('keyup','.search-service',function(){
        var keyword = jQuery(this).val();
		if(keyword != '')
		{
			jQuery.ajax({
				url: site_url+'/search-service',
				type: "POST",
				data: {keyword:keyword,type:''},
				dataType: "html",
				success: function(response) {
					if(keyword.length == 1)
					{
						jQuery('#search_service_list').css('display','none');
					}
					else
					{
						jQuery('#search_service_list').css('display','block');
						jQuery('#search_service_list').html(response);
					}
					
				}
			});
		}
		else
		{
			jQuery('#search_service_list').css('display','none');
		}
        
    });
	
	/*=======On click service name after search service in home page=========*/

    jQuery(document).on('click','.get-service-name',function(){
        var keyword = jQuery(this).attr('data-attr');
        var type = jQuery(this).attr('data-target');
        jQuery('.search-service').val(keyword);
        jQuery('.search-service-name').val(keyword);
        jQuery('#search_service_list').css('display','none');
		if(type == 'service')
		{
			if(jQuery('.searchSelectedDate').val() != '' && jQuery('.searchSelectedTime').val() != '')
			{
				jQuery('.service-search-btn').attr('href',site_url+'/service-list?service='+keyword+'&date='+jQuery('.searchSelectedDate').val()+'&time='+jQuery('.searchSelectedTime').val());
			}
			else
			{
				jQuery('.service-search-btn').attr('href',site_url+'/service-list?service='+keyword);
			}
			
		}
		else if(type == 'category')
		{
			if(jQuery('.searchSelectedDate').val() != '' && jQuery('.searchSelectedTime').val() != '')
			{
				jQuery('.service-search-btn').attr('href',site_url+'/service-list?param='+keyword.toLowerCase()+'&date='+jQuery('.searchSelectedDate').val()+'&time='+jQuery('.searchSelectedTime').val());
			}
			else
			{
				jQuery('.service-search-btn').attr('href',site_url+'/service-list?param='+keyword.toLowerCase());
			}
		}
		else
		{
			if(jQuery('.searchSelectedDate').val() != '' && jQuery('.searchSelectedTime').val() != '')
			{
				jQuery('.service-search-btn').attr('href',site_url+'/service-list?professionalservice='+jQuery(this).attr('data-value')+'&date='+jQuery('.searchSelectedDate').val()+'&time='+jQuery('.searchSelectedTime').val());
			}
			else
			{
				jQuery('.service-search-btn').attr('href',site_url+'/service-list?professionalservice='+jQuery(this).attr('data-value'));
			}
		}
        
    });
/*==========End Seductive search for service,category and professional for textfield in home page=========*/

/*=====Start search with choose date time in search list page and home page==========*/

	/*========On click Time textbox show choose datepicker div for home page==========*/
	
	jQuery(document).on('click','.select-search-date',function(){
		jQuery('.choose-datepicker').css('display','block');
		
		jQuery('#choose-datepicker').css('display','none');
		
		jQuery('.any_date').addClass('active');
		jQuery('.today_date').removeClass('active');
		jQuery('.tommorrow_date').removeClass('active');
		jQuery('.choose_date').removeClass('choose-date');
		jQuery('.dateType').val('anydate');
		
		jQuery('.any_time').removeClass('active');
		jQuery('.now_time').addClass('choose-time');
		var dt = new Date();
		var fromtime = dt.getHours()+parseInt(1) + ":00";
		var totime = dt.getHours()+parseInt(3) + ":00";
		jQuery('.time_from_times option[value="'+fromtime+'"]').prop('selected','selected');		
		jQuery('.time_to_times option[value="'+totime+'"]').prop('selected','selected');	
		jQuery('.timeType').val(fromtime+' - '+totime);
	});
	
	/*========On click choose date and Time textbox show choose datepicker div for search list page==========*/
	
	jQuery(document).on('click','.home-date-time-picker',function(){
		jQuery('.choose-datepicker').css('display','none');
		jQuery('#chooseDate').modal('hide');
		selectedDatetime();
	});
	
	/*==========On click any date button set any date============*/
	
	jQuery(document).on('click','.any_date',function(){
		jQuery('.today_date').removeClass('active');
		jQuery('.tommorrow_date').removeClass('active');
        jQuery(this).addClass('active');
		jQuery('.choose_date').removeClass('choose-date');
		var type = jQuery(this).attr('data-target');
		jQuery('.dateType').val(type);
		jQuery('.searchSelectedDate').val(type);
		jQuery('#choose-datepicker').css('display','none');
		selectedDatetime();
	});
	
	/*============On click today button set today date==========*/

	jQuery(document).on('click','.today_date',function(){
		jQuery('.any_date').removeClass('active');
		jQuery('.tommorrow_date').removeClass('active');
        jQuery(this).addClass('active');
		jQuery('.choose_date').removeClass('choose-date');
		var type = jQuery(this).attr('data-target');
		jQuery('.dateType').val(type);
		var todayDate = jQuery(this).attr('data-attr');
		jQuery('.searchSelectedDate').val(todayDate);
		jQuery('#choose-datepicker').css('display','none');
		selectedDatetime();
	});
	
	/*============On click tomorrow button set tomorrow date==========*/

	jQuery(document).on('click','.tommorrow_date',function(){
		jQuery('.any_date').removeClass('active');
		jQuery('.today_date').removeClass('active');
        jQuery(this).addClass('active');
		jQuery('.choose_date').removeClass('choose-date');
		var type = jQuery(this).attr('data-target');
		jQuery('.dateType').val(type);
		var tomDate = jQuery(this).attr('data-attr');
		jQuery('.searchSelectedDate').val(tomDate);
		jQuery('#choose-datepicker').css('display','none');
		selectedDatetime();
	});

	/*==========On click Choose date button open datepicker============*/
	
	jQuery(document).on('click','.choose_date',function(){
		jQuery('.any_date').removeClass('active');
		jQuery('.today_date').removeClass('active');
		jQuery('.tommorrow_date').removeClass('active');
        jQuery('#choose-datepicker').css('display','block');
		jQuery('.choose_date').addClass('choose-date');
		jQuery('.dateType').val('choosedate');
		var date = new Date();
		
		month = ("0" + (date.getMonth() + 1)).slice(-2),
		day = ("0" + date.getDate()).slice(-2);
		year = date.getFullYear();
		selectedDate = year+'-'+month+'-'+day;
		jQuery('.searchSelectedDate').val(selectedDate);
		selectedDatetime();
	});
	
	/*==========Set datepicker for choose date button============*/
	
	jQuery('#choose-datepicker').datetimepicker({
		inline: true,
		sideBySide: true,
		format: 'YYYY-MM-DD',
		minDate: new Date(),
	});
	
	/*========On change choose datepicker set date in textbox=========*/

	jQuery("#choose-datepicker").on("dp.change", function(e) {
		jQuery('.dateType').val('choosedate');
		if(jQuery('.dateType').val() != '')
		{
			var date = new Date(e.date);
			month = ("0" + (date.getMonth() + 1)).slice(-2),
			day = ("0" + date.getDate()).slice(-2);
			year = date.getFullYear();
			selectedDate = year+'-'+month+'-'+day;
			jQuery('.searchSelectedDate').val(selectedDate);
			selectedDatetime();
		}
	});
	
	/*===========On click Any time button set time 00:00 to 23:00=========*/

	jQuery(document).on('click','.any_time',function(){
		jQuery(this).addClass('active');
		jQuery('.now_time').removeClass('choose-time');
		var type = jQuery(this).attr('data-target');
		jQuery('.time-picker').css('display','none');
		
		jQuery('.searchSelectedTime').val(type);
		jQuery('.timeType').val(type);
		
		jQuery('.time_from_times option').removeAttr('selected','selected');
		jQuery('.time_to_times option').removeAttr('selected','selected');
		
		jQuery('.time_from_times option[value="00:00"]').prop('selected','selected');		
		jQuery('.time_to_times option[value="23:00"]').prop('selected','selected');	
		
		selectedDatetime();
	});
	
	/*===========On click Now button set current time=========*/

	jQuery(document).on('click','.now_time',function(){
		jQuery('.any_time').removeClass('active');
		jQuery(this).addClass('choose-time');
		jQuery('.chose_time').css('display','block');
		
		var dt = new Date();
		var fromtime = dt.getHours()+parseInt(1) + ":00";
		var totime = dt.getHours()+parseInt(3) + ":00";
		
		jQuery('.time_from_times option').removeAttr('selected','selected');
		jQuery('.time_to_times option').removeAttr('selected','selected');
		
		jQuery('.time_from_times option[value="'+fromtime+'"]').prop('selected','selected');		
		jQuery('.time_to_times option[value="'+totime+'"]').prop('selected','selected');		
		
		var time_from_times = jQuery('.time_from_times').val();
		var time_to_times = jQuery('.time_to_times').val();
		
		jQuery('.timeType').val(time_from_times+' - '+time_to_times);
		jQuery('.searchSelectedTime').val(time_from_times+' - '+time_to_times);
		selectedDatetime();
	});
	
	/*============On Change To Time set from time and to time===========*/
	
	jQuery(document).on('change','.time_to_times',function()
	{
		var from_time = jQuery('.time_from_times').val();
		var to_time = jQuery(this).val();
		var time = to_time.split(':');
		if(to_time == '00:00')
		{
			var fromtime = '22';
		}
		else if(to_time == '01:00')
		{
			var fromtime = '23';
		}
		else
		{
			var fromtime = parseInt(time[0])-parseInt(2);
		}
		console.log(fromtime);
		if(fromtime < parseInt(10))
		{
			
			var selectedTime = '0'+fromtime+':00';
		}
		else
		{
			var selectedTime =fromtime+':00';
		}
		jQuery('.time_from_times option').removeAttr('selected','selected');
		jQuery('.time_to_times option').removeAttr('selected','selected');
		
		jQuery('.time_from_times option[value="'+selectedTime+'"]').prop('selected','selected');
		jQuery('.time_to_times option[value="'+to_time+'"]').prop('selected','selected');
		
		jQuery('.searchSelectedTime').val(selectedTime+' - '+to_time);
		jQuery('.timeType').val(selectedTime+' - '+to_time);
		
		selectedDatetime();
	});
	
	/*============On Change From Time set from time and to time===========*/
	
	jQuery(document).on('change','.time_from_times',function()
	{
		var from_time = jQuery(this).val();
		var to_time = jQuery('.time_to_times').val();
		var time = from_time.split(':');
		var totime = parseInt(time[0])+parseInt(2);
		if(totime < parseInt(10))
		{
			
			var selectedTime = '0'+totime+':00';
		}
		else
		{
			if(from_time == '23:00')
			{
				var selectedTime ='01:00';
			}
			else if(from_time == '22:00')
			{
				var selectedTime ='00:00';
			}
			else
			{
				var selectedTime =totime+':00';
			}
		}
		jQuery('.time_from_times option').removeAttr('selected','selected');
		jQuery('.time_to_times option').removeAttr('selected','selected');
		
		jQuery('.time_from_times option[value="'+from_time+'"]').prop('selected','selected');
		if(from_time == '23:00')
		{
			jQuery('.time_to_times option[value="01:00"]').prop('selected','selected');
		}
		else if(from_time == '22:00')
		{
			jQuery('.time_to_times option[value="00:00"]').prop('selected','selected');
		}
		else
		{
			jQuery('.time_to_times option[value="'+selectedTime+'"]').prop('selected','selected');
		}
		
		jQuery('.searchSelectedTime').val(from_time+' - '+selectedTime);
		jQuery('.timeType').val(from_time+' - '+selectedTime);
		
		selectedDatetime();
	});
	
/*=====End search with choose date time in search list page and home page==========*/
	
/*============Search services in service list page============*/
    
    jQuery("#search-datepicker").on("dp.change", function(e) {
        var param = jQuery('.urlParm').val();
        var selectedDate = "";
        if(jQuery('.getDate').val().length!=0 && first_time==0){
            var date = new Date(jQuery('.getDate').val());
            month = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
            year = date.getFullYear();
            selectedDate = year+'-'+month+'-'+day;
            jQuery('.searchSelectedDate').val(selectedDate);
        }
        else if(first_time==1)
        {
            var date = new Date(e.date);
            month = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
            year = date.getFullYear();
            selectedDate = year+'-'+month+'-'+day;
            jQuery('.searchSelectedDate').val(selectedDate);
        
        }
        first_time=1;
        var fromTime = jQuery('.from_time').val();
		var toTime = jQuery('.to_time').val();
		var keyword = jQuery('.search-category-service').val();
		var type = jQuery('.search_service').val();
        searchCategory(selectedDate,param,fromTime,toTime,keyword,type);
    });
	
	jQuery(document).on('change','.search-change-location',function(){
		if(jQuery(this).val() == '')
		{
			jQuery('.searchLat').val('');
			jQuery('.searchLong').val('');
		}
	});
	

	
	function selectedDatetime()
	{
		var time = jQuery('.timeType').val();
		var datetype = jQuery('.dateType').val();
		var param = jQuery('.urlParm').val();
		jQuery('.searchSelectedTime').val(time);
		if(datetype != 'anydate')
		{
			var selectedDate = jQuery('.searchSelectedDate').val();
			var date = new Date(selectedDate);
			month = ("0" + (date.getMonth() + 1)).slice(-2),
			day = ("0" + date.getDate()).slice(-2);
			year = date.getFullYear();
			getSelectedDate = day+'/'+month+'/'+year;
			jQuery('.select-search-date').val(getSelectedDate+' '+time);
		}
		else
		{
			jQuery('.searchSelectedDate').val(datetype);
			jQuery('.select-search-date').val(time);
		}
			
		if(jQuery('.search-service').val() == '')
		{
			jQuery('.service-search-btn').attr('href',site_url+'/service-list?date='+jQuery('.searchSelectedDate').val()+'&time='+time);
		}
		else if(jQuery('.search-service').val() != '')
		{
			var service = jQuery('.search-service').val();
			jQuery('.service-search-btn').attr('href',site_url+'/service-list?service='+service+'&date='+jQuery('.searchSelectedDate').val()+'&time='+time);
		}
		jQuery.cookie("searchDate",jQuery('.searchSelectedDate').val()+' '+jQuery('.searchSelectedTime').val(),{ path: '/' })
	}
	
	jQuery(document).on('click','.date-time-picker',function(){
		var date = jQuery('.dateType').val();
		var time = jQuery('.timeType').val();
		if(date == '' && time == '')
		{
			toastr.error('Kindly select date and time both');
		}
		else if(date == '' && time != '')
		{
			toastr.error('Kindly select date also');
		}
		else if(date != '' && time == '')
		{
			toastr.error('Kindly select time also');
		}
		else
		{
			var datetype = jQuery('.dateType').val();
			var fromtime = jQuery('.time_from_time').val();
			var totime = jQuery('.time_to_time').val();
			
			jQuery('.from_time').val(fromtime);
			jQuery('.to_time').val(totime);
			
			jQuery('#chooseDate').modal('hide');
			
			var selectedDate = jQuery('.searchSelectedDate').val();
			var param = jQuery('.urlParm').val();
			var searchSelectedTime = jQuery('.searchSelectedTime').val();
			var keyword = jQuery('.search-category-service').val();
			
			if(datetype != 'anydate')
			{
				var date = new Date(selectedDate);
				month = ("0" + (date.getMonth() + 1)).slice(-2),
				day = ("0" + date.getDate()).slice(-2);
				year = date.getFullYear();
				getSelectedDate = day+'/'+month+'/'+year;
				jQuery('.select-search-date').val(getSelectedDate+' '+fromTime+' - '+toTime);
			}
			else
			{
				jQuery('.select-search-date').val(fromTime+' - '+toTime);
			}
			var type = jQuery('.search_service').val();
			searchCategory(selectedDate,param,searchSelectedTime,keyword,type);
			getCategories();
			
		}
	});
	
	jQuery(document).on('click','.get-search-name',function(){
        var keyword = jQuery(this).attr('data-attr');
		var type = jQuery('.search_service').val();
        jQuery('.search-category-service').val(keyword);
		var selectedDate = jQuery('.searchSelectedDate').val();
		var param = jQuery('.urlParm').val();
		var searchSelectedTime = jQuery('.searchSelectedTime').val();
		searchCategory(selectedDate,param,searchSelectedTime,keyword,type);
		getCategories();
    });
	
	/*=========Search location for find your service=========*/
	
	var searchList = new google.maps.places.Autocomplete(jQuery(".search-change-location")[0], {});

    google.maps.event.addListener(searchList, 'place_changed', function() {

        var place = searchList.getPlace();
        var Latitude = place.geometry.location.lat();
        var latprecision =  Math.pow(10, 6);
		var new_Latitude = (latprecision * Latitude)/latprecision;
		
		var Longitude = place.geometry.location.lng();
		var lngprecision =  Math.pow(10, 6);
		var new_Longitude = (lngprecision * Longitude)/lngprecision;

        jQuery('.searchLat').val(new_Latitude);
        jQuery('.searchLong').val(new_Longitude);
		jQuery('.service-limit').val(3);
		jQuery('.service-offset').val(0);
		jQuery('.pro-limit').val(1);
		jQuery('.pro-offset').val(0);
		
		var serLimit = jQuery('.service-limit').val();
		var serOffset = jQuery('.service-offset').val();
		var proLimit = jQuery('.pro-limit').val();
		var proOffset = jQuery('.pro-offset').val();
		var selectedDate = jQuery('.searchSelectedDate').val();
        var param = jQuery('.urlParm').val();
		var fromTime = jQuery('.from_time').val();
		var toTime = jQuery('.to_time').val();
		var service = '';
        var searchKeywords = '';
        var date = selectedDate;
        var fromTime = fromTime;
        var toTime = toTime;
        var serviceKeyword = jQuery('.search-category-service').val();
        var searchLat = new_Latitude;
        var searchLong = new_Longitude;
        var searchDate = jQuery('.getDate').val();
        var sortBy = jQuery('.sortBy').val();
        var getService = jQuery('.getService').val();
		var servicerange = jQuery('.get-service-time-range').val();
        var profId = jQuery('.profId').val();
        if(serviceKeyword.length >= 3)
        {
            service = serviceKeyword;
        }
        var price = jQuery('.get-price-range').val();
        var catId = [];
        jQuery(':checkbox:checked').each(function(i){
          catId[i] = $(this).val();
        });
		jQuery('.searchLoc').val('');
        jQuery.ajax({
            url: site_url+'/search-category',
            type: "POST",
            data:{proOffset:proOffset,proLimit:proLimit,serLimit:serLimit,serOffset:serOffset,searchLat:searchLat,searchLong:searchLong,servicerange:servicerange,searchDate:searchDate,profId:profId,getService:getService,param:param,sortBy:sortBy,date:date,catId:catId,fromTime:fromTime,toTime:toTime,service:service,price:price},
            dataType: "json",
            success: function(response) 
            {
				if(response.totalSer > serLimit)
				{
					jQuery('.load_more_services').css('display','block');
				}
				else
				{
					jQuery('.load_more_services').css('display','none');
				}
				
				if(response.totalPro > proLimit)
				{
					jQuery('.load_more_pro').css('display','block');
				}
				else
				{
					jQuery('.load_more_pro').css('display','none');
				}
               jQuery('#search-category-list').html(response.serviceList);
               jQuery('#search-professional-list').html(response.professionalList);
               jQuery('#count-services').html(response.countServices);
            }
        });
		getCategories();
    });
	
	
	function getCategories()
	{
		
		
		var selectedDate = jQuery('.searchSelectedDate').val();
        var param = jQuery('.urlParm').val();
		var searchSelectedTime = jQuery('.searchSelectedTime').val();
		var keyword = jQuery('.search-category-service').val();
		var type = jQuery('.search_service').val();
		
		var service = '';
        var date = selectedDate;
		
		var searchservice = type;
        var service = keyword;
		
		var searchLoc = jQuery('.searchLoc').val();
        var selectedTime = searchSelectedTime;
        var searchLat = jQuery('.searchLat').val();
        var searchLong = jQuery('.searchLong').val();
        var searchDate = jQuery('.getDate').val();
        var sortBy = jQuery('.sortBy').val();
        var getService = jQuery('.getService').val();
        var professionalservice = jQuery('.getProfessionalService').val();
		var servicerange = jQuery('.get-service-time-range').val();
        var profId = jQuery('.profId').val();
        var price = jQuery('.get-price-range').val();
        var catId = [];
        jQuery(':checkbox:checked').each(function(i){
          catId[i] = $(this).val();
        });
		// var latitude = jQuery('.searchLat').val();
		// var longitude = jQuery('.searchLong').val();
		// var param = jQuery('.urlParm').val();
		// var profId = jQuery('.profId').val();
		// var searchLoc = jQuery('.searchLoc').val();
		jQuery.ajax({
           url: site_url+'/wp-admin/admin-ajax.php',
            type: "POST",
            data:{action:'getCatByLocation',searchLoc:searchLoc,searchservice:searchservice,professionalservice:professionalservice,searchLat:searchLat,searchLong:searchLong,servicerange:servicerange,searchDate:searchDate,profId:profId,getService:getService,param:param,sortBy:sortBy,date:date,catId:catId,selectedTime:selectedTime,service:service,price:price},
            dataType: "html",
            success: function(response) 
            {
				jQuery('.catList').html(response);
            }
        });
	}
	
	/*=========On change location search service============*/
	
	jQuery(document).on('change','.search-change-location',function(){
		if(jQuery(this).val() == '')
		{
			jQuery('.searchLat').val('');
			jQuery('.searchLong').val('');
			jQuery('.searchLoc').val('all');
			var selectedDate = jQuery('.searchSelectedDate').val();
			var param = jQuery('.urlParm').val();
			var searchSelectedTime = jQuery('.searchSelectedTime').val();
			var keyword = jQuery('.search-category-service').val();
			var type = jQuery('.search_service').val();
			searchCategory(selectedDate,param,searchSelectedTime,keyword,type);
			getCategories();
		}
	});
	
	jQuery(document).on('click','.location_cross',function(){
		jQuery('.searchLoc').val('all');
		jQuery('.searchLat').val('');
		jQuery('.searchLong').val('');
		jQuery('.search-change-location').val('');
		getCategories();
		searchCat();
		
	});
	
	/*===========On click done from choose datetimepicker search service acc to date and time============*/
	
	jQuery(document).on('click','.search-date-time-picker',function(){
		jQuery('.choose-datepicker').css('display','none');
		jQuery('#chooseDate').modal('hide');
		selectedDatetime();
		searchCat();
		getCategories();
	});
	
	jQuery(document).on('click','.choose_date_cross',function(){
		if(jQuery('.select-search-date').val() != '')
		{
			jQuery('.choose-datepicker').css('display','none');
			jQuery('.getDate').val('');
			jQuery('.select-search-date').val('');
			jQuery('.searchSelectedDate').val('');
			jQuery('.searchSelectedTime').val('');
			searchCat();
			getCategories();
		}
	});
	
	/*==========Search service or professional in service list page==========*/
	
	jQuery(document).on('change','.search-category-service',function(){
		if(jQuery(this).val() == '')
		{
			searchCat();
			getCategories();
		}
	});

	jQuery(document).on('keyup','.search-category-service',function(){
        var keyword = jQuery(this).val();
		var type = jQuery('.search_service').val();
		var searchLoc = jQuery('.searchLoc').val();
		var searchLat = jQuery('.searchLat').val();
		var searchLong = jQuery('.searchLong').val();
		if(keyword != '')
		{
			jQuery.ajax({
				url: site_url+'/search-service',
				type: "POST",
				data: {keyword:keyword,type:type,searchLoc:searchLoc,searchLat:searchLat,searchLong:searchLong},
				dataType: "html",
				success: function(response) {
					if(keyword.length == 1)
					{
						jQuery('#search_service_list').css('display','none');
					}
					else
					{
						jQuery('#search_service_list').css('display','block');
						jQuery('#search_service_list').html(response);
					}
					
				}
			});
		}
		else
		{
			jQuery('#search_service_list').css('display','none');
			searchCat();
			getCategories();
		}
        
    });
	
	/*============Load more functionality===========*/
	
	jQuery(document).on('click','.service_load_more_btn',function(){
		var offset = jQuery('.service-offset').val();
		jQuery('.service-offset').val(parseInt(offset)+parseInt(1));
		var loadmore = 'ser_load_more';
		searchCat(loadmore);
	});
	
	jQuery(document).on('click','.pro_load_more_btn',function(){
		var offset = jQuery('.pro-offset').val();
		jQuery('.pro-offset').val(parseInt(offset)+parseInt(1));
		var loadmore = 'pro_load_more';
		searchCat(loadmore);
	});
	
	jQuery(document).on('change','.get-price-range',function(){
		var priceRange = jQuery(this).val();
		jQuery('.service_time_rangepicker').val('yes');
		jQuery('.price-range').html('€ '+priceRange.replace(/,/g , ' - '));
		getCategories();
	});
	
	jQuery(document).on('change','.get-service-time-range',function(){
		var servicerange = jQuery(this).val();
		jQuery('.service-time-range').html(servicerange.replace(/,/g , ' - ')+' min');
		getCategories();
	});
	
	/*==========common function for search in search list page============*/
	
	function searchCat(loadmore)
	{
		var selectedDate = jQuery('.searchSelectedDate').val();
        var param = jQuery('.urlParm').val();
		var searchSelectedTime = jQuery('.searchSelectedTime').val();
		var keyword = jQuery('.search-category-service').val();
		var type = jQuery('.search_service').val();
		if(loadmore == 'ser_load_more' || loadmore == 'pro_load_more')
		{
			load_more_search(selectedDate,param,searchSelectedTime,keyword,type,loadmore);
		}
		else
		{
			
			searchCategory(selectedDate,param,searchSelectedTime,keyword,type);
		}
	}
	
	jQuery(document).ready(function(){
		getCategories();
	});
	
	jQuery(document).on('change','.search-category',function(){
		searchCat();
		
    });
	
	function searchCategory(selectedDate,param,searchSelectedTime,searchKeywords,searchType)
    {
		var service = '';
        var date = selectedDate;
		
		jQuery('.service-limit').val(3);
		jQuery('.service-offset').val(0);
		jQuery('.pro-limit').val(1);
		jQuery('.pro-offset').val(0);
		
		var serLimit = jQuery('.service-limit').val();
		var serOffset = jQuery('.service-offset').val();
		var proLimit = jQuery('.pro-limit').val();
		var proOffset = jQuery('.pro-offset').val();
		
		var searchLoc = jQuery('.searchLoc').val();
        var selectedTime = searchSelectedTime;
        var searchLat = jQuery('.searchLat').val();
        var searchLong = jQuery('.searchLong').val();
        var searchDate = jQuery('.getDate').val();
        var sortBy = jQuery('.sortBy').val();
        var getService = jQuery('.getService').val();
        var professionalservice = jQuery('.getProfessionalService').val();
		var servicerange = jQuery('.get-service-time-range').val();
        var profId = jQuery('.profId').val();
        var searchservice = searchType;
        var service = searchKeywords;
        var price = jQuery('.get-price-range').val();
        var service_range = jQuery('.service_time_rangepicker').val();
        var catId = [];
        jQuery(':checkbox:checked').each(function(i){
          catId[i] = $(this).val();
        });
		jQuery.ajax({
            url: site_url+'/search-category',
            type: "POST",
            data:{service_range:service_range,proLimit:proLimit,proOffset:proOffset,serOffset:serOffset,serLimit:serLimit,searchLoc:searchLoc,searchservice:searchservice,professionalservice:professionalservice,searchLat:searchLat,searchLong:searchLong,servicerange:servicerange,searchDate:searchDate,profId:profId,getService:getService,param:param,sortBy:sortBy,date:date,catId:catId,selectedTime:selectedTime,service:service,price:price},
            dataType: "json",
            success: function(response) 
            {
				if(response.service_range == '')
				{
					console.log('fds');
					//$(".ex2").val(1).slider("refresh");
					//$(".ex2").slider("option", "value", '0,100');
					//jQuery('#maxPrice').html('€ '+response.greaterPrice);
					//jQuery('#minPrice').html('€ 0');
					jQuery('#maxPrice').html('500');
					jQuery('#minPrice').html('50');
					//$(function() { 
						 var $slide = $(".ex2").slider({
							range: true,
							values: [50, 100],
							min: 50,
							max: 100
						 });
						$slide.slider("option", "min", 50); // left handle should be at the left end, but it doesn't move
						$slide.slider("values", $slide.slider("values")); //force the view refresh, re-setting the current value
					//});
				}
				  
				jQuery('.get-price-range').val(0+','+response.greaterPrice);
				jQuery('.get-price-range').attr('data-slider-max',response.greaterPrice);
				jQuery('.get-price-range').attr('data-slider-value','[0,'+response.greaterPrice+']');
				jQuery('.get-price-range').attr('data-value',0+','+response.greaterPrice);
				 jQuery('.ex2').slider('refresh');
				if(response.totalSer > serLimit)
				{
					jQuery('.load_more_services').css('display','block');
				}
				else
				{
					jQuery('.load_more_services').css('display','none');
				}
				
				if(response.totalPro > proLimit)
				{
					jQuery('.load_more_pro').css('display','block');
				}
				else
				{
					jQuery('.load_more_pro').css('display','none');
				}
				jQuery('#search-category-list').html(response.serviceList);
				jQuery('#search-professional-list').html(response.professionalList);
				jQuery('#count-services').html(response.countServices);
			}
        });
		
    }
	
	function load_more_search(selectedDate,param,searchSelectedTime,searchKeywords,searchType,loadmore)
    {
		var service = '';
        var date = selectedDate;
		
		var serLimit = jQuery('.service-limit').val();
		var serOffset = jQuery('.service-offset').val();
		var checkOffset = parseInt(serOffset)+parseInt(1);
		var proLimit = jQuery('.pro-limit').val();
		var proOffset = jQuery('.pro-offset').val();
		
		var searchLoc = jQuery('.searchLoc').val();
        var selectedTime = searchSelectedTime;
        var searchLat = jQuery('.searchLat').val();
        var searchLong = jQuery('.searchLong').val();
        var searchDate = jQuery('.getDate').val();
        var sortBy = jQuery('.sortBy').val();
        var getService = jQuery('.getService').val();
        var professionalservice = jQuery('.getProfessionalService').val();
		var servicerange = jQuery('.get-service-time-range').val();
        var profId = jQuery('.profId').val();
        var searchservice = searchType;
        var service = searchKeywords;
        var price = jQuery('.get-price-range').val();
        var catId = [];
        jQuery(':checkbox:checked').each(function(i){
          catId[i] = $(this).val();
        });
		jQuery.ajax({
            url: site_url+'/search-category',
            type: "POST",
            data:{proLimit:proLimit,proOffset:proOffset,serOffset:serOffset,serLimit:serLimit,searchLoc:searchLoc,searchservice:searchservice,professionalservice:professionalservice,searchLat:searchLat,searchLong:searchLong,servicerange:servicerange,searchDate:searchDate,profId:profId,getService:getService,param:param,sortBy:sortBy,date:date,catId:catId,selectedTime:selectedTime,service:service,price:price},
            dataType: "json",
            success: function(response) 
            {
				if(loadmore == 'ser_load_more')
				{
					jQuery('#search-category-list').append(response.serviceList);
				}
				else
				{
					jQuery('#search-professional-list').append(response.professionalList);
				}
				
				
				if(jQuery('#search-category-list > div').length == response.totalSer)
				{
					jQuery('.load_more_services').css('display','none');
				}
				if(jQuery('#search-professional-list > div').length == response.totalPro)
				{
					jQuery('.pro_load_more_btn').css('display','none');
				}
			}
        });
		
    }
	
	function getCat(profId,param,latitude,longitude)
	{
		
		jQuery.ajax({
           url: site_url+'/wp-admin/admin-ajax.php',
            type: "POST",
            data:{action:'getCatByLocation',profId:profId,param:param,latitude:latitude,longitude:longitude},
            dataType: "html",
            success: function(response) 
            {
				jQuery('.catList').html(response);
            }
        });
	}

/*=====Order now page jquery=====*/

    /*========Next and back button functionality for tabs in order page=========*/

    jQuery(document).on('click','.confirm-time',function(){
        jQuery('#order-calender-wrap').css('display','block');
        jQuery('.confirm-order-location').css('display','none');
        jQuery('.confirm-order-detail').css('display','none');
        jQuery('.second-li').addClass('active');
    });
    jQuery(document).on('click','.confirm-location',function(){
        jQuery('#order-calender-wrap').css('display','none');
        jQuery('.confirm-order-location').css('display','block');
        jQuery('.confirm-order-detail').css('display','none');
        jQuery('.second-li').addClass('active');
    });
    jQuery(document).on('click','.confirm-order',function(){
        jQuery('.confirm-order-location').css('display','none');
        jQuery('.confirm-order-detail').css('display','block');
        jQuery('.third-li').addClass('active');
    });

    /*============Appointment Datepicker==============*/

    //jQuery(function() {
        /*jQuery('#appointment_datepicker').datepicker({
            
            
              language: "nl", 
              calendarWeeks: true, 
              todayHighlight: true

        });
        */
        // var defaults = {
            // inline: true,
            // sideBySide: true,
            // format: 'YYYY-MM-DD',
            // startDate:today,
			// maxDate:new Date(),
           // disabledDates: [new Date()]
          
        // };
        //jQuery('#appointment_datepicker').datepicker(defaults);

        /*jQuery('#appointment_datepicker').datepicker({
           
             beforeShowDay: function (date) {

                if (date.getDate() == 15 || date.getDate() == 1) {
                    return [true, ''];
                }
                return [false, ''];
               }
        });*/
    
    

    /*=========Update appointment status approved when customer click on order now==========*/
    
    jQuery(document).on('click','.order_now',function(){
        var appointmentId = jQuery(this).attr('data-attr');
        jQuery('.order_now').attr('disabled');
        jQuery('.order_now').html('Processing...');
        jQuery.ajax({
            url: site_url+'/wp-admin/admin-ajax.php',
            type: "GET",
            data: {action:'update_appointment_status',appointmentId:appointmentId},
            dataType: "json",
            success: function(response) {
                jQuery('.order_now').removeAttr('disabled');
                jQuery('.order_now').html('Order Now');
                if(response.success == true)
                {
                    toastr.success(response.msg, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                    window.setTimeout(function() {
                        window.location.href = response.url;
                    }, 2000);
                }
                else
                {
                    toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                }
            }
        });
    });

/*======Dashboard appointment and calendar jquery for customer dashboard=====*/
    
    /*====Show review star=====*/

    jQuery('#star1').starrr({
        change: function(e, value){
            jQuery('.rating').val(value);
        }
    });

    /*=====Cancel appointment======*/

    jQuery(document).on('click','.cancel-appoint',function(){
        var appointmentId = jQuery(this).attr('data-attr');
        jQuery('.appointId').val(appointmentId);
        jQuery('#cancelModal').modal('show');
    });

	/*===========On change language set cookie of language=========*/
	
	jQuery(document).on('click','#language_change',function(){
		var lang = $(this).attr('data-attr');
		jQuery.removeCookie("lang", '', { path: '/' });
        jQuery.cookie("lang", lang, { path: '/' });
	});
	

    



