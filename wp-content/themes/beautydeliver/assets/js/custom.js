(function(jQuery) {
	
  'use strict';
  	//Preloader
	jQuery(window).on('load',function () {
		jQuery('.preloader').remove();
	});


	// jQuery(document).on("scroll", onScroll);
	// //smoothscroll
	// jQuery(".service-info-wrap .btn-wrap .book-now").on('click', function (e) {
	// 	e.preventDefault();
	// 	jQuery(document).off("scroll");
	// 	jQuery('.service-info-wrap .btn-wrap .book-now').each(function () {
	// 		jQuery(this).removeClass('active');
	// 	})
	// 	jQuery(this).addClass('active');
	// 	var target = this.hash
	// 		, menu = target;
	// 	target = jQuery(target);
	// 	jQuery('html, body').stop().animate({
	// 		'scrollTop': target.offset().top + 2
	// 	}, 1000, 'swing', function () {
	// 		window.location.hash = target;
	// 		jQuery(document).on("scroll", onScroll);
	// 	});
	// });


	jQuery(document).ready(function(){

		// Wow Js
		var wow = new WOW({
			mobile: false // default
		})
		wow.init();

		// Date picker
		jQuery('body').find('[data-toggle="datepicker"]').datepicker({
			container: ".docs-datepicker-container",
			inline:true,
		});


		//Service Slider

		var serviceslider = jQuery('body').find('.service-slider');
		serviceslider.owlCarousel({
			loop:false,
			margin: 30,
			nav:true,
			autoPlay : true,
			dots:false,
			navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3,
				},
				1000:{
					items:4,
				}
			}
		});

		//ToolTip
		jQuery('[data-toggle="tooltip"]').tooltip(); 

		//categories Slider

		var categoriesslider = jQuery('body').find('.categories-grid-wrap');
		categoriesslider.owlCarousel({
			loop:false,
			margin: 20,
			nav:true,
			autoPlay : true,
			dots:false,
			navText : false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3,
				},
				1000:{
					items:6,
				}
			}
		});

		// Date Picker
  
		jQuery(function() {
			$('#datepicker').datepicker({
				minDate: 'today',
			});
		});

		// Upload Image
			
		function readsURL(input) { 
			console.log(input.files["0"].name);
			if (input.files && input.files[0]) {
			var uimage = new FileReader();
				uimage.readAsDataURL(input.files[0]);
				jQuery(".upload-image #uploadFile").val(input.files["0"].name);
			}
		}
		jQuery('body').find(".upload-image input[type='file']").on('change',function(){
			readsURL(this);
		});
		
		//Model
		jQuery(function() {
			//----- OPEN
			jQuery('[data-popup-open]').on('click', function(e) {
			var targeted_popup_class = jQuery(this).attr('data-popup-open');
			jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);
		
			e.preventDefault();
			});
		
			//----- CLOSE
			jQuery(document).on('click','[data-popup-close]','.cstmClose',function(e) {
			var targeted_popup_class = jQuery(this).attr('data-popup-close');
			jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);
		
			e.preventDefault();
			});
		});
		
		// Aos Js
		AOS.init({
			easing: 'ease-in-out-sine'
		});

		//Header
		jQuery(window).on("load resize scroll", function(e) {
			var Win = jQuery(window).height();
			var Header = jQuery("header").height();
			var Footer = jQuery("footer").height();
			var NHF = Header + Footer;
			jQuery('.main').css('min-height', (Win - NHF));
		});

		//Textarea 
		jQuery('.firstCap, textarea').on('keypress', function(event) {
			var jQuerythis = jQuery(this),
			thisVal = jQuerythis.val(),
			FLC = thisVal.slice(0, 1).toUpperCase(),
			con = thisVal.slice(1, thisVal.length);
			jQuery(this).val(FLC + con);
		});

		//Sticky Header
		jQuery(window).scroll(function () {
			var scroll = jQuery(window).scrollTop();
			if (scroll >= 200) {
				jQuery(".header-bottom").addClass("fixed-header");
			} else {
				jQuery(".header-bottom").removeClass("fixed-header");
			}
		});

	});
  
	//Page Zoom
	document.documentElement.addEventListener('touchstart', function (event) {
		if (event.touches.length > 1) {
			event.preventDefault();
		}
	}, false);

	//Avoid pinch zoom on iOS
	document.addEventListener('touchmove', function(event) {
		if (event.scale !== 1) {
			event.preventDefault();
		}
	}, false);

	 // Profile Upload
    
	 function readURL(input) { 
		var size = input.files[0].size;
		if(size > 1000000)
		{
			toastr.error('Upload profile image less than size 1MB');
		}
		else
		{
			if (input.files && input.files[0]) {
			var reader = new FileReader();
				reader.onload = function (e) {
					jQuery('.profile-img-sec figure').attr('style', 'background-image: url('+e.target.result   +')');  
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
        
    }
    jQuery('body').find(".profile-img-sec input[type='file']").on('change',function(){
        readURL(this);
    });
	
})(jQuery)

// Range Slider
    
jQuery('.ex2').slider();
jQuery('.ex1').slider();